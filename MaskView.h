//
//  MaskView.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 23/03/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaskView : UIView

@property (nonatomic, readonly) CGRect maskFrame;

- (void)maskSuperview:(UIView *)superview;
- (void)setMaskFrame:(CGRect)frame animated:(BOOL)animated;
- (void)setMaskText:(NSString *)text topSpace:(CGFloat)originY animated:(BOOL)animated;
- (void)setMaskText:(NSString *)text topSpace:(CGFloat)originY color:(UIColor *)color animated:(BOOL)animated;
- (void)hideMaskViewCompletion:(void(^)())completionBlock;

@end
