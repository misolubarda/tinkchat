//
//  MainNavigationController.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 03/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "MainNavigationController.h"
#import "MainToSettingsTransition.h"
#import "SettingsVC.h"
#import "TinkHelper.h"
#import "UsersListVC.h"

@interface MainNavigationController () <UINavigationControllerDelegate>
@end

@implementation MainNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [TinkHelper addDefaultShadowToView:self.navigationBar];

    self.delegate = self;
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    if ([fromVC isKindOfClass:[UsersListVC class]] && [toVC isKindOfClass:[SettingsVC class]])
    {
        MainToSettingsTransition *transition = [[MainToSettingsTransition alloc] init];
        transition.pushForward = YES;
        
        return transition;
    }
    else if ([fromVC isKindOfClass:[SettingsVC class]] && [toVC isKindOfClass:[UsersListVC class]])
    {
        return [[MainToSettingsTransition alloc] init];
    }
    return nil;
}

@end
