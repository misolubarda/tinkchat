//
//  SignInUpTransition.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 28/01/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

@import UIKit;
#import "SignInJoinVC.h"
#import "SignUpVC.h"
#import "SignInVC.h"

@interface SignInUpTransition : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic, assign) BOOL forwardTransition;

- (void)initWithDirectionForward:(BOOL)forward;
@end
