//
//  LoginNavigationController.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 29/01/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "LoginNavigationController.h"
#import "SignInUpTransition.h"
#import "TinkHelper.h"

@interface LoginNavigationController () <UINavigationControllerDelegate>

@end

@implementation LoginNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.delegate = self;
    self.navigationBarHidden = YES;
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    SignInUpTransition *transition;
    
    if ([fromVC isKindOfClass:[SignInJoinVC class]])
    {
        if ([toVC isKindOfClass:[SignInVC class]] || [toVC isKindOfClass:[SignUpVC class]])
        {
            transition = [[SignInUpTransition alloc] init];
            transition.forwardTransition = YES;
        }
    }
    else if ([toVC isKindOfClass:[SignInJoinVC class]])
    {
        if ([fromVC isKindOfClass:[SignInVC class]] || [fromVC isKindOfClass:[SignUpVC class]])
        {
            transition = [[SignInUpTransition alloc] init];
            transition.forwardTransition = NO;
        }
    }
    
    return transition;
}

- (BOOL)fromVC:(UIViewController *)fromVC andToVC:(UIViewController *)toVC isOfClass:(Class)firstClass andClass:(Class)secondClass
{
    return YES;
}

@end
