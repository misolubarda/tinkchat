//
//  SignInUpTransition.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 28/01/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "SignInUpTransition.h"

@interface SignInUpTransition ()

@property (nonatomic, strong) id currentTransitionContext;
@property (nonatomic, strong) UIViewController *sourceVC;
@property (nonatomic, strong) UIViewController *destVC;
@property (nonatomic, strong) UIView *sourceButton;
@property (nonatomic, strong) UIView *destButton;
@property (nonatomic, strong) UIView *sourceContainerView;
@property (nonatomic, strong) UIView *destContainerView;
@property (nonatomic, strong) UIView *sourceDisclamerView;
@property (nonatomic, strong) UIView *destDisclamerView;

@end

@implementation SignInUpTransition

- (void)initWithDirectionForward:(BOOL)forward
{
    
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 1.0;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    self.currentTransitionContext = transitionContext;
    self.sourceVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    self.destVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];

    if (self.forwardTransition)
    {
        SignInJoinVC *sourceVC = (SignInJoinVC *)self.sourceVC;
        
        if ([self.destVC isKindOfClass:[SignUpVC class]])
        {
            self.sourceButton = sourceVC.joinButton;

            SignUpVC *destVC = (SignUpVC *)self.destVC;
            self.destButton = destVC.joinButton;
            self.destContainerView = destVC.containerView;
            self.destDisclamerView = destVC.disclamerTextView;
        }
        else if ([self.destVC isKindOfClass:[SignInVC class]])
        {
            self.sourceButton = sourceVC.signInButton;

            SignInVC *destVC = (SignInVC *)self.destVC;
            self.destButton = destVC.signInButton;
            self.destContainerView = destVC.containerView;
        }
        
        [self forwardAnimation];
    }
    else
    {
        SignInJoinVC *destVC = (SignInJoinVC *)self.destVC;

        if ([self.sourceVC isKindOfClass:[SignUpVC class]])
        {
            self.destButton = destVC.joinButton;

            SignUpVC *sourceVC = (SignUpVC *)self.sourceVC;
            self.sourceButton = sourceVC.joinButton;
            self.sourceContainerView = sourceVC.containerView;
        }
        else if ([self.sourceVC isKindOfClass:[SignInVC class]])
        {
            self.destButton = destVC.signInButton;

            SignInVC *sourceVC = (SignInVC *)self.sourceVC;
            self.sourceButton = sourceVC.signInButton;
            self.sourceContainerView = sourceVC.containerView;
        }
        
        [self backwardAnimation];
    }
}

- (void)prepareForAnimation
{
    UIView *containerView = [self.currentTransitionContext containerView];
    
    /**
     *  Adding subviews
     */
    [containerView addSubview:self.sourceVC.view];
    [containerView insertSubview:self.destVC.view belowSubview:self.sourceVC.view];
    
    /**
     *  AFTER ADDING SUBVIEWS TO SOURCE, MUST CALL -layoutIfNeeded!!!!
     */
    [self.sourceVC.view layoutIfNeeded];
    [self.destVC.view layoutIfNeeded];
    
}

- (void)forwardAnimation
{
    [self prepareForAnimation];
    
    UIView *containerView = [self.currentTransitionContext containerView];

    /**
     *  Move destination view next to source view
     */
    self.destVC.view.transform = CGAffineTransformTranslate(self.destVC.view.transform, self.destVC.view.frame.size.width, 0);
    
    /**
     *  Hide destination join button
     */
    self.destButton.hidden = YES;
    self.destDisclamerView.hidden = YES;
    
    /**
     *  Move container view to be aligned with source join button
     */
    CGFloat containerViewJoinButtonDistance = self.destContainerView.frame.origin.y - self.sourceButton.frame.origin.y;
    self.destContainerView.transform = CGAffineTransformTranslate(self.destContainerView.transform, 0, -containerViewJoinButtonDistance);
    
    /**
     *  Create cover view and add it over container view.
     */
    UIView *coverView = [[UIView alloc] initWithFrame:self.destContainerView.frame];
    coverView.backgroundColor = self.destVC.view.backgroundColor;
    [self.destVC.view addSubview:coverView];
    
    CGFloat joinButtonsVerticalDistance = self.destButton.frame.origin.y - self.sourceButton.frame.origin.y;
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView animateWithDuration:0.7
    animations:^
    {
        containerView.transform = CGAffineTransformMakeTranslation(-self.destVC.view.frame.size.width, 0);
        self.sourceButton.transform = CGAffineTransformMakeTranslation(self.destVC.view.frame.size.width, 0);
    }
    completion:^(BOOL finished)
    {
        [UIView animateWithDuration:0.5
        animations:^
        {
            self.sourceButton.transform = CGAffineTransformTranslate(self.sourceButton.transform, 0, joinButtonsVerticalDistance);
            self.destContainerView.transform = CGAffineTransformTranslate(self.destContainerView.transform, 0, containerViewJoinButtonDistance);
            coverView.transform = CGAffineTransformTranslate(self.destContainerView.transform, 0, joinButtonsVerticalDistance);
        }
        completion:^(BOOL finished)
        {
            /**
             *  Resetting to initial state
             */
            self.destButton.hidden = NO;
            [coverView removeFromSuperview];
            self.sourceButton.transform = CGAffineTransformIdentity;
            self.sourceVC.view.transform = CGAffineTransformIdentity;
            self.destVC.view.transform = CGAffineTransformIdentity;
            containerView.transform = CGAffineTransformIdentity;
            self.destDisclamerView.hidden = NO;
            
            [self.currentTransitionContext completeTransition:YES];
        }];
     }];
}

- (void)backwardAnimation
{
    [self prepareForAnimation];
    
    UIView *containerView = [self.currentTransitionContext containerView];

    /**
     *  Move destination view next to source view
     */
    containerView.transform = CGAffineTransformTranslate(containerView.transform, -self.sourceVC.view.frame.size.width, 0);
    self.sourceVC.view.transform = CGAffineTransformTranslate(self.sourceVC.view.transform, self.sourceVC.view.frame.size.width, 0);
    
    /**
     *  Hide destination join button
     */
    self.destButton.hidden = YES;
    self.sourceDisclamerView.hidden = YES;
    
    /**
     *  Move container view to be aligned with source join button
     */
    CGFloat containerViewJoinButtonDistance = self.sourceContainerView.frame.origin.y - self.sourceButton.frame.origin.y;
    
    /**
     *  Create cover view and add it over container view.
     */
    UIView *coverView = [[UIView alloc] initWithFrame:self.sourceContainerView.frame];
    coverView.backgroundColor = self.sourceVC.view.backgroundColor;
    [self.sourceVC.view insertSubview:coverView belowSubview:self.sourceButton];
    coverView.transform = CGAffineTransformTranslate(coverView.transform, 0, -containerViewJoinButtonDistance);
    
    CGFloat joinButtonsVerticalDistance = self.destButton.frame.origin.y - self.sourceButton.frame.origin.y;
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView animateWithDuration:0.7
    animations:^
    {
        self.sourceContainerView.transform = CGAffineTransformTranslate(self.sourceContainerView.transform, 0, joinButtonsVerticalDistance-containerViewJoinButtonDistance);
        coverView.transform = CGAffineTransformTranslate(coverView.transform, 0, joinButtonsVerticalDistance);
        self.sourceButton.transform = CGAffineTransformTranslate(self.sourceButton.transform, 0, joinButtonsVerticalDistance);
    }
    completion:^(BOOL finished)
    {
        [UIView animateWithDuration:0.5
        animations:^
        {
            containerView.transform = CGAffineTransformIdentity;
            self.sourceVC.view.transform = CGAffineTransformMakeTranslation(self.destVC.view.frame.size.width, 0);
            self.sourceButton.transform = CGAffineTransformTranslate(self.sourceButton.transform, -self.destVC.view.frame.size.width, 0);
        }
        completion:^(BOOL finished)
        {
            /**
             *  Resetting to initial state
             */
            self.destButton.hidden = NO;
            [coverView removeFromSuperview];
            containerView.transform = CGAffineTransformIdentity;
            self.sourceButton.transform = CGAffineTransformIdentity;
            self.sourceVC.view.transform = CGAffineTransformIdentity;
            self.destVC.view.transform = CGAffineTransformIdentity;
            
            [self.currentTransitionContext completeTransition:YES];
         }];
    }];
}

@end
