//
//  MainToSettingsTransition.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 03/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "MainToSettingsTransition.h"

@interface MainToSettingsTransition ()

@property (nonatomic, strong) id currentTransitionContext;

@end

@implementation MainToSettingsTransition

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return .3f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    self.currentTransitionContext = transitionContext;
    
    UIView *containerView = [transitionContext containerView];
    
    UIView *sView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    UIView *dView = [transitionContext viewForKey:UITransitionContextToViewKey];
    
    if (self.pushForward)
    {
        [containerView addSubview:dView];
    }
    else
    {
        [containerView insertSubview:dView belowSubview:sView];
        sView.hidden = YES;
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = [self transitionDuration:transitionContext];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.delegate = self;
    
    if (self.pushForward)
    {
        transition.subtype = kCATransitionFromLeft;
        [dView.layer addAnimation:transition forKey:nil];
    }
    else
    {
        transition.type = kCATransitionReveal;
        transition.subtype = kCATransitionFromRight;
        [sView.layer addAnimation:transition forKey:nil];
    }
}


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [self.currentTransitionContext completeTransition:YES];
}

@end
