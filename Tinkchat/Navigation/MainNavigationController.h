//
//  MainNavigationController.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 03/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainNavigationController : UINavigationController

@end
