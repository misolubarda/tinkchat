//
//  LoginNavigationController.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 29/01/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginNavigationController : UINavigationController

@end
