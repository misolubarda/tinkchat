//
//  MainToSettingsTransition.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 03/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainToSettingsTransition : NSObject <UIViewControllerAnimatedTransitioning, UINavigationControllerDelegate>

@property (nonatomic) BOOL pushForward;

@end
