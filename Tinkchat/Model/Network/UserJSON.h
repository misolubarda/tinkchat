//
//  UserJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 15/07/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"

typedef NS_ENUM(NSUInteger, SignInRegistrationStatus) {
    SignInRegistrationStatus_UsernameRequired = 0,
    SignInRegistrationStatus_OnboardingRequired = 1,
    SignInRegistrationStatus_Done = 2,
    SignInRegistrationStatus_Count = 3 //Do not remove, set to last value
};

@interface UserJSON : JSONModel

@property (nonatomic, strong) NSString<Optional> *username;
@property (nonatomic) SignInRegistrationStatus registrationStatus;

@end
