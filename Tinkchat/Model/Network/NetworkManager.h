//
//  TinkchatNetworkManager.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 08.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DOSingleton/DOSingleton.h>
#import "NetworkConstants.h"
#import "ErrorJSON.h"
#import "SignUpJSON.h"
#import "SignInJSON.h"
#import "TinkchatManager.h"
#import "TinksListJSON.h"
#import "TinkSendJSON.h"
#import "UsersListJSON.h"
#import "InviteUserJSON.h"
#import "EditUserJSON.h"

@protocol NetworkManagerDataSource

@required
- (NSDictionary *)networkManagerPostDictionaryForSignInRequest;

@end

@interface NetworkManager : DOSingleton

@property (nonatomic, weak) id<NetworkManagerDataSource> delegate;

- (void)signUpUserWithParameters:(NSDictionary *)parameters completion:(void(^)(SignUpJSON *signUpJSON, ErrorJSON *errorJSON))completionBlock;
- (void)signInUserWithParameters:(NSDictionary *)parameters completion:(void(^)(SignInJSON *signInJSON, ErrorJSON *errorJSON))completionBlock;
- (void)signInUserWithFacebookWithParameters:(NSDictionary *)parameters completion:(void(^)(SignInJSON *signInJSON, ErrorJSON *errorJSON))completionBlock;
- (void)editUserData:(NSDictionary *)parameters completion:(void(^)(EditUserJSON *editUserJSON, ErrorJSON *errorJSON))completionBlock;
- (void)changePassword:(NSDictionary *)parameters completion:(void(^)(ErrorJSON *errorJSON))completionBlock;
- (void)resetPassword:(NSDictionary *)parameters completion:(void(^)(ErrorJSON *errorJSON))completionBlock;

- (void)friendsListWithParameters:(NSDictionary *)parameters completion:(void(^)(FriendsListJSON *friendsListJSON, ErrorJSON *errorJSON))completionBlock;
- (void)findTinkchatUserWithParameters:(NSDictionary *)parameters completion:(void (^)(UsersListJSON *usersListJSON, ErrorJSON *errorJSON))completionBlock;
- (void)addTinkchatUserWithParameters:(NSDictionary *)parameters completion:(void (^)(AddUserJSON *addUserJSON, ErrorJSON *errorJSON))completionBlock;
- (void)removeTinkchatUserWithParameters:(NSDictionary *)parameters completion:(void (^)(ErrorJSON *errorJSON))completionBlock;
- (void)usersAddedMeListWithParameters:(NSDictionary *)parameters completion:(void(^)(FriendsListJSON *friendsListJSON, ErrorJSON *errorJSON))completionBlock;
- (void)inviteWithParameters:(NSDictionary *)parameters completion:(void (^)(InviteUserJSON *inviteUserJSON, ErrorJSON *errorJSON))completionBlock;

- (void)tinksListWithParameters:(NSDictionary *)parameters completion:(void(^)(TinksListJSON *tinksListJSON, ErrorJSON *errorJSON))completionBlock;
- (void)sendTinkToUserWithParameters:(NSDictionary *)parameters completion:(void (^)(TinkSendJSON *tinkJSON, ErrorJSON *errorJSON))completionBlock;
- (void)markTinkAsRead:(NSDictionary *)parameters completion:(void (^)(ErrorJSON *errorJSON))completionBlock;

- (void)sendFeedback:(NSDictionary *)parameters completion:(void (^)(ErrorJSON *errorJSON))completionBlock;

@end
