//
//  NetworkConstants.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 24/01/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ErrorJSON;

typedef NS_ENUM(NSUInteger, EndpointType) {
    EndpointTypeNone,
    EndpointTypeSingUp,
    EndpointTypeSingIn,
    EndpointTypeSignInFacebook,
    EndpointTypeEditUserData,
    EndpointTypePasswordChange,
    EndpointTypePasswordReset,
    
    EndpointTypeFriendsList,
    EndpointTypeFindTinkchatUser,
    EndpointTypeAddTinkchatUser,
    EndpointTypeRemoveTinkchatUser,
    EndpointTypeInvite,
    EndpointTypeUsersAddedMe,
    
    EndpointTypeTinksList,
    EndpointTypeSendTink,
    EndpointTypeMarkTinkRead,
    
    EndpointTypeSendFeedback
};

typedef NS_ENUM(NSUInteger, NetworkManagerRequestKey)
{
    NetworkManagerRequestKeyHeader,
    NetworkManagerRequestKeyBody,
    NetworkManagerRequestKeyOther
};

typedef NS_ENUM(NSUInteger, NetworkManagerAPIKey)
{
    NetworkManagerAPIKey_SignUp_Fullname,
    NetworkManagerAPIKey_SignUp_Username,
    NetworkManagerAPIKey_EditUser_RegistrationStatus,
    
    NetworkManagerAPIKey_SignInFacebook_AccessToken,
    
    NetworkManagerAPIKey_General_AuthToken,
    NetworkManagerAPIKey_General_PushToken,
    NetworkManagerAPIKey_General_Email,
    NetworkManagerAPIKey_General_Password,
    
    NetworkManagerAPIKey_FindTinkchatUser_SearchString,
    
    NetworkManagerAPIKey_AddTinkchatUser_UserId,
    
    NetworkManagerAPIKey_SendTink_UserId,
    
    NetworkManagerAPIKey_MarkTink_tinkId,
    
    NetworkManagerAPIKey_RemoveTinkchatUser_UserId,
    
    NetworkManagerAPIKey_Invite_Email,
    
    NetworkManagerAPIKey_PasswordOld,
    NetworkManagerAPIKey_PasswordNew,
    
    NetworkManagerAPIKey_SendFeedback_Message,
    NetworkManagerAPIKey_SendFeedback_Subject
};

typedef NS_ENUM(NSUInteger, ErrorCodeGeneral)
{
    ErrorCodeGeneral_AuthTokenInvalid = 101,
    
    ErrorCodeGeneral_ParsingResponse = 99000,   // parsing http response failed
    ErrorCodeGeneral_CustomForUser = 99001,     // custom error to show to user
    ErrorCodeGeneral_CustomInternal = 99002,    // custom error for internal logging
    ErrorCodeGeneral_ParsingError = 99003    // parsing http error response failed
    
};

typedef NS_ENUM(NSUInteger, ErrorCodeSignUp)
{
    ErrorCodeSignUp_EmailMissing = 101,
    ErrorCodeSignUp_EmailTaken = 102,
    ErrorCodeSignUp_NameMissing = 103,
    ErrorCodeSignUp_PasswordMissing = 104,
    ErrorCodeSignUp_PasswordTooShort = 105
};

typedef NS_ENUM(NSUInteger, ErrorCodeSignIn)
{
    ErrorCodeSignIn_EmailOrPassWrong = 101,
    ErrorCodeSignIn_AccountNotActivated = 103,
    ErrorCodeSignIn_EmailRequired = 104,
    ErrorCodeSignIn_PasswordRequired = 105
};

typedef NS_ENUM(NSUInteger, ErrorCodeSignInFacebook)
{
    ErrorCodeSignInFacebook_AccessTokenMissing = 101,
    ErrorCodeSignInFacebook_AccessTokenInvalid = 102
};

typedef NS_ENUM(NSUInteger, ErrorCodeEditUserData)
{
    ErrorCodeEditUserData_UsernameInvalid = 102,
    ErrorCodeEditUserData_UsernameTaken = 103,
    ErrorCodeEditUserData_UsernameTooShort = 1001,          //Local error code
    ErrorCodeEditUserData_UsernameTooLong = 1002            //Local error code
};

typedef NS_ENUM(NSUInteger, ErrorCodePasswordChange)
{
    ErrorCodePasswordChange_OldPasswordIncorrect = 102,
    ErrorCodePasswordChange_NewPasswordTooShort = 103
};

typedef NS_ENUM(NSUInteger, ErrorCodePasswordReset)
{
    ErrorCodePasswordReset_EmailMissing = 101,
    ErrorCodePasswordReset_EmailNotExist = 102
};

typedef NS_ENUM(NSUInteger, ErrorCodeResendConfirmationCode)
{
    ErrorCodeResendConfirmationCode_EmailMissing = 101,
    ErrorCodeResendConfirmationCode_UserNotExist = 102
};

//typedef NS_ENUM(NSUInteger, ErrorCodeFindTinkchatUser)
//{
//};

typedef NS_ENUM(NSUInteger, ErrorCodeBanTinkchatUser)
{
    ErrorCodeBanTinkchatUser_UserIdRequred = 103,
    ErrorCodeBanTinkchatUser_BannedUserNotExist = 104,
    ErrorCodeBanTinkchatUser_SelfBan = 105,
    ErrorCodeBanTinkchatUser_AlreadyBan = 106
};

//typedef NS_ENUM(NSUInteger, ErrorCodeCheckIfUserExists)
//{
//};

typedef NS_ENUM(NSUInteger, ErrorCodeTinksList)
{
    ErrorCodeTinksList_RecipientIdRequired = 103
};

typedef NS_ENUM(NSUInteger, ErrorCodeSendTink)
{
    ErrorCodeSendTink_RecipientBanned = 103,
    ErrorCodeSendTink_RecipientNotExist = 104
};

typedef NS_ENUM(NSUInteger, ErrorCodeMarkTinkRead)
{
    ErrorCodeMarkTinkRead_NotRecipientsTink = 103
};

//typedef NS_ENUM(NSUInteger, ErrorCodeFriendsList)
//{
//};

typedef NS_ENUM(NSUInteger, ErrorCodeAddTinkchatUser)
{
    ErrorCodeAddTinkchatUser_UserIdRequired = 103,
    ErrorCodeAddTinkchatUser_AddingUnknownUser = 104,
    ErrorCodeAddTinkchatUser_AddingSelf = 105,
    ErrorCodeAddTinkchatUser_AlreadyAdded = 106
};

typedef NS_ENUM(NSUInteger, ErrorCodeRemoveTinkchatUser)
{
    ErrorCodeRemoveTinkchatUser_UserIdRequired = 103,
    ErrorCodeRemoveTinkchatUser_RemovingUnknownUser = 104,
    ErrorCodeRemoveTinkchatUser_NotFriend = 105
};

typedef NS_ENUM(NSUInteger, ErrorCodeInvite)
{
    ErrorCodeInviteUserIdRequired = 102,
    ErrorCodeInviteCannotInviteSelf = 103,
    ErrorCodeInviteAlreadyFriends = 104,
    
    ErrorCodeInviteEmailMissing = 998,
    ErrorCodeInviteEmailNotValid = 999 //local error code
};

typedef NS_ENUM(NSUInteger, ErrorCodeSendFeedback)
{
    ErrorCodeSendFeedback_SubjectRequired = 102,
    ErrorCodeSendFeedback_MessageRequired = 103
};

@interface NetworkConstants : NSObject

+ (NSString *)relativeUrlForEndpoint:(EndpointType)endpoint;
+ (NSString *)apiKeyForType:(NetworkManagerAPIKey)apiKeyType;
+ (NSString *)requestKeyForType:(NetworkManagerRequestKey)requestKeyType;

+ (BOOL)shouldShowErrorToUserByDefault:(ErrorJSON *)errorJSON;

@end
