//
//  UserJSON.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 15/07/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "UserJSON.h"

@interface UserJSON ()

@property (nonatomic, strong) NSString<Optional> *registration_status;

@end

@implementation UserJSON

+ (BOOL)propertyIsIgnored:(NSString *)propertyName
{
    if ([propertyName isEqualToString:@"registrationStatus"])
    {
        return YES;
    }
    return NO;
}

- (void)setRegistration_status:(NSString<Optional> *)registration_status
{
    _registration_status = registration_status;
    
    if (registration_status.length > 0)
    {
        NSInteger status = registration_status.integerValue;
        
        if (status >= 0 && status < SignInRegistrationStatus_Count)
        {
            self.registrationStatus = status;
        }
        else
        {
            [AnalyticsHelper trackInvalidStateWithMessage:@"Invalid registrationStatus = SignInRegistrationStatus_Count" inMethod:__PRETTY_FUNCTION__];
            self.registrationStatus = SignInRegistrationStatus_Count;
        }
    }
    else
    {
        [AnalyticsHelper trackInvalidStateWithMessage:@"Invalid registrationStatus = SignInRegistrationStatus_Count" inMethod:__PRETTY_FUNCTION__];
        self.registrationStatus = SignInRegistrationStatus_Count;
    }
}

@end
