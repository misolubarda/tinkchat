//
//  FriendJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 22.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "JSONModel.h"

@protocol AddressBookUserJSON
@end

@interface AddressBookUserJSON : JSONModel

@property (nonatomic, strong) NSString<Optional> *parseUsername;
@property (nonatomic, strong) NSString<Optional> *firstName;
@property (nonatomic, strong) NSString<Optional> *lastName;
@property (nonatomic, strong) NSString<Optional> *email0;
@property (nonatomic, strong) NSString<Optional> *email1;
@property (nonatomic, strong) NSString<Optional> *email2;
@property (nonatomic, strong) NSString<Optional> *email3;
@property (nonatomic, strong) NSString<Optional> *mobile0;
@property (nonatomic, strong) NSString<Optional> *mobile1;
@property (nonatomic, strong) NSString<Optional> *mobile2;
@property (nonatomic, strong) NSString<Optional> *mobile3;

@property (nonatomic, strong) NSString<Ignore> *fullName;

- (void)inviteUser;
- (void)saveToFriends;

@end
