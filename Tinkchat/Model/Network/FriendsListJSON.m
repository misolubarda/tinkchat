//
//  FriendsListJSON.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 14.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "FriendsListJSON.h"
#import "FriendCell.h"
#import "TinkchatManager.h"
#import "KeychainManager.h"

@interface FriendsListJSON ()

@property (nonatomic, strong) NSArray<Ignore> *friendsIDs;
@property (nonatomic, strong) NSMutableArray<Ignore> *friendsAnimating;

@end

@implementation FriendsListJSON

- (NSArray<FriendJSON> *)friends
{
    if (!_friends)
    {
        _friends = (NSArray<FriendJSON> *)@[];
    }
    return _friends;
}

- (NSMutableArray<Ignore> *)friendsAnimating
{
    if (!_friendsAnimating)
    {
        _friendsAnimating = [@[] mutableCopy];
    }
    return _friendsAnimating;
}

- (void)updateWithNewFriendList:(FriendsListJSON *)newFriendList dispatchAsync:(BOOL)async completionBlock:(void (^)(NSArray *, NSArray *, NSArray *))completionBlock
{
    /**
     *  Sorting incomming array
     */
   newFriendList.friends = (NSArray<FriendJSON> *)[newFriendList.friends sortedArrayUsingComparator:^NSComparisonResult(FriendJSON *user1, FriendJSON *user2)
    {
        NSComparisonResult result = [user1.name localizedCaseInsensitiveCompare:user2.name];
        
        if (result == NSOrderedSame)
        {
            return [@(user1.user_id.integerValue) compare:@(user2.user_id.integerValue)];
        }
        
        return result;
    }];
    
    NSPredicate *myselfPredicate = [NSPredicate predicateWithFormat:@"user_id like %@", [KeychainManager userId]];
    
    NSPredicate *myselfExcludedPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"NOT %@", myselfPredicate.predicateFormat]];
    
    NSArray *myself = [newFriendList.friends filteredArrayUsingPredicate:myselfPredicate];

    newFriendList.friends = (NSArray<FriendJSON> *)[newFriendList.friends filteredArrayUsingPredicate:myselfExcludedPredicate];
    
    if (myself.count > 0)
    {
        newFriendList.friends = (NSArray<FriendJSON> *)[myself arrayByAddingObjectsFromArray:newFriendList.friends];
    }
    
    
    NSMutableArray *indexesToDelete = [NSMutableArray array];
    NSMutableArray *indexesToInsert = [NSMutableArray array];
    NSMutableArray *indexesToReload = [NSMutableArray array];

    
    NSInteger maxCount = self.friends.count >= newFriendList.friends.count ? self.friends.count : newFriendList.friends.count;
    
    for (int i = 0; i < maxCount; i++)
    {
        FriendJSON *currentFriend;
        FriendJSON *newFriend;
        
        if (i < self.friends.count)
        {
            currentFriend = self.friends[i];
        }
        else
        {
            [indexesToInsert addObject:@(i)];
            continue;
        }
        
        if (i < newFriendList.friends.count)
        {
            newFriend = newFriendList.friends[i];
        }
        else
        {
            [indexesToDelete addObject:@(i)];
            continue;
        }
        
        if (![currentFriend.user_id isEqualToString:newFriend.user_id])
        {
            [indexesToReload addObject:@(i)];
            continue;
        }
    }
    
    NSMutableArray *friendIDs = [NSMutableArray array];
    
    for (FriendJSON *friend in newFriendList.friends) {
        [friendIDs addObject:friend.user_id];
    }

    self.friends = newFriendList.friends;
    self.friendsIDs = friendIDs;
    
    if (completionBlock)
    {
        completionBlock(indexesToInsert, indexesToDelete, indexesToReload);
    }
    
}

- (BOOL)containsUserID:(NSString *)userID
{
    for (NSString *friendID in self.friendsIDs)
    {
        if ([friendID isEqualToString:userID])
        {
            return YES;
        }
    }
    
    return NO;
}


- (BOOL)sendTinkToFriendID:(NSString *)friendID notSendButAnimateOnly:(BOOL)animateOnly completionBlock:(void (^)(NSTimeInterval))completionBlock
{
    NSArray *friendIdAnimatingArray = [self.friendsAnimating filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.userId like %@", friendID]];
    
    if (friendIdAnimatingArray.count > 0)
    {
        NSDictionary *friendAnimating = [friendIdAnimatingArray firstObject];
        
        /**
         *  Continue animating. If continueAnimatingPastStartTime returns NO, replace friendAnimating data in friends animating array and start animating cell
         */
        if (![self isCellAnimatingForFriendID:friendID])
        {
            NSInteger objIndex = [self.friendsAnimating indexOfObject:friendAnimating];
            friendAnimating = [self friendAnimatingWithId:friendID];
            [self.friendsAnimating replaceObjectAtIndex:objIndex withObject:friendAnimating];
            
            if (!animateOnly)
            {
                [self sendTinkToFriendId:friendID];
            }

            if (completionBlock)
            {
                completionBlock(0.0);
            }
            
            return YES;
            
        }
        else
        {
            if (completionBlock)
            {
                completionBlock([friendAnimating[@"animationStarted"] doubleValue]);
            }
            
            return NO;
        }
    }
    else
    {
        [self.friendsAnimating addObject:[self friendAnimatingWithId:friendID]];
        
        if (completionBlock)
        {
            completionBlock(0.0);
        }

        if (!animateOnly)
        {
            [self sendTinkToFriendId:friendID];
        }
        
        return YES;

    }
    
    return NO;
}

- (void)sendTinkToFriendId:(NSString *)friendID
{
    [[TinkchatManager sharedInstance] sendTinkToUserWithId:friendID
    completion:^(TinkSendJSON *tinkJSON, ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            NSLog(@"tink sent");
        }
        else
        {
            [errorJSON showAlertView];
        }
    }];
}

- (NSNumber *)indexOfFriendId:(NSString *)friendId
{
    for (NSString *friendIDFromList in self.friendsIDs)
    {
        if ([friendIDFromList isEqualToString:friendId])
        {
            return @([self.friendsIDs indexOfObject:friendIDFromList]);
        }
    }
    return nil;
}

- (BOOL)isCellAnimatingForFriendID:(NSString *)friendId
{
    NSArray *friendIdAnimatingArray = [self.friendsAnimating filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.userId like %@", friendId]];
    
    if (friendIdAnimatingArray.count > 0)
    {
        NSDictionary *friendAnimating = [friendIdAnimatingArray firstObject];
        NSTimeInterval animationStarted = [friendAnimating[@"animationStarted"] doubleValue];
        
        NSTimeInterval elapsedTime = CACurrentMediaTime() - animationStarted;
        
        if (elapsedTime < tinkAnimationTime)
        {
            return YES;
        }
    }
    return NO;
}

- (NSDictionary *)friendAnimatingWithId:(NSString *)friendId
{
    return @{@"userId" : friendId,
             @"animationStarted" : @(CACurrentMediaTime())};
}

@end
