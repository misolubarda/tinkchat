//
//  TinksListJSON.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 19/01/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "TinksListJSON.h"

@interface TinksListJSON ()

@end

@implementation TinksListJSON

- (void)updateWithNewTinkList:(TinksListJSON *)newTinkList completionBlock:(void (^)(NSArray *, NSArray *))completionBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        NSMutableArray *indexesToRemove = [NSMutableArray array];
        NSMutableArray *indexesToAdd = [NSMutableArray array];
        
        NSMutableArray *newTinkIDs = [NSMutableArray array];
        for (TinkOnListJSON *aTink in newTinkList.tinks)
        {
            [newTinkIDs addObject:aTink.tink_id];
        }
        
        NSMutableArray *existingTinkIDs = [NSMutableArray array];
        for (TinkOnListJSON *aTink in self.tinks)
        {
            [existingTinkIDs addObject:aTink.tink_id];
        }
        
        for (NSString *tinkID in newTinkIDs)
        {
            if (![existingTinkIDs containsObject:tinkID])
            {
                NSUInteger anIndex = [newTinkIDs indexOfObject:tinkID];
                [indexesToAdd addObject:@(anIndex)];
            }
        }
        
        for (NSString *tinkID in existingTinkIDs)
        {
            if (![newTinkIDs containsObject:tinkID])
            {
                NSUInteger anIndex = [existingTinkIDs indexOfObject:tinkID];
                [indexesToRemove addObject:@(anIndex)];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^
        {
            self.tinks = newTinkList.tinks;
            completionBlock(indexesToAdd, indexesToRemove);
        });
    });
}

@end
