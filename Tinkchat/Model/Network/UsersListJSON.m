//
//  UsersListJSON.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 02/05/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "UsersListJSON.h"

@implementation UsersListJSON

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"users" : @"friends"}];
}

@end
