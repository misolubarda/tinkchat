//
//  FriendJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 13.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"

@protocol FriendJSON
@end

@interface FriendJSON : JSONModel

@property (nonatomic, strong) NSString<Optional> *alternative_id;
@property (nonatomic, strong) NSString<Optional> *user_id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSDate<Optional> *last_tink_created_at;
@property (nonatomic, strong) NSNumber<Optional> *amount_to;      // Number of tinks you've sent
@property (nonatomic, strong) NSNumber<Optional> *amount_from;    // Number of tinks you've recieved

//Ignored properties
@property (nonatomic) NSTimeInterval animationTime;

@end
