//
//  PushNotificationJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 26/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"
#import "PushNotificationApsJSON.h"

@interface PushNotificationJSON : MLMainJSONModel
@property (nonatomic, strong) PushNotificationApsJSON *aps;

@end
