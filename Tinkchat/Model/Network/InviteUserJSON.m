//
//  InviteUserJSON.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 31/05/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "InviteUserJSON.h"

@implementation InviteUserJSON

- (void)setType:(NSString<Optional> *)type
{
    _type = type;
    
    if ([self.type isEqualToString:@"invited_friend"])
    {
        self.invitationType = @(InviteUserJSONTypeInvited);
    }
    else if([self.type isEqualToString:@"existing_friend"])
    {
        self.invitationType = @(InviteUserJSONTypeFriendAdded);
    }
}

- (void)setName:(NSString<Optional> *)name
{
    _name = name;
    self.fullname = name;
}

@end
