//
//  SignInJSON.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 11.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "SignInJSON.h"

@implementation SignInJSON

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"name" : @"fullname"}];
}

@end
