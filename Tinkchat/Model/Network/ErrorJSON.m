//
//  ErrorJSON.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 10.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "ErrorJSON.h"

@implementation ErrorJSON

- (void)setErrorCode:(NSNumber *)errorCode
{
    _errorCode = errorCode;
    
    self.shouldShowToUser = [self shouldShowToUserByDefault];
}

- (void)setEndpointType:(EndpointType)endpointType
{
    _endpointType = endpointType;
    
    self.shouldShowToUser = [self shouldShowToUserByDefault];
}

- (BOOL)shouldShowToUserByDefault
{
    return [NetworkConstants shouldShowErrorToUserByDefault:self];
}

+ (instancetype)initWithData:(id)data
{
    //
    // We don't need JSON parsing error handling, MCBError is self explanatory
    //
    
    ErrorJSON *errorJSON;
    
    if ([data isKindOfClass:[NSDictionary class]])
    {
        errorJSON = [[ErrorJSON alloc] initWithDictionary:data error:nil];
    }
    else if ([data isKindOfClass:[NSString class]])
    {
        errorJSON = [[ErrorJSON alloc] initWithString:data error:nil];
    }
    else
    {
        errorJSON = [[ErrorJSON alloc] initWithData:data error:nil];
    }
    
    return errorJSON;
}

//
// Http success and parsing response error
//
+ (instancetype)initWithJSONParsingErrorInMethod:(const char *)methodName error:(NSError *)error
{
    ErrorJSON *errorJSON = [[ErrorJSON alloc] init];
    
    errorJSON.errorCode = @(ErrorCodeGeneral_ParsingResponse);
    errorJSON.errorMessage = @"Something went wrong.";
    errorJSON.errorDescription = [NSString stringWithFormat:@"JSON parsing error in method: %s, error description: %@", methodName, error.description];
    
    NSLog(@"error: %@", errorJSON.errorDescription);
    
    return errorJSON;
}

+ (instancetype)initWithCustomMessage:(NSString *)message shouldAlertUser:(BOOL)shoudAlert
{
    ErrorJSON *error = [[ErrorJSON alloc] init];
    
    error.errorCode = shoudAlert ? @(ErrorCodeGeneral_CustomForUser) : @(ErrorCodeGeneral_CustomInternal);
    error.errorMessage = message;
    
    if (error.errorCode.integerValue == ErrorCodeGeneral_CustomInternal)
    {
        NSLog(@"error: %@", message);
    }
    
    return error;
}

+ (instancetype)initWithNetworkError:(NSError *)error
{
    ErrorJSON *errorJSON;
    
    if (error.userInfo[@"NSLocalizedDescription"])
    {
        errorJSON = [ErrorJSON initWithCustomMessage:error.userInfo[@"NSLocalizedDescription"] shouldAlertUser:YES];
    }
    else
    {
        errorJSON = [ErrorJSON initWithCustomMessage:@"Network error" shouldAlertUser:YES];
    }
    
    return errorJSON;
}

- (void)showAlertView
{
    if (self.shouldShowToUser || [self shouldShowToUserByDefault])
    {
        if (self.errorCode.integerValue > 0)
        {
            if (self.errorCode.integerValue != ErrorCodeGeneral_ParsingError &&
                self.errorCode.integerValue != ErrorCodeGeneral_CustomInternal &&
                self.errorCode.integerValue != ErrorCodeGeneral_ParsingResponse)
            {
                [[[UIAlertView alloc] initWithTitle:@"Tinkchat" message:self.errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else if (self.errorCode.integerValue == ErrorCodeGeneral_AuthTokenInvalid)
            {
                [[[UIAlertView alloc] initWithTitle:@"Tinkchat" message:@"Something went wrong." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"Tinkchat" message:@"Something went wrong." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        }
    }
}

+ (BOOL)propertyIsIgnored:(NSString *)propertyName
{
    if ([propertyName isEqualToString:@"shouldShowToUser"] ||
        [propertyName isEqualToString:@"endpointType"])
    {
        return YES;
    }
    
    return NO;
}

@end
