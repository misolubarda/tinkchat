//
//  FriendsListJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 14.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"
#import "FriendJSON.h"
#import "FriendCell.h"

@interface FriendsListJSON : JSONModel

@property (nonatomic, strong) NSArray<FriendJSON> *friends;

- (void)updateWithNewFriendList:(FriendsListJSON *)newFriendList dispatchAsync:(BOOL)async completionBlock:(void(^)(NSArray *indexesToAdd, NSArray *indexesToRemove, NSArray *indexesToReload))completionBlock;
- (BOOL)containsUserID:(NSString *)userID;

- (NSNumber *)indexOfFriendId:(NSString *)friendId;

- (BOOL)isCellAnimatingForFriendID:(NSString *)friendId;

/**
 *  This method sends tink to user, if user cell is not animating already. Method returns bool if tink was actually sent to user.
 *
 *  @param friendID        ID of a user
 *  @param animateOnly     If animateOnly is true, no tink will be sent.
 *  @param completionBlock Returning animation time elapsed, from where to continue animating.
 *
 *  @return Returns YES if tink was actually sent, otherwise NO.
 */
- (BOOL)sendTinkToFriendID:(NSString *)friendID notSendButAnimateOnly:(BOOL)animateOnly completionBlock:(void(^)(NSTimeInterval pastStartTime))completionBlock;

@end
