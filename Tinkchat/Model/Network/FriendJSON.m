//
//  FriendJSON.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 13.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "FriendJSON.h"

@implementation FriendJSON

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id" : @"alternative_id"}];
}

- (void)setAlternative_id:(NSString<Optional> *)alternative_id
{
    _alternative_id = alternative_id;
    
    _user_id = alternative_id;
}

+ (BOOL)propertyIsIgnored:(NSString *)propertyName
{
    if ([propertyName isEqualToString:@"animationTime"])
    {
        return YES;
    }
    
    return NO;
}

@end
