//
//  EditUserJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 04/06/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "UserJSON.h"

@interface EditUserJSON : UserJSON

@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *email_visibility;

@end
