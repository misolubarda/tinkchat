//
//  AddUserJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 17.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"

@interface AddUserJSON : JSONModel

@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSString *friend_id;

@end
