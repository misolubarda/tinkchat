//
//  AnalyticsHelper.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 29/07/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ErrorJSON.h"

@interface AnalyticsHelper : NSObject

+ (void)trackLogin:(BOOL)success error:(ErrorJSON *)error;
+ (void)trackSignUp:(BOOL)success error:(ErrorJSON *)error;
+ (void)trackSearchWithQuery:(NSString *)query success:(BOOL)success error:(ErrorJSON *)error;
+ (void)trackInviteEmail:(NSString *)email success:(BOOL)success error:(ErrorJSON *)error;
+ (void)trackInvalidStateWithMessage:(NSString *)message inMethod:(const char *)method;

@end
