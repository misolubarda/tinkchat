//
//  AnalyticsHelper.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 29/07/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "AnalyticsHelper.h"

#import <Answers.h>

@implementation AnalyticsHelper

static NSString* const kSearchForUserEvent = @"searchForUser";
static NSString* const kInviteWithEmailEvent = @"inviteWithEmail";
//static NSString* const searchForUserEvent = @"searchForUser";
//static NSString* const searchForUserEvent = @"searchForUser";
//static NSString* const searchForUserEvent = @"searchForUser";

+ (void)trackSignUp:(BOOL)success error:(ErrorJSON *)error
{
    NSMutableDictionary *attributes = [self attributesFromError:error];
    attributes[@"success"] = @(success);
    
    [Answers logSignUpWithMethod:@"email" success:@(success) customAttributes:attributes];
}

+ (void)trackLogin:(BOOL)success error:(ErrorJSON *)error
{
    NSMutableDictionary *attributes = [self attributesFromError:error];
    attributes[@"success"] = @(success);
    
    [Answers logLoginWithMethod:@"email" success:@(success) customAttributes:attributes];
}

+ (void)trackSearchWithQuery:(NSString *)query success:(BOOL)success error:(ErrorJSON *)error
{
    NSMutableDictionary *attributes = [self attributesFromError:error];
    attributes[@"success"] = @(success);

    if (query.length > 0)
    {
        attributes[@"query"] = query;
    }
    
    [Answers logCustomEventWithName:kSearchForUserEvent customAttributes:attributes];
}

+ (void)trackInviteEmail:(NSString *)email success:(BOOL)success error:(ErrorJSON *)error
{
    NSMutableDictionary *attributes = [self attributesFromError:error];
    attributes[@"success"] = @(success);
    
    if (email.length > 0)
    {
        attributes[@"email"] = email;
    }
    
    [Answers logCustomEventWithName:kInviteWithEmailEvent customAttributes:attributes];
}

+ (void)trackInvalidStateWithMessage:(NSString *)message inMethod:(const char *)method
{
    if (message.length > 0)
    {
        NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
        attributes[@"invalidStateInAppWithComment"] = message;
        
        NSString *methodName = [NSString stringWithFormat:@"%s", method];
        if (methodName.length > 0)
        {
            attributes[@"inMethod"] = methodName;
        }
        
        [Answers logCustomEventWithName:kInviteWithEmailEvent customAttributes:attributes];
    }
}

+ (NSMutableDictionary *)attributesFromError:(ErrorJSON *)error
{
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    
    if (error.errorMessage.length > 0)
    {
        attributes[@"errorMessage"] = error.errorMessage;
    }
    if (error.errorDescription.length > 0)
    {
        attributes[@"errorDescription"] = error.errorDescription;
    }
    
    return attributes;
}


@end
