//
//  AddressBookUsersListJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 17.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"
#import "AddressBookUserJSON.h"

@interface AddressBookUsersListJSON : JSONModel

@property (nonatomic, strong) NSArray<AddressBookUserJSON> *addressBookUsersJSON;

@end
