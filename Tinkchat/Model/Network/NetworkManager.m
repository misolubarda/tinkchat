//
//  TinkchatNetworkManager.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 08.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "NetworkManager.h"
#import "KeychainManager.h"
#import "MLNetworkManager.h"


@interface NetworkManager ()

@property (nonatomic, strong) NSTimer *authTokenTimer;

@end

@implementation NetworkManager

- (ErrorJSON *)isErrorInResponse:(NSData *)responseObject
{
    ErrorJSON *errorJSON = [ErrorJSON initWithData:responseObject];
    
    return errorJSON;
}

- (NSInvocation *)invocationFromRequestMethod:(SEL)methodSelector parameters:(NSDictionary *)parameters completionBlockParameter:(id)completionBlockParameter
{
    NSMethodSignature *aMethodSignature = [NetworkManager instanceMethodSignatureForSelector:methodSelector];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:aMethodSignature];
    invocation.target = self;
    invocation.selector = methodSelector;
    [invocation setArgument:&parameters atIndex:2];
    [invocation setArgument:&completionBlockParameter atIndex:3];
    
    return invocation;
}

- (BOOL)renewTokenIfExpiredError:(ErrorJSON *)errorJSON andInvokeMethod:(SEL)method withParameters:(NSDictionary *)parameters completionBlock:(id)completionBlock
{
    if (errorJSON.errorCode.integerValue == ErrorCodeGeneral_AuthTokenInvalid)
    {
        NSDictionary *postDictionary = [self.delegate networkManagerPostDictionaryForSignInRequest];
        
        if (!postDictionary)
        {
            /**
             * Cannot build valid postDictionary due to uknown password or other data
             * Should return YES to preven calling completion block in caller method
             */
            [[[UIAlertView alloc] initWithTitle:@"Tinkchat" message:@"Something went wrong. Please sing out and sing in again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
            return YES;
        }
        
        /**
         *  If authentication token doesn't exist, token renewal is NOT permitted
         */
        if ([KeychainManager loginType] == LoginTypeEmail)
        {
            [self signInUserWithParameters:postDictionary
            completion:^(SignInJSON *signInJSON, ErrorJSON *errorJSON)
             {
                 if (!errorJSON && signInJSON)
                 {
                     [self invokeMethod:method parameters:parameters authToken:signInJSON.auth_token completionBlock:completionBlock];
                 }
                 else
                 {
                     /**
                      * Error when getting new authorization token could happen if password was changed by user but wasn't saved because of the network issue.
                      */
                     [[[UIAlertView alloc] initWithTitle:@"Tinkchat" message:@"Something went wrong. Please sing out and sing in again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                 }
             }];
        }
        else if ([KeychainManager loginType] == LoginTypeFacebook)
        {
            [self signInUserWithFacebookWithParameters:postDictionary
            completion:^(SignInJSON *signInJSON, ErrorJSON *errorJSON) {
                if (!errorJSON && signInJSON)
                {
                    [self invokeMethod:method parameters:parameters authToken:signInJSON.auth_token completionBlock:completionBlock];
                }
                else
                {
                    /**
                     * Error when getting new authorization token could happen if password was changed by user but wasn't saved because of the network issue.
                     */
                    [[[UIAlertView alloc] initWithTitle:@"Tinkchat" message:@"Something went wrong. Please sing out and sing in again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }
            }];
        }
        else
        {
            return NO;
        }
        
        
        return YES;
    }

    return NO;
}


- (void)invokeMethod:(SEL)method parameters:(NSDictionary *)parameters authToken:(NSString *)authToken completionBlock:(id)completionBlock
{
    /**
     *  Replacing atuh_token for request that is to be invoked
     */
    [KeychainManager saveAuthenticationToken:authToken];
    NSMutableDictionary *parametersMutalbeCopy = [parameters mutableCopy];
    NSMutableDictionary *headerDictionary = [[NSMutableDictionary alloc] initWithDictionary:parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]]];
    headerDictionary[[NetworkConstants apiKeyForType:NetworkManagerAPIKey_General_AuthToken]] = authToken;
    parametersMutalbeCopy[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]] = headerDictionary;
    
    /**
     *  Reinvoking method
     */
    NSInvocation *invocation = [self invocationFromRequestMethod:method parameters:parametersMutalbeCopy completionBlockParameter:completionBlock];
    [invocation invoke];
}

- (void)signUpUserWithParameters:(NSDictionary *)parameters completion:(void (^)(SignUpJSON *, ErrorJSON *))completionBlock
{
    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithDictionary:parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody]]];
    
    if (!bodyDictionary[[NetworkConstants apiKeyForType:NetworkManagerAPIKey_General_PushToken]]) {
        bodyDictionary[[NetworkConstants apiKeyForType:NetworkManagerAPIKey_General_PushToken]] = @"";
    }
    
    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    [manager POST:[NetworkConstants relativeUrlForEndpoint:EndpointTypeSingUp] headerParameters:nil bodyParameters:bodyDictionary
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeSingUp;
        if (completionBlock)
        {
            if (!errorInResponse)
            {
                completionBlock(nil, nil);
            }
            else
            {
                completionBlock(nil, errorInResponse);
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeSingUp;
        if (completionBlock)
        {
            completionBlock(nil, errorJSON);
        }
    }];

}

- (void)signInUserWithParameters:(NSDictionary *)parameters completion:(void (^)(SignInJSON *, ErrorJSON *))completionBlock
{
    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithDictionary:parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody]]];
    
    if (!bodyDictionary[[NetworkConstants apiKeyForType:NetworkManagerAPIKey_General_PushToken]]) {
        bodyDictionary[[NetworkConstants apiKeyForType:NetworkManagerAPIKey_General_PushToken]] = @"";
    }

    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    
    [manager POST:[NetworkConstants relativeUrlForEndpoint:EndpointTypeSingIn] headerParameters:nil bodyParameters:bodyDictionary
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeSingIn;
        
        if (!errorInResponse)
        {
            NSError *jsonError;
            SignInJSON *signInJSON = [[SignInJSON alloc] initWithData:data error:&jsonError];
            if (!jsonError)
            {
                if (completionBlock)
                {
                    completionBlock(signInJSON, nil);
                }
            }
            else
            {
                ErrorJSON *errorJSON = [ErrorJSON initWithJSONParsingErrorInMethod:__PRETTY_FUNCTION__ error:jsonError];
                
                if (completionBlock)
                {
                    completionBlock(nil, errorJSON);
                }
            }
        }
        else
        {
            if (completionBlock)
            {
                completionBlock(nil, errorInResponse);
            }
        }

    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeSingIn;
        if (completionBlock)
        {
            completionBlock(nil, errorJSON);
        }
    }];
}

- (void)signInUserWithFacebookWithParameters:(NSDictionary *)parameters completion:(void (^)(SignInJSON *, ErrorJSON *))completionBlock
{
    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithDictionary:parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody]]];
    
    if (!bodyDictionary[[NetworkConstants apiKeyForType:NetworkManagerAPIKey_General_PushToken]]) {
        bodyDictionary[[NetworkConstants apiKeyForType:NetworkManagerAPIKey_General_PushToken]] = @"";
    }
    
    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    
    [manager POST:[NetworkConstants relativeUrlForEndpoint:EndpointTypeSignInFacebook] headerParameters:nil bodyParameters:bodyDictionary
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeSignInFacebook;
        
        if (!errorInResponse)
        {
            NSError *jsonError;
            SignInJSON *signInJSON = [[SignInJSON alloc] initWithData:data error:&jsonError];
            
            if (!jsonError)
            {
                if (completionBlock)
                {
                    completionBlock(signInJSON, nil);
                }
            }
            else
            {
                ErrorJSON *errorJSON = [ErrorJSON initWithJSONParsingErrorInMethod:__PRETTY_FUNCTION__ error:jsonError];
                
                if (completionBlock)
                {
                    completionBlock(nil, errorJSON);
                }
            }
        }
        else
        {
            if (completionBlock)
            {
                completionBlock(nil, errorInResponse);
            }
        }
        
    }
          failure:^(NSURLResponse *response, NSError *error)
     {
         ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
         errorJSON.endpointType = EndpointTypeSingIn;
         if (completionBlock)
         {
             completionBlock(nil, errorJSON);
         }
     }];
}

- (void)editUserData:(NSDictionary *)parameters completion:(void (^)(EditUserJSON *, ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithDictionary:parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody]]];
    
    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    [manager POST:[NetworkConstants relativeUrlForEndpoint:EndpointTypeEditUserData] headerParameters:headerParameters bodyParameters:bodyDictionary
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeEditUserData;
        if (!errorInResponse)
        {
            NSError *jsonError;
            EditUserJSON *editUserJSON = [[EditUserJSON alloc] initWithData:data error:&jsonError];
            if (!jsonError)
            {
                if (completionBlock)
                {
                    completionBlock(editUserJSON, nil);
                }
            }
            else
            {
                ErrorJSON *errorJSON = [ErrorJSON initWithJSONParsingErrorInMethod:__PRETTY_FUNCTION__ error:jsonError];
                
                if (completionBlock)
                {
                    completionBlock(nil, errorJSON);
                }
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(nil, errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeEditUserData;
        if (completionBlock)
        {
            completionBlock(nil, errorJSON);
        }
    }];
}

- (void)changePassword:(NSDictionary *)parameters completion:(void (^)(ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithDictionary:parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody]]];
    
    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    [manager POST:[NetworkConstants relativeUrlForEndpoint:EndpointTypePasswordChange] headerParameters:headerParameters bodyParameters:bodyDictionary
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypePasswordChange;
        if (!errorInResponse)
        {
            if (completionBlock)
            {
                completionBlock(nil);
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypePasswordChange;
        if (completionBlock)
        {
            completionBlock(errorJSON);
        }
    }];
}

- (void)resetPassword:(NSDictionary *)parameters completion:(void (^)(ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithDictionary:parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody]]];
    
    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    [manager POST:[NetworkConstants relativeUrlForEndpoint:EndpointTypePasswordReset] headerParameters:headerParameters bodyParameters:bodyDictionary
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypePasswordReset;
        if (!errorInResponse)
        {
            if (completionBlock)
            {
                completionBlock(nil);
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypePasswordReset;
        if (completionBlock)
        {
            completionBlock(errorJSON);
        }
    }];
}

- (void)friendsListWithParameters:(NSDictionary *)parameters completion:(void (^)(FriendsListJSON *, ErrorJSON *))completionBlock
{    
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    
    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    
    [manager GET:[NetworkConstants relativeUrlForEndpoint:EndpointTypeFriendsList] headerParameters:headerParameters
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeFriendsList;
        
        if (!errorInResponse)
        {
            NSError *jsonError;
            FriendsListJSON *friendsListJSON = [[FriendsListJSON alloc] initWithData:data error:&jsonError];
            if (!jsonError)
            {
                if (completionBlock)
                {
                    completionBlock(friendsListJSON, nil);
                }
            }
            else
            {
                ErrorJSON *errorJSON = [ErrorJSON initWithJSONParsingErrorInMethod:__PRETTY_FUNCTION__ error:jsonError];
                
                if (completionBlock)
                {
                    completionBlock(nil, errorJSON);
                }
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(nil, errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeFriendsList;
        if (completionBlock)
        {
            completionBlock(nil, errorJSON);
        }
    }];
}

- (void)findTinkchatUserWithParameters:(NSDictionary *)parameters completion:(void (^)(UsersListJSON *, ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    NSDictionary *bodyParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody]];

    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    
    [manager POST:[NetworkConstants relativeUrlForEndpoint:EndpointTypeFindTinkchatUser] headerParameters:headerParameters bodyParameters:bodyParameters
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeFindTinkchatUser;
        
        if (!errorInResponse)
        {
            NSError *jsonError;
            UsersListJSON *usersListJSON = [[UsersListJSON alloc] initWithData:data error:&jsonError];
            if (!jsonError)
            {
                if (completionBlock)
                {
                    completionBlock(usersListJSON, nil);
                }
            }
            else
            {
                ErrorJSON *errorJSON = [ErrorJSON initWithJSONParsingErrorInMethod:__PRETTY_FUNCTION__ error:jsonError];
                
                if (completionBlock)
                {
                    completionBlock(nil, errorJSON);
                }
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(nil, errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeFindTinkchatUser;
        if (completionBlock)
        {
            completionBlock(nil, errorJSON);
        }
    }];
}

- (void)addTinkchatUserWithParameters:(NSDictionary *)parameters completion:(void (^)(AddUserJSON *, ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    NSDictionary *bodyParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody]];

    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    
    [manager POST:[NetworkConstants relativeUrlForEndpoint:EndpointTypeAddTinkchatUser] headerParameters:headerParameters bodyParameters:bodyParameters
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeAddTinkchatUser;
        
        if (!errorInResponse)
        {
            NSError *jsonError;
            AddUserJSON *addUserJSON = [[AddUserJSON alloc] initWithData:data error:&jsonError];
            if (!jsonError)
            {
                if (completionBlock)
                {
                    completionBlock(addUserJSON, nil);
                }
            }
            else
            {
                ErrorJSON *errorJSON = [ErrorJSON initWithJSONParsingErrorInMethod:__PRETTY_FUNCTION__ error:jsonError];
                
                if (completionBlock)
                {
                    completionBlock(nil, errorJSON);
                }
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(nil, errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeAddTinkchatUser;
        if (completionBlock)
        {
            completionBlock(nil, errorJSON);
        }
    }];
}

- (void)removeTinkchatUserWithParameters:(NSDictionary *)parameters completion:(void (^)(ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    NSDictionary *otherParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyOther]];
    
    NSString *userIdPathComponent = otherParameters[[NetworkConstants apiKeyForType:NetworkManagerAPIKey_RemoveTinkchatUser_UserId]];
    NSString *relativePath = [[NetworkConstants relativeUrlForEndpoint:EndpointTypeRemoveTinkchatUser] stringByAppendingPathComponent:userIdPathComponent];
    
    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    
    [manager DELETE:relativePath headerParameters:headerParameters
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeRemoveTinkchatUser;
        
        if (!errorInResponse)
        {
            if (completionBlock)
            {
                completionBlock(nil);
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeRemoveTinkchatUser;
        if (completionBlock)
        {
            completionBlock(errorJSON);
        }
    }];
}

- (void)usersAddedMeListWithParameters:(NSDictionary *)parameters completion:(void (^)(FriendsListJSON *, ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    
    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    
    [manager GET:[NetworkConstants relativeUrlForEndpoint:EndpointTypeUsersAddedMe] headerParameters:headerParameters
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeUsersAddedMe;
        
        if (!errorInResponse)
        {
            NSError *jsonError;
            FriendsListJSON *friendsListJSON = [[FriendsListJSON alloc] initWithData:data error:&jsonError];
            if (!jsonError)
            {
                if (completionBlock)
                {
                    completionBlock(friendsListJSON, nil);
                }
            }
            else
            {
                ErrorJSON *errorJSON = [ErrorJSON initWithJSONParsingErrorInMethod:__PRETTY_FUNCTION__ error:jsonError];
                
                if (completionBlock)
                {
                    completionBlock(nil, errorJSON);
                }
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(nil, errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeUsersAddedMe;
        if (completionBlock)
        {
            completionBlock(nil, errorJSON);
        }
    }];
}

- (void)inviteWithParameters:(NSDictionary *)parameters completion:(void (^)(InviteUserJSON *, ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    NSDictionary *bodyParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody]];
    
    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    
    [manager POST:[NetworkConstants relativeUrlForEndpoint:EndpointTypeInvite] headerParameters:headerParameters bodyParameters:bodyParameters
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeInvite;
        
        if (!errorInResponse)
        {
            NSError *jsonError;
            InviteUserJSON *inviteUserJSON = [[InviteUserJSON alloc] initWithData:data error:&jsonError];
            if (!jsonError)
            {
                if (completionBlock)
                {
                    completionBlock(inviteUserJSON, nil);
                }
            }
            else
            {
                ErrorJSON *errorJSON = [ErrorJSON initWithJSONParsingErrorInMethod:__PRETTY_FUNCTION__ error:jsonError];
                
                if (completionBlock)
                {
                    completionBlock(nil, errorJSON);
                }
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(nil, errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeInvite;
        if (completionBlock)
        {
            completionBlock(nil, errorJSON);
        }
    }];
}

- (void)tinksListWithParameters:(NSDictionary *)parameters completion:(void (^)(TinksListJSON *, ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];

    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    
    [manager GET:[NetworkConstants relativeUrlForEndpoint:EndpointTypeTinksList] headerParameters:headerParameters
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeTinksList;
        
        if (!errorInResponse)
        {
            NSError *jsonError;
            TinksListJSON *tinksListJSON = [[TinksListJSON alloc] initWithData:data error:&jsonError];
            if (!jsonError)
            {
                if (completionBlock)
                {
                    completionBlock(tinksListJSON, nil);
                }
            }
            else
            {
                ErrorJSON *errorJSON = [ErrorJSON initWithJSONParsingErrorInMethod:__PRETTY_FUNCTION__ error:jsonError];
                
                if (completionBlock)
                {
                    completionBlock(nil, errorJSON);
                }
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(nil, errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeTinksList;
        if (completionBlock)
        {
            completionBlock(nil, errorJSON);
        }
    }];
}

- (void)sendTinkToUserWithParameters:(NSDictionary *)parameters completion:(void (^)(TinkSendJSON *, ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    NSDictionary *bodyParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody]];

    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    
    [manager POST:[NetworkConstants relativeUrlForEndpoint:EndpointTypeSendTink] headerParameters:headerParameters bodyParameters:bodyParameters
    success:^(NSURLResponse *response, NSData *data)
     {
         ErrorJSON *errorInResponse = [self isErrorInResponse:data];
         errorInResponse.endpointType = EndpointTypeSendTink;
         
         if (!errorInResponse)
         {
             NSError *jsonError;
             TinkSendJSON *tinkJSON = [[TinkSendJSON alloc] initWithData:data error:&jsonError];
             if (!jsonError)
             {
                 if (completionBlock)
                 {
                     completionBlock(tinkJSON, nil);
                 }
             }
             else
             {
                 ErrorJSON *errorJSON = [ErrorJSON initWithJSONParsingErrorInMethod:__PRETTY_FUNCTION__ error:jsonError];
                 
                 if (completionBlock)
                 {
                     completionBlock(nil, errorJSON);
                 }
             }
         }
         else
         {
             /**
              *  If renewing token DO NOT call completionBlock
              */
             if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
             {
                 if (completionBlock)
                 {
                     completionBlock(nil, errorInResponse);
                 }
             }
         }
     }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeSendTink;
        if (completionBlock)
        {
            completionBlock(nil, errorJSON);
        }
    }];
}

- (void)markTinkAsRead:(NSDictionary *)parameters completion:(void (^)(ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    NSDictionary *otherParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyOther]];
    
    NSString *tinkIdPathComponent = otherParameters[[NetworkConstants apiKeyForType:NetworkManagerAPIKey_MarkTink_tinkId]];
    NSString *relativePath = [[NetworkConstants relativeUrlForEndpoint:EndpointTypeMarkTinkRead] stringByAppendingPathComponent:tinkIdPathComponent];
    
    MLNetworkManager *manager = [[MLNetworkManager alloc] init];

    [manager DELETE:relativePath headerParameters:headerParameters
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeMarkTinkRead;
         
        if (!errorInResponse)
        {
            if (completionBlock)
            {
                completionBlock(nil);
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeMarkTinkRead;
        if (completionBlock)
        {
            completionBlock(errorJSON);
        }
    }];
}

- (void)sendFeedback:(NSDictionary *)parameters completion:(void (^)(ErrorJSON *))completionBlock
{
    NSDictionary *headerParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader]];
    NSDictionary *bodyParameters = parameters[[NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody]];
    
    MLNetworkManager *manager = [[MLNetworkManager alloc] init];
    
    [manager POST:[NetworkConstants relativeUrlForEndpoint:EndpointTypeSendFeedback] headerParameters:headerParameters bodyParameters:bodyParameters
    success:^(NSURLResponse *response, NSData *data)
    {
        ErrorJSON *errorInResponse = [self isErrorInResponse:data];
        errorInResponse.endpointType = EndpointTypeSendFeedback;
        
        if (!errorInResponse)
        {
            if (completionBlock)
            {
                completionBlock(nil);
            }
        }
        else
        {
            /**
             *  If renewing token DO NOT call completionBlock
             */
            if (![self renewTokenIfExpiredError:errorInResponse andInvokeMethod:_cmd withParameters:parameters completionBlock:completionBlock])
            {
                if (completionBlock)
                {
                    completionBlock(errorInResponse);
                }
            }
        }
    }
    failure:^(NSURLResponse *response, NSError *error)
    {
        ErrorJSON *errorJSON = [ErrorJSON initWithNetworkError:error];
        errorJSON.endpointType = EndpointTypeSendFeedback;
        if (completionBlock)
        {
            completionBlock(errorJSON);
        }
    }];
}

@end
