//
//  FriendJSON.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 22.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "AddressBookUserJSON.h"

@implementation AddressBookUserJSON

- (NSString<Ignore> *)fullName
{
    if (!_fullName)
    {
        if (self.firstName)
        {
            if (self.lastName)
            {
                _fullName = [self.firstName stringByAppendingFormat:@" %@", self.lastName];
            }
            else
            {
                _fullName = (NSString<Ignore> *)self.firstName;
            }
        }
        else if (self.lastName)
        {
            _fullName = (NSString<Ignore> *)self.lastName;
        }
        else
        {
            _fullName = (NSString<Ignore> *)self.email0;
        }
    }
    return _fullName;
}

- (void)inviteUser
{
    
}

- (void)saveToFriends
{
    
}

@end
