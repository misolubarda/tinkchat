//
//  TinkJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 19/01/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"

@protocol TinkJSON
@end

@interface TinkSendJSON : JSONModel

@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSNumber *read;
@property (nonatomic, strong) NSString *recipient_id;

@end
