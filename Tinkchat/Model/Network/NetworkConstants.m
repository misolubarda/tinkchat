//
//  NetworkConstants.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 24/01/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "NetworkConstants.h"
#import "ErrorJSON.h"

@implementation NetworkConstants

+ (NSString *)relativeUrlForEndpoint:(EndpointType)endpoint
{
    switch (endpoint)
    {
        case EndpointTypeSingUp:
            return @"/api/v1/users/sign_up/";
        case EndpointTypeSingIn:
            return @"/api/v1/users/sign_in/";
        case EndpointTypeSignInFacebook:
            return @"/api/v1/users/fb_connect/";
        case EndpointTypeEditUserData:
            return @"/api/v1/users/edit/";
        case EndpointTypePasswordChange:
            return @"/api/v1/users/change_password/";
        case EndpointTypePasswordReset:
            return @"/api/v1/users/new_password";
        case EndpointTypeFriendsList:
            return @"/api/v1/friends/1";
        case EndpointTypeFindTinkchatUser:
            return @"/api/v1/users/find/";
        case EndpointTypeAddTinkchatUser:
            return @"/api/v1/friends/";
        case EndpointTypeRemoveTinkchatUser:
            return @"/api/v1/friends/";
        case EndpointTypeInvite:
            return @"/api/v1/friends/invite/";
        case EndpointTypeUsersAddedMe:
            return @"/api/v1/friends/2";
        case EndpointTypeTinksList:
            return @"/api/v1/tinks/";
        case EndpointTypeSendTink:
            return @"/api/v1/tinks/";
        case EndpointTypeMarkTinkRead:
            return @"/api/v1/tinks/";
        case EndpointTypeSendFeedback:
            return @"/api/v1/messages/";
        default:
            break;
    }
    
    return nil;
}

+ (NSString *)apiKeyForType:(NetworkManagerAPIKey)apiKeyType
{
    switch (apiKeyType) {
        case NetworkManagerAPIKey_General_AuthToken:
            return @"token";
        case NetworkManagerAPIKey_General_Email:
            return @"user[email]";
        case NetworkManagerAPIKey_General_Password:
            return @"user[password]";
        case NetworkManagerAPIKey_General_PushToken:
            return @"user[apns_token]";
        case NetworkManagerAPIKey_SignUp_Fullname:
            return @"user[name]";
        case NetworkManagerAPIKey_SignUp_Username:
            return @"user[username]";
        case NetworkManagerAPIKey_EditUser_RegistrationStatus:
            return @"user[registration_status]";
        case NetworkManagerAPIKey_PasswordOld:
            return @"old_password";
        case NetworkManagerAPIKey_PasswordNew:
            return @"new_password";
        case NetworkManagerAPIKey_SignInFacebook_AccessToken:
            return @"user[access_token]";
        case NetworkManagerAPIKey_FindTinkchatUser_SearchString:
            return @"keyword";
        case NetworkManagerAPIKey_AddTinkchatUser_UserId:
            return @"friend_id";
        case NetworkManagerAPIKey_SendTink_UserId:
            return @"tink[recipient_id]";
        case NetworkManagerAPIKey_MarkTink_tinkId:
            return @"tinkIdInURL";
        case NetworkManagerAPIKey_RemoveTinkchatUser_UserId:
            return @"userIdInURL";
        case NetworkManagerAPIKey_Invite_Email:
            return @"email";
        case NetworkManagerAPIKey_SendFeedback_Message:
            return @"message";
        case NetworkManagerAPIKey_SendFeedback_Subject:
            return @"subject";
        default:
            break;
    }
    
    return nil;
}

+ (NSString *)requestKeyForType:(NetworkManagerRequestKey)requestKeyType
{
    switch (requestKeyType) {
        case NetworkManagerRequestKeyHeader:
            return @"header";
        case NetworkManagerRequestKeyBody:
            return @"body";
        case  NetworkManagerRequestKeyOther:
            return @"other";
        default:
            break;
    }
}

+ (BOOL)shouldShowErrorToUserByDefault:(ErrorJSON *)errorJSON
{
    switch (errorJSON.errorCode.integerValue) {
        case ErrorCodeGeneral_ParsingError:
            return YES;
            break;
        case ErrorCodeGeneral_CustomForUser:
            return YES;
            break;
        case ErrorCodeGeneral_CustomInternal:
            return NO;
            break;
        case ErrorCodeGeneral_ParsingResponse:
            return YES;
            break;
        case ErrorCodeGeneral_AuthTokenInvalid:
            return YES;
            break;
        default:
            break;
    }
    
    switch (errorJSON.endpointType)
    {
        case EndpointTypeSingUp:
        {
            break;
        }
        case EndpointTypeSingIn:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                case ErrorCodeSignIn_AccountNotActivated:
                    return YES;
                    break;
                default:
                    break;
            }
            break;
        }
        case EndpointTypeSignInFacebook:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                default:
                    break;
            }
            break;
        }
        case EndpointTypeEditUserData:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                default:
                    break;
            }
            break;
        }
        case EndpointTypePasswordChange:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                default:
                    break;
            }
            break;
        }
        case EndpointTypePasswordReset:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                default:
                    break;
            }
            break;
        }
        case EndpointTypeSendTink:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                default:
                    break;
            }
            break;
        }
        case EndpointTypeTinksList:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                default:
                    break;
            }
            break;
        }
        case EndpointTypeFriendsList:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                default:
                    break;
            }
            break;
        }
        case EndpointTypeAddTinkchatUser:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                case ErrorCodeAddTinkchatUser_AlreadyAdded:
                    return YES;
                    break;
                case ErrorCodeAddTinkchatUser_AddingUnknownUser:
                    return YES;
                    break;
                default:
                    break;
            }
            break;
        }
        case EndpointTypeFindTinkchatUser:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                default:
                    break;
            }
            break;
        }
        case EndpointTypeRemoveTinkchatUser:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                case ErrorCodeRemoveTinkchatUser_RemovingUnknownUser:
                    return YES;
                    break;
                case ErrorCodeRemoveTinkchatUser_NotFriend:
                    return YES;
                    break;
                default:
                    break;
            }
            break;
        }
        case EndpointTypeInvite:
        {
            break;
        }
        case EndpointTypeUsersAddedMe:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                default:
                    break;
            }
            break;
        }
        case EndpointTypeMarkTinkRead:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                default:
                    break;
            }
            break;
        }
        case EndpointTypeSendFeedback:
        {
            switch (errorJSON.errorCode.integerValue)
            {
                case ErrorCodeSendFeedback_MessageRequired:
                    return YES;
                    break;
                case ErrorCodeSendFeedback_SubjectRequired:
                    return YES;
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
    
    return NO;
}

@end
