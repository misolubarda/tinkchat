//
//  TinkOnListJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 22/01/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"

@protocol TinkOnListJSON
@end

@interface TinkOnListJSON : JSONModel

@property (nonatomic, strong) NSString *sender_name;
@property (nonatomic, strong) NSString *sender_id;
@property (nonatomic, strong) NSString *tink_id;
@property (nonatomic, strong) NSNumber *color;
@property (nonatomic, assign) BOOL read;
@property (nonatomic, strong) NSDate *created_at;

@end
