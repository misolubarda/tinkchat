//
//  SignInJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 11.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "UserJSON.h"

@interface SignInJSON : UserJSON

@property (nonatomic, strong) NSString *auth_token;
@property (nonatomic, strong) NSString *fullname;
@property (nonatomic, strong) NSString *user_id;
@property (nonatomic) NSNumber<Optional> *email_visibility;

@end
