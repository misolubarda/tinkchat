//
//  ErrorJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 10.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

@import UIKit;

#import "JSONModel.h"
#import "NetworkConstants.h"


@interface ErrorJSON : JSONModel

@property (nonatomic, strong) NSNumber *errorCode;
@property (nonatomic, strong) NSString *errorMessage;
@property (nonatomic, strong) NSString<Ignore> *errorDescription;
@property (nonatomic, assign) EndpointType endpointType;    // Ignored JSONModel property
@property (nonatomic, assign) BOOL shouldShowToUser;        // Ignored JSONModel property

+ (instancetype)initWithData:(id)data;
+ (instancetype)initWithJSONParsingErrorInMethod:(const char *)methodName error:(NSError *)error;
+ (instancetype)initWithCustomMessage:(NSString *)message shouldAlertUser:(BOOL)shoudAlert;
+ (instancetype)initWithNetworkError:(NSError *)error;

- (void)showAlertView;

@end
