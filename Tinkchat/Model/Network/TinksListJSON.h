//
//  TinksListJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 19/01/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"
#import "TinkOnListJSON.h"

@interface TinksListJSON : JSONModel

@property (nonatomic, strong) NSArray<TinkOnListJSON, Optional> *tinks;

- (void)updateWithNewTinkList:(TinksListJSON *)newTinkList completionBlock:(void(^)(NSArray *indexesToAdd, NSArray *indexesToRemove))completionBlock;

@end
