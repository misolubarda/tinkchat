//
//  PushNotificationApsJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 26/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"

@interface PushNotificationApsJSON : MLMainJSONModel
@property (nonatomic, strong) NSString<Optional> *alert;
@property (nonatomic, strong) NSNumber<Optional> *badge;
@property (nonatomic, strong) NSString<Optional> *sound;
@end
