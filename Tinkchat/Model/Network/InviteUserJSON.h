//
//  InviteUserJSON.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 31/05/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"

typedef NS_ENUM(NSUInteger, InviteUserJSONType)
{
    InviteUserJSONTypeInvited,
    InviteUserJSONTypeFriendAdded
};

@interface InviteUserJSON : JSONModel

@property (nonatomic, strong) NSString<Optional> *type;
@property (nonatomic, strong) NSString<Optional> *email;
@property (nonatomic, strong) NSString<Optional> *name;

@property (nonatomic, strong) NSNumber<Ignore> *invitationType;
@property (nonatomic, strong) NSString<Ignore> *fullname;

@end
