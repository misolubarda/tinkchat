//
//  UserManager.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 12.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


#import "TinkchatManager.h"
#import "NetworkManager.h"
#import "KeychainManager.h"
#import "PushNotificationJSON.h"
#import "OnboardingManager.h"

@interface TinkchatManager () <NetworkManagerDataSource, UIAlertViewDelegate>
@end

@implementation TinkchatManager

#pragma mark - Request Constructors

- (NSMutableDictionary *)postDictionaryDefault
{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]
                                     initWithDictionary:@{[NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader] : [NSMutableDictionary new],
                                                          [NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody] : [NSMutableDictionary new],
                                                          [NetworkConstants requestKeyForType:NetworkManagerRequestKeyOther] : [NSMutableDictionary new]}];
    return postDict;
}

- (NSDictionary *)constructPostDictionaryForHeaderKVPs:(NSArray *)headerKVPs bodyKVPs:(NSArray *)bodyKVPs otherKVPs:(NSArray *)otherKVPs
{
    NSMutableDictionary *postDictionary = [self postDictionaryDefault];
    
    NSString *bodyKey = [NetworkConstants requestKeyForType:NetworkManagerRequestKeyBody];
    NSString *headerKey = [NetworkConstants requestKeyForType:NetworkManagerRequestKeyHeader];
    NSString *otherKey = [NetworkConstants requestKeyForType:NetworkManagerRequestKeyOther];

    if (headerKVPs.count > 0 && headerKVPs.count%2 == 0)
    {
        for (int i=0; i < headerKVPs.count; i = i + 2)
        {
            NSMutableDictionary *headerDict = postDictionary[headerKey];
            
            [headerDict setObject:headerKVPs[i+1] forKey:[NetworkConstants apiKeyForType:[headerKVPs[i] integerValue]]];
        }
    }
    
    if (bodyKVPs.count > 0 && bodyKVPs.count%2 == 0)
    {
        for (int i=0; i < bodyKVPs.count; i = i + 2)
        {
            NSMutableDictionary *bodyDict = postDictionary[bodyKey];
            
            [bodyDict setObject:bodyKVPs[i+1] forKey:[NetworkConstants apiKeyForType:[bodyKVPs[i] integerValue]]];
        }
    }
    
    if (otherKVPs.count > 0 && otherKVPs.count%2 == 0)
    {
        for (int i=0; i < otherKVPs.count; i = i + 2)
        {
            NSMutableDictionary *otherDict = postDictionary[otherKey];
            
            [otherDict setObject:otherKVPs[i+1] forKey:[NetworkConstants apiKeyForType:[otherKVPs[i] integerValue]]];
        }
    }
    
    return postDictionary;
}

- (NSDictionary *)networkManagerPostDictionaryForSignInRequest
{
    NSDictionary *postDictionary;
    
    if ([KeychainManager loginType] == LoginTypeEmail)
    {
        postDictionary = [self constructPostDictionaryForHeaderKVPs:nil
                                                           bodyKVPs:@[@(NetworkManagerAPIKey_General_Email), [KeychainManager email],
                                                                      @(NetworkManagerAPIKey_General_Password), [KeychainManager password],
                                                                      @(NetworkManagerAPIKey_General_PushToken), @""]
                                                          otherKVPs:nil];
    }
    else if ([KeychainManager loginType] == LoginTypeFacebook && [FBSDKAccessToken currentAccessToken].tokenString.length > 0)
    {
        postDictionary = [self constructPostDictionaryForHeaderKVPs:nil
                                                           bodyKVPs:@[@(NetworkManagerAPIKey_SignInFacebook_AccessToken), [FBSDKAccessToken currentAccessToken].tokenString]
                                                          otherKVPs:nil];
    }
    
    return postDictionary;
}

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [NetworkManager sharedInstance].delegate = self;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(managePushNotification:) name:kNotificationCenterTink object:nil];
    }
    return self;
}

#pragma mark - Push notifications

- (void)managePushNotification:(NSNotification *)pushNotification
{
    NSError *jsonError;
    PushNotificationJSON *pushNotificationJSON = [[PushNotificationJSON alloc] initWithObject:pushNotification.object error:&jsonError];
    
    if (!jsonError)
    {
        [self setBadgeNumber:pushNotificationJSON.aps.badge];
    }
}

- (void)decreeseBadgeNumber
{
    if ([UIApplication sharedApplication].applicationIconBadgeNumber > 0)
    {
        [UIApplication sharedApplication].applicationIconBadgeNumber--;
    }
}

- (void)setBadgeNumber:(NSNumber *)badgeNumber
{
    if (badgeNumber >= 0)
    {
        [UIApplication sharedApplication].applicationIconBadgeNumber = [badgeNumber integerValue];
    }
}

#pragma mark - First Time App Start

- (void)markAppWasStarted
{
    
    NSString *appStarted = @"markAppAsStarted";
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:appStarted])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:appStarted];
        
        [self signOut];
        
        /**
         *  Remove keychain completely
         */
        [KeychainManager deleteKeychain];
    }
}

#pragma mark - Sign in/up methods
#pragma mark Sign UP

- (void)signUpUserWithEmail:(NSString *)email password:(NSString *)password completion:(void (^)(ErrorJSON *))completionBlock
{
    /**
     *  Input data check
     */
    if (!email || !password)
    {
        ErrorJSON *dataMissing = [ErrorJSON initWithCustomMessage:@"Please insert all requested data." shouldAlertUser:YES];
        if (completionBlock)
        {
            completionBlock(dataMissing);
        }
        return;
    }
    
    NSString *token = [KeychainManager pushToken];
    
    if (!token) {
        token = @"";
    }
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:nil
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_General_Email), email,
                                                                                @(NetworkManagerAPIKey_General_Password), password,
                                                                                @(NetworkManagerAPIKey_General_PushToken), token]
                                                                    otherKVPs:nil];
    [[NetworkManager sharedInstance] signUpUserWithParameters:postDictionary
    completion:^(SignUpJSON *signUpJSON, ErrorJSON *errorJSON)
    {
        if (completionBlock)
        {
            if (!errorJSON)
            {
                completionBlock(nil);
            }
            else
            {
                completionBlock(errorJSON);
            }
        }
    }];
}

#pragma mark Sign IN

- (void)signInUserWithEmail:(NSString *)email password:(NSString *)password completion:(void (^)(SignInJSON *, ErrorJSON *))completionBlock
{
    /**
     *  Input data check
     */
    if (!email || !password)
    {
        ErrorJSON *dataMissing = [ErrorJSON initWithCustomMessage:@"Please insert all requested data." shouldAlertUser:YES];
        if (completionBlock)
        {
            completionBlock(nil, dataMissing);
        }
        return;
    }
    
    NSString *token = [KeychainManager pushToken];
    
    if (!token) {
        token = @"";
    }

    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:nil
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_General_Email), email,
                                                                                @(NetworkManagerAPIKey_General_Password), password,
                                                                                @(NetworkManagerAPIKey_General_PushToken), token]
                                                                    otherKVPs:nil];
    [[NetworkManager sharedInstance] signInUserWithParameters:postDictionary
    completion:^(SignInJSON *signInJSON, ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            [KeychainManager saveAuthenticationToken:signInJSON.auth_token];
            [KeychainManager saveEmail:email password:password username:signInJSON.username fullname:signInJSON.fullname userId:signInJSON.user_id];
            [KeychainManager saveLoginType:LoginTypeEmail];
            [self manageRegistrationStatus:signInJSON.registrationStatus];
            
            if (completionBlock)
            {
                completionBlock(signInJSON, nil);
            }
        }
        else
        {
            if (completionBlock)
            {
                completionBlock(nil, errorJSON);
            }
        }
    }];
}

- (void)signInUserWithFacebookCompletion:(void (^)(ErrorJSON *))completionBlock
{

    if ([FBSDKAccessToken currentAccessToken])
    {
        NSString *facebookToken = [FBSDKAccessToken currentAccessToken].tokenString;
        
        [self signInWithFacebookToken:facebookToken completion:completionBlock];
    }
    else
    {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        
        [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error)
            {
                // Process error
                ErrorJSON *processError = [ErrorJSON initWithCustomMessage:@"Facebook error. Please sign out and sign in again." shouldAlertUser:YES];
                
                if (completionBlock)
                {
                    completionBlock(processError);
                }
            }
            else if (result.isCancelled)
            {
                // Handle cancellations
                [self signOut];
            }
            else
            {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                if ([result.grantedPermissions containsObject:@"email"]) {
                    // Do work
                    
                    [self signInWithFacebookToken:result.token.tokenString completion:completionBlock];
                    
                }
                else
                {
                    ErrorJSON *anError = [ErrorJSON initWithCustomMessage:@"Please share your facebook email with us to sign in." shouldAlertUser:YES];
                    
                    if (completionBlock)
                    {
                        completionBlock(anError);
                    }
                }
            }
        }];
    }
}

- (void)signInWithFacebookToken:(NSString *)facebookToken completion:(void(^)(ErrorJSON *))completionBlock
{
    NSString *token = [KeychainManager pushToken];
    
    if (!token) {
        token = @"";
    }
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:nil
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_SignInFacebook_AccessToken), facebookToken,
                                                                                @(NetworkManagerAPIKey_General_PushToken), token]
                                                                    otherKVPs:nil];
    
    [[NetworkManager sharedInstance] signInUserWithFacebookWithParameters:postDictionary
                                                               completion:^(SignInJSON *signInJSON, ErrorJSON *errorJSON)
     {
         if (!errorJSON)
         {
             [KeychainManager saveAuthenticationToken:signInJSON.auth_token];
             [KeychainManager saveEmail:nil password:nil username:signInJSON.username fullname:signInJSON.fullname userId:signInJSON.user_id];
             [KeychainManager saveLoginType:LoginTypeFacebook];
             [self manageRegistrationStatus:signInJSON.registrationStatus];

             if (completionBlock)
             {
                 completionBlock(nil);
             }
         }
         else
         {
             if (completionBlock)
             {
                 completionBlock(errorJSON);
             }
         }
     }];

}

- (void)refreshSignIn
{
    if ([KeychainManager loginType] == LoginTypeEmail)
    {
        if ([KeychainManager email] != nil && [KeychainManager password] != nil) {
            [self signInUserWithEmail:[KeychainManager email] password:[KeychainManager password] completion:nil];
        }
    }
    else if ([KeychainManager loginType] == LoginTypeFacebook)
    {
        [self signInUserWithFacebookCompletion:nil];
    }
    else
    {
        [AnalyticsHelper trackInvalidStateWithMessage:@"Trying to refresh sign due to new push token but no sign in method available." inMethod:__PRETTY_FUNCTION__];
    }
}

-(BOOL)isSignedIn
{
    if ([KeychainManager authenticationToken].length > 0)
    {
        return YES;
    }
    
    return NO;
}

- (SignInRegistrationStatus)registrationStatus
{
    return [KeychainManager registrationStatus];
}

- (void)resetPasswordForEmail:(NSString *)email completion:(void (^)(ErrorJSON *))completionBlock
{
    if (email.length == 0)
    {
        email = @"";
    }
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:nil
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_General_Email), email]
                                                                    otherKVPs:nil];
    [[NetworkManager sharedInstance] resetPassword:postDictionary
                                        completion:^(ErrorJSON *errorJSON)
     {
         if (completionBlock)
         {
             if (!errorJSON)
             {
                 completionBlock(nil);
             }
             else
             {
                 completionBlock(errorJSON);
             }
         }
     }];
}

#pragma mark Sign OUT

- (void)signOut
{
    [KeychainManager deleteKeychain];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [login logOut];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

#pragma mark - Editing User

- (void)editFullname:(NSString *)newName completion:(void (^)(EditUserJSON *, ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_SignUp_Fullname), newName]
                                                                    otherKVPs:nil];
    [[NetworkManager sharedInstance] editUserData:postDictionary
    completion:^(EditUserJSON *editUserJSON, ErrorJSON *errorJSON)
     {
         if (!errorJSON)
         {
             [KeychainManager saveEmail:nil password:nil username:nil fullname:editUserJSON.name userId:nil];
             if (completionBlock)
             {
                 completionBlock(editUserJSON, nil);
             }
         }
         else
         {
             if (completionBlock)
             {
                 completionBlock(nil, errorJSON);
             }
         }
     }];

}

- (void)editUsername:(NSString *)newName onboarding:(BOOL)isOnboarding completion:(void (^)(EditUserJSON *, ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary;
    
    if (isOnboarding)
    {
        /**
         *  Updating username AND fullname if onboarding
         */
        postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                           bodyKVPs:@[@(NetworkManagerAPIKey_SignUp_Username), newName,
                                                                      @(NetworkManagerAPIKey_SignUp_Fullname), newName]
                                                          otherKVPs:nil];
    }
    else
    {
        postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                           bodyKVPs:@[@(NetworkManagerAPIKey_SignUp_Username), newName]
                                                          otherKVPs:nil];
    
    }
    
    [[NetworkManager sharedInstance] editUserData:postDictionary
    completion:^(EditUserJSON *editUserJSON, ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            [KeychainManager saveEmail:nil password:nil username:editUserJSON.username fullname:editUserJSON.name userId:nil];
            if (completionBlock)
            {
                completionBlock(editUserJSON, nil);
            }
        }
        else
        {
            if (completionBlock)
            {
                completionBlock(nil, errorJSON);
            }
        }
    }];
}

- (void)editRegistrationStatusTo:(SignInRegistrationStatus)registrationStatus completion:(void (^)(ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_EditUser_RegistrationStatus), @(registrationStatus)]
                                                                    otherKVPs:nil];
    [[NetworkManager sharedInstance] editUserData:postDictionary
    completion:^(EditUserJSON *editUserJSON, ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            [self manageRegistrationStatus:editUserJSON.registrationStatus];
            
            if (completionBlock)
            {
                completionBlock(nil);
            }
        }
        else
        {
            if (completionBlock)
            {
                completionBlock(errorJSON);
            }
        }

    }];
}

- (void)changePassword:(NSString *)passwordOld passwordNew:(NSString *)passwordNew completion:(void (^)(ErrorJSON *))completionBlock
{
    if (passwordOld.length == 0 || passwordNew.length == 0)
    {
        return;
    }
    
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_PasswordOld), passwordOld,
                                                                                @(NetworkManagerAPIKey_PasswordNew), passwordNew]
                                                                    otherKVPs:nil];
    [[NetworkManager sharedInstance] changePassword:postDictionary
    completion:^(ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            [KeychainManager saveEmail:nil password:passwordNew username:nil fullname:nil userId:nil];
            if (completionBlock)
            {
                completionBlock(nil);
            }
        }
        else
        {
            if (completionBlock)
            {
                completionBlock(errorJSON);
            }
        }
    }];
}

#pragma mark - Friends

- (void)friendsListCompletion:(void (^)(FriendsListJSON *, ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:nil
                                                                    otherKVPs:nil];
    
    [[NetworkManager sharedInstance] friendsListWithParameters:postDictionary
    completion:^(FriendsListJSON *friendsListJSON, ErrorJSON *errorJSON)
    {
        if (completionBlock)
        {
            if (!errorJSON)
            {
                completionBlock(friendsListJSON, nil);
            }
            else
            {
                completionBlock(nil, errorJSON);
            }
        }
    }];
    
}

- (void)findTinkchatUserForSearchString:(NSString *)searchString completion:(void (^)(UsersListJSON *, ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_FindTinkchatUser_SearchString), searchString]
                                                                    otherKVPs:nil];

    [[NetworkManager sharedInstance] findTinkchatUserWithParameters:postDictionary
    completion:^(UsersListJSON *usersListJSON, ErrorJSON *errorJSON)
    {
        if (completionBlock)
        {
            if (!errorJSON)
            {
                completionBlock(usersListJSON, nil);
            }
            else
            {
                completionBlock(nil, errorJSON);
            }
        }
    }];
}

- (void)addTinkchatUserWithId:(NSString *)userId completion:(void (^)(AddUserJSON *, ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];

    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_AddTinkchatUser_UserId), userId]
                                                                    otherKVPs:nil];

    [[NetworkManager sharedInstance] addTinkchatUserWithParameters:postDictionary
    completion:^(AddUserJSON *addUserJSON, ErrorJSON *errorJSON)
    {
        if (completionBlock)
        {
            if (!errorJSON)
            {
                completionBlock(addUserJSON, nil);
            }
            else
            {
                completionBlock(nil, errorJSON);
            }
        }
    }];
}

- (void)removeTinkchatUserWithId:(NSString *)userId completion:(void (^)(ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:nil
                                                                    otherKVPs:@[@(NetworkManagerAPIKey_RemoveTinkchatUser_UserId), userId]];
    
    [[NetworkManager sharedInstance] removeTinkchatUserWithParameters:postDictionary
    completion:^(ErrorJSON *errorJSON)
    {
        if (completionBlock)
        {
            if (!errorJSON)
            {
                completionBlock(nil);
            }
            else
            {
                completionBlock(errorJSON);
            }
        }
    }];
}

- (void)usersAddedMeListCompletion:(void (^)(FriendsListJSON *, ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:nil
                                                                    otherKVPs:nil];
    
    [[NetworkManager sharedInstance] usersAddedMeListWithParameters:postDictionary
    completion:^(FriendsListJSON *friendsListJSON, ErrorJSON *errorJSON)
    {
        if (completionBlock)
        {
            if (!errorJSON)
            {
                completionBlock(friendsListJSON, nil);
            }
            else
            {
                completionBlock(nil, errorJSON);
            }
        }
    }];
}

- (void)inviteWithEmail:(NSString *)email completion:(void (^)(InviteUserJSON *, ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_Invite_Email), email]
                                                                    otherKVPs:nil];
    
    [[NetworkManager sharedInstance] inviteWithParameters:postDictionary completion:^(InviteUserJSON *inviteUserJSON, ErrorJSON *errorJSON)
    {
        if (completionBlock)
        {
            if (!errorJSON)
            {
                completionBlock(inviteUserJSON, nil);
            }
            else
            {
                completionBlock(nil, errorJSON);
            }
        }
    }];
}

#pragma mark - Tinks

- (void)tinksListCompletion:(void (^)(TinksListJSON *, ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:nil
                                                                    otherKVPs:nil];

    [[NetworkManager sharedInstance] tinksListWithParameters:postDictionary
    completion:^(TinksListJSON *tinksListJSON, ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            if (completionBlock)
            {
                completionBlock(tinksListJSON, nil);
            }
            
            [self setBadgeNumber:@(tinksListJSON.tinks.count)];
        }
        else
        {
            if (completionBlock)
            {
                completionBlock(nil, errorJSON);
            }
        }
    }];
}

- (void)sendTinkToUserWithId:(NSString *)userId completion:(void (^)(TinkSendJSON *, ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_SendTink_UserId), userId]
                                                                    otherKVPs:nil];

    [[NetworkManager sharedInstance] sendTinkToUserWithParameters:postDictionary
    completion:^(TinkSendJSON *tinkJSON, ErrorJSON *errorJSON)
    {
        if (completionBlock)
        {
            if (!errorJSON)
            {
                completionBlock(tinkJSON, nil);
            }
            else
            {
                completionBlock(nil, errorJSON);
            }
        }
    }];
}

- (void)markTinkAsRead:(NSString *)tinkId completion:(void (^)(ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:nil
                                                                    otherKVPs:@[@(NetworkManagerAPIKey_MarkTink_tinkId), tinkId]];
    
    [[NetworkManager sharedInstance] markTinkAsRead:postDictionary
    completion:^(ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            if (completionBlock)
            {
                completionBlock(nil);
            }
            
            [self decreeseBadgeNumber];
        }
        else
        {
            if (completionBlock)
            {
                completionBlock(errorJSON);
            }
        }
    }];
}

#pragma mark - Feedback

- (void)sendFeedback:(NSString *)feedback completion:(void (^)(ErrorJSON *))completionBlock
{
    NSString *authToken = [KeychainManager authenticationToken];
    
    NSDictionary *postDictionary = [self constructPostDictionaryForHeaderKVPs:@[@(NetworkManagerAPIKey_General_AuthToken), authToken]
                                                                     bodyKVPs:@[@(NetworkManagerAPIKey_SendFeedback_Subject), @"[Feedback]",
                                                                                @(NetworkManagerAPIKey_SendFeedback_Message), feedback]
                                                                    otherKVPs:nil];
    
    [[NetworkManager sharedInstance] sendFeedback:postDictionary
    completion:^(ErrorJSON *errorJSON)
    {
        if (completionBlock)
        {
            if (!errorJSON)
            {
                completionBlock(nil);
            }
            else
            {
                completionBlock(errorJSON);
            }
        }
    }];
}

#pragma mark - Helpers

- (void)manageRegistrationStatus:(SignInRegistrationStatus)status
{
    [KeychainManager saveRegistrationStatus:status];
    
    if (status <= SignInRegistrationStatus_OnboardingRequired)
    {
        [OnboardingManager setOnboardingDone:NO];
    }
    else
    {
        [OnboardingManager setOnboardingDone:YES];
    }
}

@end
