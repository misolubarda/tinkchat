//
//  AddressBookJSONMapper.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 27.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "AddressBookJSONMapper.h"

@implementation AddressBookJSONMapper

+ (AddressBookUserJSON *)userJSONfromAddressBookRecord:(ABRecordRef)addressBookRecord
{
    AddressBookUserJSON *addressBookUserJSON = [[AddressBookUserJSON alloc] init];

    addressBookUserJSON.firstName = (__bridge NSString*)ABRecordCopyValue(addressBookRecord, kABPersonFirstNameProperty);
    addressBookUserJSON.lastName = (__bridge NSString*)ABRecordCopyValue(addressBookRecord, kABPersonLastNameProperty);
    
    ABMultiValueRef emails = ABRecordCopyValue(addressBookRecord, kABPersonEmailProperty);
    
    
    for (NSInteger emailCounter = 0; emailCounter < ABMultiValueGetCount(emails); emailCounter++)
    {
        NSString *email = (__bridge NSString*)ABMultiValueCopyValueAtIndex(emails, emailCounter);
        
        switch (emailCounter) {
            case 0:
                addressBookUserJSON.email0 = email;
                break;
            case 1:
                addressBookUserJSON.email1 = email;
                break;
            case 2:
                addressBookUserJSON.email2 = email;
                break;
            case 3:
                addressBookUserJSON.email3 = email;
                break;
            default:
                break;
        }
    }
    
    ABMultiValueRef phones = ABRecordCopyValue(addressBookRecord, kABPersonPhoneProperty);
    
    
    NSInteger mobilePhoneCounter = 0;
    
    for (NSInteger phoneCounter = 0; phoneCounter < ABMultiValueGetCount(phones); phoneCounter++)
    {
        CFStringRef currentPhoneLabel = ABMultiValueCopyValueAtIndex(phones, phoneCounter);
        
        if (!CFStringCompare(currentPhoneLabel, kABPersonPhoneMobileLabel, 0))
        {
            continue;
        }
        
        NSString *phoneNumber = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, phoneCounter);
        
        switch (mobilePhoneCounter) {
            case 0:
                addressBookUserJSON.mobile0 = phoneNumber;
                break;
            case 1:
                addressBookUserJSON.mobile1 = phoneNumber;
                break;
            case 2:
                addressBookUserJSON.mobile2 = phoneNumber;
                break;
            case 3:
                addressBookUserJSON.mobile3 = phoneNumber;
                break;
            default:
                break;
        }
        
        mobilePhoneCounter++;
    }
    
    
    NSError *jsonError;
    addressBookUserJSON = [[AddressBookUserJSON alloc] initWithDictionary:addressBookUserJSON.toDictionary error:&jsonError];
    
    if (!jsonError)
    {
        return addressBookUserJSON;
    }

    return nil;
}

@end
