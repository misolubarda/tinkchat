//
//  PushNotificationManager.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 13/05/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "PushRegistrationManager.h"
#import <AudioToolbox/AudioToolbox.h>

static NSString* const kPushServiceUserHasAgreed = @"kPushServiceUserHasAgreed";
static NSString* const kPushServiceNextAlertTimestamp = @"kPushServiceNextAlertTimestamp";
static NSString* const kPushServicePresentedAlertCount = @"kPushServicePresentedAlertCount";

@implementation PushRegistrationManager

static int const kSystemSoundForPush = 1301;

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playSoundOnPush:) name:kNotificationCenterTink object:nil];
    }
    return self;
}

+ (BOOL)hasUserAgreedToPushService
{
    NSNumber *userHasAgreed = [[NSUserDefaults standardUserDefaults] objectForKey:kPushServiceUserHasAgreed];
    
    return userHasAgreed.boolValue;
}

+ (void)userAgreedToPushService
{
    [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:kPushServiceUserHasAgreed];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSNumber *)presentedAlertCount
{
    NSNumber *alertCount = [[NSUserDefaults standardUserDefaults] objectForKey:kPushServicePresentedAlertCount];
    
    if (!alertCount)
    {
        alertCount = @(0);
    }
    
    return alertCount;
}

+ (void)increaseAlertCount
{
    NSNumber *alertCount = [PushRegistrationManager presentedAlertCount];
    alertCount = @(alertCount.integerValue + 1);
    
    [[NSUserDefaults standardUserDefaults] setObject:alertCount forKey:kPushServicePresentedAlertCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)timeToPresentAlet
{
    NSDate *nextDate = [[NSUserDefaults standardUserDefaults] objectForKey:kPushServiceNextAlertTimestamp];
    NSDate *currentDate;
    
    if (!nextDate)
    {
        nextDate = [NSDate date];
        currentDate = nextDate;
    }
    else
    {
        currentDate = [NSDate date];
    }
    
    if ([currentDate compare:nextDate] != NSOrderedAscending)
    {
        /**
         *  Current date >= next date
         */
        return YES;
    }
    
    return NO;
}

/**
 *  Alert day scheme defines after how many days next alert will be presented. After reaching the last number every next alert is to be presented after number of days equals to last number in array.
 *
 *  @return NSArray of number of days to delay next push alert.
 */
+ (NSArray *)alertDaysScheme
{
    return @[@1, @2, @4, @8, @16];
}

+ (void)increaseAlertDate
{
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDate *nextAlertDate = [NSDate date];

    NSInteger alertCount = [PushRegistrationManager presentedAlertCount].integerValue;
    NSArray *alertScheme = [PushRegistrationManager alertDaysScheme];
    
    if (alertCount < alertScheme.count)
    {
        /**
         *  Offsetting next alert date for number of days regarding the alert scheme
         */
        NSInteger daysDifference = [alertScheme[alertCount] integerValue];
        nextAlertDate = [calendar dateByAddingUnit:NSCalendarUnitDay value:daysDifference toDate:nextAlertDate options:0];
    }
    else
    {
        /**
         *  Offsetting next alert date for LAST number of days regarding the alert scheme (reached end of the scheme)
         */
        NSInteger daysDifference = [[alertScheme lastObject] integerValue];
        nextAlertDate = [calendar dateByAddingUnit:NSCalendarUnitDay value:daysDifference toDate:nextAlertDate options:0];
    }
    
    /**
     *  Set next alert date and increase alert count
     */
    [[NSUserDefaults standardUserDefaults] setObject:nextAlertDate forKey:kPushServiceNextAlertTimestamp];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [PushRegistrationManager increaseAlertCount];
}

- (void)registerToPushServiceIfNeeded
{
//    if ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications])
//    {
//         //  Do not bother if already registered.
//        return;
//    }
    
    if ([PushRegistrationManager hasUserAgreedToPushService])
    {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        UIUserNotificationType userNotificationTypes = UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    else
    {
        if ([PushRegistrationManager timeToPresentAlet])
        {
            [[PushRegistrationManager sharedInstance] presentAlert];
        }
    }
}

- (void)presentAlert
{
    NSString *message;
    NSString *title;
    
    NSInteger alertCount = [PushRegistrationManager presentedAlertCount].integerValue;
    
    switch (alertCount)
    {
        case 0:
            title = @"Most Importantly";
            message = @"Would you like us to notify you when a friend is thinking of you?";
            break;
        default:
            title = @"Real Time Notifications";
            message = @"Would you like us to instantly notify you when a friend is thinking of you?";
            break;
    }
    
    [[[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Don’t Allow", @"Notify Me", nil] show];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.firstOtherButtonIndex + 1)
    {
        [PushRegistrationManager userAgreedToPushService];
        [self registerToPushServiceIfNeeded];
    }
    else
    {
        [PushRegistrationManager increaseAlertDate];
    }
}

#pragma mark - Playing sound on push

- (void)playSoundOnPush:(NSNotification *)notification
{
    AudioServicesPlaySystemSound(kSystemSoundForPush);
}

@end
