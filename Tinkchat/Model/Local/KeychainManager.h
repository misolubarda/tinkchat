//
//  KeychainManager.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 12.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DOSingleton/DOSingleton.h>
#import <UICKeyChainStore/UICKeyChainStore.h>
#import "SignInJSON.h"

typedef NS_ENUM(NSUInteger, LoginType)
{
    LoginTypeUnknown,
    LoginTypeEmail,
    LoginTypeFacebook,
    LoginTypeCount
};

@interface KeychainManager : DOSingleton

@property (nonatomic, strong) UICKeyChainStore *keychainStore;

+ (void)saveLoginType:(LoginType)type;
+ (LoginType)loginType;

+ (void)saveEmail:(NSString *)email password:(NSString *)password username:(NSString *)username fullname:(NSString *)fullname userId:(NSString *)userId;
+ (void)saveEmailVisibility:(BOOL)visible;
+ (void)saveRegistrationStatus:(SignInRegistrationStatus)status;
+ (NSString *)email;
+ (NSString *)password;
+ (NSString *)fullname;
+ (NSString *)username;
+ (NSString *)userId;
+ (NSNumber *)emailVisibile;
+ (SignInRegistrationStatus)registrationStatus;

+ (void)saveAuthenticationToken:(NSString *)authToken;
+ (NSString *)authenticationToken;

+ (void)saveAgreedToPushService;
+ (BOOL)hasAgreedToPushService;

+ (void)savePushToken:(NSString *)pushToken;
+ (NSString *)pushToken;

+ (void)deleteKeychain;

@end
