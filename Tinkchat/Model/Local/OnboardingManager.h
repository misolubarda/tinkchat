//
//  OnboardingManager.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 31/03/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "DOSingleton.h"
#import "TinksListJSON.h"
#import "FriendsListJSON.h"

@class OnboardingManager;

typedef NS_ENUM(NSUInteger, OnboardingStage)
{
    OnboardingStageNone,
    OnboardingStageStart,
    OnboardingStageSwipeRight,
    OnboardingStageTink,
    OnboardingStageSwipeLeft,
    OnboardingStageEnd
};

@protocol OnboardingManagerProtocol <NSObject>

@required
- (void)onboardingManager:(OnboardingManager *)manager refreshTinks:(TinksListJSON *)tinksList;
- (void)onboardingManager:(OnboardingManager *)manager refreshFriends:(FriendsListJSON *)friendsList;
- (void)onboardingManagerCompleted;

@optional
- (void)onboardingManager:(OnboardingManager *)manager didSwitchedToStage:(OnboardingStage)stage;

@end

@interface OnboardingManager : DOSingleton

@property (nonatomic, readonly) OnboardingStage onboardingStage;

+ (void)setOnboardingDone:(BOOL)done;
+ (BOOL)isOnboardingDone;

- (void)activateOnboardingIfNeededWithDelegate:(id<OnboardingManagerProtocol>)delegate;
- (void)continueOnboardingIfNeeded;
- (void)adaptMaskToView:(UIView *)view;
- (void)addFinalLabelToView:(UIView *)view belowSubview:(UIView *)subview;
- (void)removeFinalLabel;

@end
