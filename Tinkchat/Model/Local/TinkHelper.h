//
//  TinkHelper.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 05/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TinkHelper : NSObject

+ (UIColor *)colorForTinkColorID:(NSInteger)colorID;
+ (UIColor *)randomInviteColor;
+ (void)addDefaultShadowToView:(UIView *)view;
+ (BOOL)isEmailValid:(NSString *)email;

@end
