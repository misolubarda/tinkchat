//
//  AddressBookJSONMapper.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 27.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

#import "AddressBookUserJSON.h"

@interface AddressBookJSONMapper : NSObject

+ (AddressBookUserJSON *)userJSONfromAddressBookRecord:(ABRecordRef)addressBookRecord;

@end
