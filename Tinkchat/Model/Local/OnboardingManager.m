//
//  OnboardingManager.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 31/03/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "OnboardingManager.h"
#import "MaskView.h"
#import "TinkchatManager.h"

@interface OnboardingManager ()

@property (nonatomic, strong) MaskView *onboardingMaskView;
@property (nonatomic, readwrite) OnboardingStage onboardingStage;
@property (nonatomic, weak) id <OnboardingManagerProtocol>delegate;

@property (nonatomic, weak) UILabel *onboardingFinalLabel;

@end

@implementation OnboardingManager

static NSString* const kOnboardingDone = @"kOnboardingDone";

#pragma mark -Class methods
+ (void)setOnboardingDone
{
    [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:kOnboardingDone];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[TinkchatManager sharedInstance] editRegistrationStatusTo:SignInRegistrationStatus_Done completion:nil];
}

+ (void)setOnboardingDone:(BOOL)done
{
    [[NSUserDefaults standardUserDefaults] setObject:@(done) forKey:kOnboardingDone];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)isOnboardingDone
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kOnboardingDone] boolValue];
}

#pragma mark -Initialization
- (void)activateOnboardingIfNeededWithDelegate:(id<OnboardingManagerProtocol>)delegate
{
    if (![OnboardingManager isOnboardingDone])
    {        
        self.onboardingStage = OnboardingStageStart;
        
        self.delegate = delegate;
        
        self.onboardingMaskView = [[MaskView alloc] init];
        [self.onboardingMaskView maskSuperview:[[UIApplication sharedApplication] keyWindow]];
    }
}

#pragma mark -Onboarding
- (void)continueOnboardingIfNeeded
{
    switch (self.onboardingStage)
    {
        case OnboardingStageStart:
        {
            TinkOnListJSON *tinkOnList = [[TinkOnListJSON alloc] init];
            tinkOnList.sender_id = @"0";
            tinkOnList.sender_name = @"Tinkchat Team added you.";
            tinkOnList.tink_id = @"0";
            tinkOnList.color = @1;
            tinkOnList.read = NO;
            tinkOnList.created_at = [NSDate date];
            
            TinksListJSON *tinksList = [[TinksListJSON alloc] init];
            tinksList.tinks = (NSArray <TinkOnListJSON, Optional> *)@[tinkOnList];
            
            [self refreshTinks:tinksList];
            
            [self.onboardingMaskView setMaskText:@"Swipe Right to add us to your friend list." topSpace:210.f color:[GlobalData kColorSunFlower] animated:YES];
            
            self.onboardingStage = OnboardingStageSwipeRight;
            break;
        }
        case OnboardingStageSwipeRight:
        {
            FriendJSON *friendJSON = [[FriendJSON alloc] init];
            friendJSON.user_id = @"0";
            friendJSON.name = @"Tinkchat Team";
            friendJSON.email = @"";
            
            FriendsListJSON *onboardingList = [[FriendsListJSON alloc] init];
            onboardingList.friends = (NSArray <FriendJSON, Optional> *)@[friendJSON];
            
            [self refreshFriends:onboardingList];
            
            [self.onboardingMaskView setMaskText:@"Swipe Right to tink us." topSpace:440.f animated:YES];

            self.onboardingStage = OnboardingStageTink;

            break;
        }
        case OnboardingStageTink:
        {
            /**
             *  Logic moved to timer function
             */
            [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(delayTinkTimerAction:) userInfo:nil repeats:NO];
            return;
            break;
        }
        case OnboardingStageSwipeLeft:
        {
            /**
             *  Hide mask
             */
            [self.onboardingMaskView hideMaskViewCompletion:^
            {
                [self.onboardingMaskView maskSuperview:nil];
                self.onboardingMaskView = nil;
                
                /**
                 *  Onboarding COMPLETED
                 */
                [OnboardingManager setOnboardingDone];
                
                [self.delegate onboardingManagerCompleted];
            }];
            break;
        }
        default:
            break;
    }
    
    if ([self.delegate respondsToSelector:@selector(onboardingManager:didSwitchedToStage:)])
    {
        [self.delegate onboardingManager:self didSwitchedToStage:self.onboardingStage];
    }
}

- (void)delayTinkTimerAction:(NSTimer *)timer
{
    TinkOnListJSON *tinkOnList = [[TinkOnListJSON alloc] init];
    tinkOnList.sender_id = @"0";
    tinkOnList.sender_name = @"Tinkchat Team";
    tinkOnList.tink_id = @"0";
    tinkOnList.color = @6;
    tinkOnList.read = NO;
    tinkOnList.created_at = [NSDate date];
    
    TinksListJSON *tinksList = [[TinksListJSON alloc] init];
    tinksList.tinks = (NSArray <TinkOnListJSON, Optional> *)@[tinkOnList];
    
    [self refreshTinks:tinksList];
    
    [self.onboardingMaskView setMaskText:@"Swipe Left to remove tink." topSpace:210.f color:[GlobalData kColorEmerald] animated:YES];
    
    self.onboardingStage = OnboardingStageSwipeLeft;
}

- (void)adaptMaskToView:(UIView *)view
{
    BOOL animated;
    
    switch (self.onboardingStage)
    {
        case OnboardingStageSwipeLeft:
        case OnboardingStageTink:
            animated = YES;
            break;
        case OnboardingStageSwipeRight:
            animated = NO;
            break;
        default:
            // If not onboarding, skip setting frame
            return;
            break;
    }
    CGRect newFrame = [view.superview convertRect:view.frame toView:[[UIApplication sharedApplication] keyWindow]];
    [self.onboardingMaskView setMaskFrame:newFrame animated:animated];
}

- (void)addFinalLabelToView:(UIView *)view belowSubview:(UIView *)subview
{
    UILabel *onboardingCompletedLabel = [[UILabel alloc] init];
    onboardingCompletedLabel.text = @"Nailed it!\n\nNow add your friends\nand tink them to your\nheart's content.";
    onboardingCompletedLabel.font = [GlobalData defaultFont];
    onboardingCompletedLabel.textColor = [UIColor whiteColor];
    onboardingCompletedLabel.textAlignment = NSTextAlignmentCenter;
    onboardingCompletedLabel.numberOfLines = 0;
    onboardingCompletedLabel.translatesAutoresizingMaskIntoConstraints = NO;
    onboardingCompletedLabel.alpha = .0f;
    [view insertSubview:onboardingCompletedLabel belowSubview:subview];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[onboardingCompletedLabel]-20-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(onboardingCompletedLabel)]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[onboardingCompletedLabel]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(onboardingCompletedLabel)]];
    self.onboardingFinalLabel = onboardingCompletedLabel;
    
    [UIView animateWithDuration:1.f animations:^{
        onboardingCompletedLabel.alpha = 1.f;
    }];
}

- (void)removeFinalLabel
{
    [self.onboardingFinalLabel removeFromSuperview];
}

- (void)refreshTinks:(TinksListJSON *)tinks
{
    if ([self.delegate respondsToSelector:@selector(onboardingManager:refreshTinks:)])
    {
        [self.delegate onboardingManager:self refreshTinks:tinks];
    }
}

- (void)refreshFriends:(FriendsListJSON *)friends
{
    if ([self.delegate respondsToSelector:@selector(onboardingManager:refreshFriends:)])
    {
        [self.delegate onboardingManager:self refreshFriends:friends];
    }
}

@end
