//
//  DateTimeFormatter.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 04/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateTimeFormatter : DOSingleton

- (NSString *)tinkDateStingFromDate:(NSDate *)date;
- (NSString *)friendTimeElapsedFromDate:(NSDate *)date;

@end
