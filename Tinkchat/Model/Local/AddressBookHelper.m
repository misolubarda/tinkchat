//
//  AddressBookHelper.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 25.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <AddressBook/AddressBook.h>

#import "AddressBookHelper.h"
#import "AddressBookJSONMapper.h"

@implementation AddressBookHelper

+ (void)getContactsCompletion:(void (^)(NSArray<AddressBookUserJSON> *))completionBlock
{
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
    
    if (status == kABAuthorizationStatusDenied)
    {
        completionBlock(nil);
        return;
    }
    
    
    if (status == kABAuthorizationStatusNotDetermined)
    {
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, NULL), ^(bool granted, CFErrorRef error)
        {
            if (granted)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSArray<AddressBookUserJSON> *contacts = [AddressBookHelper contactsFromAddressBook];
                    
                    completionBlock(contacts);
                    return;
                });
            }
            else
            {
                completionBlock(nil);
                return;
            }
        });
    }
    else if (status == kABAuthorizationStatusAuthorized)
    {
        NSArray<AddressBookUserJSON> *contacts = [AddressBookHelper contactsFromAddressBook];
        
        completionBlock(contacts);
        return;
    }
    else
    {
        completionBlock(nil);
    }

}

+ (NSArray<AddressBookUserJSON> *)contactsFromAddressBook
{
    CFErrorRef error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    
    NSArray *contacts = (__bridge NSArray *)(ABAddressBookCopyArrayOfAllPeople(addressBook));
    
    NSMutableArray *usersArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < contacts.count; i++)
    {
        ABRecordRef aRecord = (__bridge ABRecordRef)(contacts[i]);
        
        AddressBookUserJSON *aContact = [AddressBookJSONMapper userJSONfromAddressBookRecord:aRecord];
        
        [usersArray addObject:aContact];
    }
    
    return [usersArray copy];
}

@end
