//
//  TinkHelper.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 05/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "TinkHelper.h"

@implementation TinkHelper

+ (UIColor *)colorForTinkColorID:(NSInteger)colorID
{
    if (colorID > 0 && colorID < [TinkHelper tinkColors].count)
    {
        return [TinkHelper tinkColors][colorID];
    }
    
    return [TinkHelper tinkColors][0];
}

+ (NSArray *)tinkColors
{
    return @[[UIColor blackColor],
             [GlobalData kColorSunFlower],
             [GlobalData kColorAmethyst],
             [GlobalData kColorCarrot],
             [GlobalData kColorTorquise],
             [GlobalData kColorAlizarin],
             [GlobalData kColorEmerald]];
}

+ (UIColor *)randomInviteColor
{
    NSInteger randomIndex = arc4random_uniform((unsigned int)[TinkHelper tinkColors].count - 1) + 1;
    return [TinkHelper colorForTinkColorID:randomIndex];
}

+ (void)addDefaultShadowToView:(UIView *)view
{
    view.layer.shadowOffset = CGSizeMake(1.f, 1.f);
    view.layer.shadowColor  = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = .5f;
}

+ (BOOL)isEmailValid:(NSString *)email
{
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailPredicate evaluateWithObject:email];
}

//- (UIColor *)interpolatedColorForPercentage:(CGFloat)percent
//{
//    CGFloat startHue;
//    CGFloat startSaturation;
//    CGFloat startBrightness;
//    CGFloat startAlpha;
//
//    CGFloat finalHue;
//    CGFloat finalSaturation;
//    CGFloat finalBrightness;
//    CGFloat finalAlpha;
//
//    CGFloat interpolatedHue;
//    CGFloat interpolatedSaturation;
//    CGFloat interpolatedBrightness;
//    CGFloat interpolatedAlpha;
//
//    UIColor *finalColor;
//
//    NSLog(@"percent: %f", percent);
//
//    if (percent >= 0)
//    {
//        finalColor = [UIColor whiteColor];
//    }
//    else
//    {
//        finalColor = [GlobalData kColorMidnightBlue];
//    }
//
//    [self.notificationColor getHue:&startHue saturation:&startSaturation brightness:&startBrightness alpha:&startAlpha];
//    [finalColor getHue:&finalHue saturation:&finalSaturation brightness:&finalBrightness alpha:&finalAlpha];
//
//    interpolatedHue = percent * (finalHue - startHue) + startHue;
//    interpolatedSaturation = percent * (finalSaturation - startSaturation) + startSaturation;
//    interpolatedBrightness = percent * (finalBrightness - startBrightness) + startBrightness;
//    interpolatedAlpha = percent * (finalAlpha - startAlpha) + startAlpha;
//
//    NSLog(@"hue %f", interpolatedHue);
//    NSLog(@"sat %f", interpolatedSaturation);
//    NSLog(@"bri %f", interpolatedBrightness);
//    NSLog(@"alp %f", interpolatedAlpha);
//
//    return [UIColor colorWithHue:interpolatedHue saturation:interpolatedSaturation brightness:interpolatedBrightness alpha:interpolatedAlpha];
//}

@end
