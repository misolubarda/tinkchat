//
//  AddressBookHelper.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 25.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressBookUserJSON.h"

@interface AddressBookHelper : NSObject

+ (void)getContactsCompletion:(void(^)(NSArray<AddressBookUserJSON> *addressBookContacts))completionBlock;

@end
