//
//  DateTimeFormatter.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 04/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "DateTimeFormatter.h"

@interface DateTimeFormatter ()

@property (nonatomic, strong) NSDateFormatter *formatter;

@end

@implementation DateTimeFormatter

- (NSDateFormatter *)formatter
{
    if (!_formatter) {
        _formatter = [[NSDateFormatter alloc] init];
    }
    return _formatter;
}

- (NSString *)tinkDateStingFromDate:(NSDate *)date
{    
    
    NSCalendar *currentCalendar = [NSCalendar autoupdatingCurrentCalendar];

    if ([currentCalendar isDateInToday:date])
    {
        self.formatter.doesRelativeDateFormatting = NO;
        self.formatter.timeStyle = NSDateFormatterShortStyle;
        self.formatter.dateStyle = NSDateFormatterNoStyle;
    }
    else if ([currentCalendar isDateInYesterday:date])
    {
        self.formatter.doesRelativeDateFormatting = YES;
        self.formatter.timeStyle = NSDateFormatterShortStyle;
        self.formatter.dateStyle = NSDateFormatterMediumStyle;
    }
    else
    {
        self.formatter.doesRelativeDateFormatting = NO;
        self.formatter.timeStyle = NSDateFormatterNoStyle;
        self.formatter.dateStyle = NSDateFormatterMediumStyle;
    }
    
    return [self.formatter stringFromDate:date];
}

- (NSString *)friendTimeElapsedFromDate:(NSDate *)date
{
    if (!date)
    {
        return @"";
    }
    
    NSCalendar *currentCalendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *components = [currentCalendar components:NSCalendarUnitMinute | NSCalendarUnitHour | NSCalendarUnitDay fromDate:date toDate:[NSDate date] options:0];
    
    if (components.day > 0)
    {
        return [NSString stringWithFormat:@"%lid", (long)components.day];
    }
    else if (components.hour > 0)
    {
        return [NSString stringWithFormat:@"%lih", (long)components.hour];
    }
    else if (components.minute > 0)
    {
        return [NSString stringWithFormat:@"%lim", (long)components.minute];
    }
    else
    {
        return @"now";
    }
    
    return @"";
}

@end
