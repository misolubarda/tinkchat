//
//  PushNotificationManager.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 13/05/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PushRegistrationManager : DOSingleton

- (void)registerToPushServiceIfNeeded;

@end
