//
//  KeychainManager.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 12.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "KeychainManager.h"
#import "SignInJSON.h"

@interface KeychainManager ()

@end

@implementation KeychainManager


static NSString *const kServiceKey = @"com.tinkchat";
static NSString *const kGroupKey = @"personalData";
static NSString *const kEmailKey = @"emailKey";
static NSString *const kPasswordKey = @"passwordKey";
static NSString *const kFullnameKey = @"fullnameKey";
static NSString *const kUsernameKey = @"usernameKey";
static NSString *const kUserId = @"userId";
static NSString *const kRegistrationStatusKey = @"registrationStatusKey";
static NSString *const kAuthenticationTokenKey = @"authTokenKey";
static NSString *const kPushTokenKey = @"pushTokenKey";
static NSString *const kAgreedToPushService = @"agreedToPushService";
static NSString *const kEmailVisibilityKey = @"emailVisibilityKey";

static NSString *const kBoolValueTrue = @"YES";
static NSString *const kBoolValueFalse = @"NO";

static NSString *const kLoginType = @"loginType";
static NSString *const kLoginTypeEmail = @"loginTypeEmail";
static NSString *const kLoginTypeFacebook = @"loginTypeFacebook";

+ (NSString *)keyForLoginType:(LoginType)type
{
    switch (type) {
        case LoginTypeEmail:
            return kLoginTypeEmail;
        case LoginTypeFacebook:
            return kLoginTypeFacebook;
        default:
            [[NSException exceptionWithName:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] reason:@"Wrong LoginType enum used" userInfo:nil] raise];
            break;
    }
    return nil;
}

+ (LoginType)loginTypeForKey:(NSString *)key
{
    if ([key isEqualToString:kLoginTypeEmail])
    {
        return LoginTypeEmail;
    }
    else if ([key isEqualToString:kLoginTypeFacebook])
    {
        return LoginTypeFacebook;
    }
    
    return LoginTypeUnknown;
}

- (UICKeyChainStore *)keychainStore
{
    if (!_keychainStore)
    {
        _keychainStore = [UICKeyChainStore keyChainStoreWithService:kServiceKey];
    }
    
    return _keychainStore;
}

+ (void)saveLoginType:(LoginType)type
{
    [[KeychainManager sharedInstance].keychainStore setString:[KeychainManager keyForLoginType:type] forKey:kLoginType];
}

+ (LoginType)loginType
{
    NSString *loginTypeKey = [[KeychainManager sharedInstance].keychainStore stringForKey:kLoginType];
    
    return [KeychainManager loginTypeForKey:loginTypeKey];
}

+ (void)saveEmail:(NSString *)email password:(NSString *)password username:(NSString *)username fullname:(NSString *)fullname userId:(NSString *)userId
{
    if (email)
    {
        [[KeychainManager sharedInstance].keychainStore setString:email forKey:kEmailKey];
    }
    
    if (password)
    {
        [[KeychainManager sharedInstance].keychainStore setString:password forKey:kPasswordKey];
    }
    
    if (fullname)
    {
        [[KeychainManager sharedInstance].keychainStore setString:fullname forKey:kFullnameKey];
    }
    
    if (username)
    {
        [[KeychainManager sharedInstance].keychainStore setString:username forKey:kUsernameKey];
    }
    
    if (userId)
    {
        [[KeychainManager sharedInstance].keychainStore setString:userId forKey:kUserId];
    }
}

+ (void)saveEmailVisibility:(BOOL)visible
{
    [[KeychainManager sharedInstance].keychainStore setString:(visible ? kBoolValueTrue : kBoolValueFalse) forKey:kEmailVisibilityKey];
}

+ (void)saveRegistrationStatus:(SignInRegistrationStatus)status
{
    [[KeychainManager sharedInstance].keychainStore setString:[NSString stringWithFormat:@"%lu", (unsigned long)status] forKey:kRegistrationStatusKey];
}


+ (NSString *)email
{
    return [[KeychainManager sharedInstance].keychainStore stringForKey:kEmailKey];
}

+ (NSString *)password
{
    return [[KeychainManager sharedInstance].keychainStore stringForKey:kPasswordKey];
}

+ (NSString *)fullname
{
    return [[KeychainManager sharedInstance].keychainStore stringForKey:kFullnameKey];
}

+ (NSString *)username
{
    return [[KeychainManager sharedInstance].keychainStore stringForKey:kUsernameKey];
}

+ (NSString *)userId
{
    return [[KeychainManager sharedInstance].keychainStore stringForKey:kUserId];
}

+ (NSNumber *)emailVisibile
{
    NSString *visibleString = [[KeychainManager sharedInstance].keychainStore stringForKey:kEmailVisibilityKey];
    
    if ([visibleString isEqualToString:kBoolValueTrue])
    {
        return @(YES);
    }
    
    if ([visibleString isEqualToString:kBoolValueFalse])
    {
        return @(NO);
    }
    
    return nil;
}

+ (SignInRegistrationStatus)registrationStatus
{
    NSString *statusString = [[KeychainManager sharedInstance].keychainStore stringForKey:kRegistrationStatusKey];
    NSInteger status = statusString.integerValue;
    
    if (status >= 0 && status < SignInRegistrationStatus_Count)
    {
        return status;
    }
    
    return SignInRegistrationStatus_Count;
}

+ (void)saveAuthenticationToken:(NSString *)authToken
{
    if (authToken)
    {
        [[KeychainManager sharedInstance].keychainStore setString:authToken forKey:kAuthenticationTokenKey];
    }
}

+ (NSString *)authenticationToken
{
    return [[KeychainManager sharedInstance].keychainStore stringForKey:kAuthenticationTokenKey];
}

+ (BOOL)hasAgreedToPushService
{
    if ([[KeychainManager sharedInstance].keychainStore stringForKey:kAgreedToPushService])
    {
        return YES;
    }
    return NO;
}

+ (void)saveAgreedToPushService
{
    [[KeychainManager sharedInstance].keychainStore setString:kBoolValueTrue forKey:kAgreedToPushService];
}

+ (void)savePushToken:(NSString *)pushToken
{
    if (pushToken)
    {
        [[KeychainManager sharedInstance].keychainStore setString:pushToken forKey:kPushTokenKey];
    }
}

+ (NSString *)pushToken
{
    return [[KeychainManager sharedInstance].keychainStore stringForKey:kPushTokenKey];
}

+ (void)deleteKeychain
{
    /**
     *  Delete all data BUT push token
     */
    [[KeychainManager sharedInstance].keychainStore removeItemForKey:kAuthenticationTokenKey];
    [[KeychainManager sharedInstance].keychainStore removeItemForKey:kEmailKey];
    [[KeychainManager sharedInstance].keychainStore removeItemForKey:kFullnameKey];
    [[KeychainManager sharedInstance].keychainStore removeItemForKey:kUsernameKey];
    [[KeychainManager sharedInstance].keychainStore removeItemForKey:kUserId];
    [[KeychainManager sharedInstance].keychainStore removeItemForKey:kPasswordKey];
    [[KeychainManager sharedInstance].keychainStore removeItemForKey:kLoginType];
    [[KeychainManager sharedInstance].keychainStore removeItemForKey:kAgreedToPushService];
    [[KeychainManager sharedInstance].keychainStore removeItemForKey:kEmailVisibilityKey];
    [[KeychainManager sharedInstance].keychainStore removeItemForKey:kRegistrationStatusKey];
}

@end
