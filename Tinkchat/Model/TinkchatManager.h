//
//  UserManager.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 12.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "DOSingleton.h"
#import "ErrorJSON.h"
#import "FriendsListJSON.h"
#import "AddUserJSON.h"
#import "TinksListJSON.h"
#import "TinkSendJSON.h"
#import "UsersListJSON.h"
#import "InviteUserJSON.h"
#import "EditUserJSON.h"
#import "SignInJSON.h"

@interface TinkchatManager : DOSingleton

/**
 *  Deleting keychaing if app was started for the first time.
 */
- (void)markAppWasStarted;

- (void)signUpUserWithEmail:(NSString *)email password:(NSString *)password completion:(void(^)(ErrorJSON *errorJSON))completionBlock;
- (void)signInUserWithEmail:(NSString *)email password:(NSString *)password completion:(void(^)(SignInJSON *signInJSON, ErrorJSON *errorJSON))completionBlock;
- (void)signInUserWithFacebookCompletion:(void (^)(ErrorJSON *errorJSON))completionBlock;
- (void)refreshSignIn;
- (void)editUsername:(NSString *)newName onboarding:(BOOL)isOnboarding completion:(void(^)(EditUserJSON *editUserJSON, ErrorJSON *errorJSON))completionBlock;
- (void)editFullname:(NSString *)newName completion:(void (^)(EditUserJSON *editUserJSON, ErrorJSON *errorJSON))completionBlock;
- (void)changePassword:(NSString *)passwordOld passwordNew:(NSString *)passwordNew completion:(void(^)(ErrorJSON *errorJSON))completionBlock;
- (void)resetPasswordForEmail:(NSString *)email completion:(void(^)(ErrorJSON *errorJSON))completionBlock;
- (void)signOut;

/**
 *  Sending status of username/onboarding//done to server.
 *
 *  @param registrationStatus Current registration status
 */
- (void)editRegistrationStatusTo:(SignInRegistrationStatus)registrationStatus completion:(void(^)(ErrorJSON *errorJSON))completionBlock;


- (BOOL)isSignedIn;
- (SignInRegistrationStatus)registrationStatus;

- (void)friendsListCompletion:(void(^)(FriendsListJSON *friendsListJSON, ErrorJSON *errorJSON))completionBlock;
- (void)findTinkchatUserForSearchString:(NSString *)searchString completion:(void(^)(UsersListJSON *usersListJSON, ErrorJSON *errorJSON))completionBlock;
- (void)addTinkchatUserWithId:(NSString *)userId completion:(void(^)(AddUserJSON *addUserJSON, ErrorJSON *errorJSON))completionBlock;
- (void)removeTinkchatUserWithId:(NSString *)userId completion:(void(^)(ErrorJSON *errorJSON))completionBlock;
- (void)usersAddedMeListCompletion:(void (^)(FriendsListJSON *friendsListJSON, ErrorJSON *errorJSON))completionBlock;
- (void)inviteWithEmail:(NSString *)email completion:(void(^)(InviteUserJSON *inviteUserJSON, ErrorJSON *errorJSON))completionBlock;

- (void)tinksListCompletion:(void(^)(TinksListJSON *tinksListJSON, ErrorJSON *errorJSON))completionBlock;
- (void)sendTinkToUserWithId:(NSString *)userId completion:(void (^)(TinkSendJSON *tinkJSON, ErrorJSON *errorJSON))completionBlock;
- (void)markTinkAsRead:(NSString *)tinkId completion:(void (^)(ErrorJSON *errorJSON))completionBlock;

- (void)sendFeedback:(NSString *)feedback completion:(void (^)(ErrorJSON *errorJSON))completionBlock;

@end
