//
//  NavigationBarViewController.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 19/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "NavigationBarViewController.h"
#import "UIViewController+StatusBarBackground.h"
#import "TinkHelper.h"

@interface NavigationBarViewController ()

@end

@implementation NavigationBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [TinkHelper addDefaultShadowToView:self.navigationBar];
    
    [self statusBarBackgroundWithColor:[GlobalData kColorPeterRiver] visible:YES];
}

- (void)disableStatusBarBackgroundView
{
    [self statusBarBackgroundWithColor:nil visible:NO];
}

@end
