//
//  MLMainJSONModel.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 26/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "MLMainJSONModel.h"

@implementation MLMainJSONModel

- (instancetype)initWithObject:(id)object error:(NSError **)error;
{
    
    self = [super init]; //need to be set to silence warnings
    
    if ([object isKindOfClass:[NSDictionary class]])
    {
        self = [[[self class] alloc] initWithDictionary:object error:error];
    }
    else if ([object isKindOfClass:[NSData class]])
    {
        self = [[[super class] alloc] initWithData:object error:error];
    }
    else
    {
        /**
         * Expecting error
         */
        self = [[[self class] alloc] initWithDictionary:object error:error];
    }
    
    return self;
}

@end
