//
//  MLMainJSONModel.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 26/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "JSONModel.h"

@interface MLMainJSONModel : JSONModel

- (instancetype)initWithObject:(id)object error:(NSError **)error NS_DESIGNATED_INITIALIZER;

@end
