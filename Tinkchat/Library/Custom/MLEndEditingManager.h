//
//  MLEndEditingManager.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 06/05/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "DOSingleton.h"

/**
 * Dismissing keyboard on tap on window
 */

@interface MLEndEditingManager : DOSingleton

- (void)managerEnabled:(BOOL)enabled;
- (BOOL)isEnabled;

@end
