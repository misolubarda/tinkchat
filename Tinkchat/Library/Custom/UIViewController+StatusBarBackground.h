//
//  UIViewController+StatusBarBackground.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 06/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController(StatusBarBackground)

@property (nonatomic, strong) UIView *statusBarBackgroundView;

- (void)statusBarBackgroundWithColor:(UIColor *)statusBarColor visible:(BOOL)visible;

@end
