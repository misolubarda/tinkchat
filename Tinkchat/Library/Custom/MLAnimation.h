//
//  MLAnimation.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 14/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLAnimationInterpolation.h"

@interface MLAnimation : NSObject

@property (nonatomic, strong) id fromValue;
@property (nonatomic, strong) id toValue;
@property (nonatomic) NSTimeInterval stopTimestamp;
@property (nonatomic) NSTimeInterval startTimestamp;
@property (nonatomic) NSTimeInterval offset;
@property (nonatomic, assign) MLAnimationType animationType;
@property (nonatomic, copy) void (^interpolationBlock)(id intermediateValue);
@property (nonatomic, copy) void (^completionBlock)();

- (id)currentValueForAnimationTime:(NSTimeInterval)currentTime;

@end
