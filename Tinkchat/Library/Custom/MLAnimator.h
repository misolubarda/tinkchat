//
//  MLAnimator.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 11/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MLAnimation.h"

@interface MLAnimator : DOSingleton

- (void)deleteAnimations:(NSArray *)animationObjects;

- (MLAnimation *)animateWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay type:(MLAnimationType)animationType fromValue:(id)fromValue toValue:(id)toValue interpolationBlock:(void(^)(id currentValue))interpolationBlock competionBlock:(void(^)())completionBlock;
- (MLAnimation *)animateWithDuration:(NSTimeInterval)duration offset:(NSTimeInterval)offset delay:(NSTimeInterval)delay type:(MLAnimationType)animationType fromValue:(id)fromValue toValue:(id)toValue interpolationBlock:(void(^)(id currentValue))interpolationBlock competionBlock:(void(^)())completionBlock;
- (void)invalidate;

@end
