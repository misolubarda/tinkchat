//
//  MLURLRequest.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 17.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "MLMutalbeURLRequest.h"

@implementation MLMutalbeURLRequest

+ (instancetype)initWithBaseURL:(NSString *)baseUrl relativeURL:(NSString *)relativeURL httpMethod:(HttpMethodType)httpMethod headerParameters:(NSDictionary *)headerParameters bodyParameters:(NSDictionary *)bodyParameters
{
    MLMutalbeURLRequest *aRequest = [[MLMutalbeURLRequest alloc] initWithURL:[NSURL URLWithString:baseUrl]];
    aRequest.URL = [aRequest.URL URLByAppendingPathComponent:relativeURL];
    
    [aRequest setHTTPMethod:[MLMutalbeURLRequest httpMethodForType:httpMethod]];
   
    [aRequest setHeaderDataFromDictionary:headerParameters];
    [aRequest setBodyDataFromDictionary:bodyParameters];
    
    return aRequest;
}

+ (NSString *)httpMethodForType:(HttpMethodType)type
{
    switch (type)
    {
        case HttpMethodTypeGET:
            return @"GET";
            break;
        case HttpMethodTypePOST:
            return @"POST";
            break;
        case HttpMethodTypeDELETE:
            return @"DELETE";
            break;
        default:
            break;
    }
    
    return nil;
}

- (void)setBodyDataFromDictionary:(NSDictionary *)parameters
{
    if (parameters)
    {
        NSString *requestString = @"";
        for (NSString *akey in [parameters allKeys])
        {
            if (requestString.length > 2)
            {
                requestString = [requestString stringByAppendingString:@"&"];
            }
            requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"%@=%@", akey, parameters[akey]]];
        }
        
        requestString = [requestString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSData *requestData = [requestString dataUsingEncoding:NSUTF8StringEncoding];
        
        [self setHTTPBody:requestData];
    }
}

- (void)setHeaderDataFromDictionary:(NSDictionary *)parameters
{
    if (parameters)
    {
        for (NSString *akey in [parameters allKeys])
        {
            [self setValue:parameters[akey] forHTTPHeaderField:akey];
        }
    }
}

@end
