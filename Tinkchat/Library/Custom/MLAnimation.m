//
//  MLAnimation.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 14/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "MLAnimation.h"

@interface MLAnimation ()

@end

@implementation MLAnimation

@synthesize startTimestamp;

- (id)currentValueForAnimationTime:(NSTimeInterval)currentTime
{
    if ([self.fromValue isKindOfClass:[self.toValue class]])
    {
        if ([self.fromValue isKindOfClass:[NSNumber class]])
        {
            NSNumber *interpolatedValue = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                           startValue:self.fromValue
                                                                                             endValue:self.toValue
                                                                                            startTime:self.startTimestamp
                                                                                              endTime:self.stopTimestamp
                                                                                          currentTime:currentTime];
            
            return interpolatedValue;
        }
        else if ([self.fromValue isKindOfClass:[UIColor class]])
        {
            UIColor *fromColor = (UIColor *)self.fromValue;
            UIColor *toColor = (UIColor *)self.toValue;
            
            CGFloat fromHue;
            CGFloat fromSaturation;
            CGFloat fromBrightness;
            CGFloat fromAlpha;

            CGFloat toHue;
            CGFloat toSaturation;
            CGFloat toBrightness;
            CGFloat toAlpha;

            [fromColor getHue:&fromHue saturation:&fromSaturation brightness:&fromBrightness alpha:&fromAlpha];
            [toColor getHue:&toHue saturation:&toSaturation brightness:&toBrightness alpha:&toAlpha];
            
            NSNumber *interpolatedHue = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                         startValue:@(fromHue)
                                                                                           endValue:@(toHue)
                                                                                          startTime:self.startTimestamp
                                                                                            endTime:self.stopTimestamp
                                                                                        currentTime:currentTime];
            NSNumber *interpolatedSaturation = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                         startValue:@(fromSaturation)
                                                                                           endValue:@(toSaturation)
                                                                                          startTime:self.startTimestamp
                                                                                            endTime:self.stopTimestamp
                                                                                        currentTime:currentTime];
            NSNumber *interpolatedBrightness = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                         startValue:@(fromBrightness)
                                                                                           endValue:@(toBrightness)
                                                                                          startTime:self.startTimestamp
                                                                                            endTime:self.stopTimestamp
                                                                                        currentTime:currentTime];
            NSNumber *interpolatedAlpha = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                         startValue:@(fromAlpha)
                                                                                           endValue:@(toAlpha)
                                                                                          startTime:self.startTimestamp
                                                                                            endTime:self.stopTimestamp
                                                                                        currentTime:currentTime];
            return [UIColor colorWithHue:interpolatedHue.floatValue
                              saturation:interpolatedSaturation.floatValue
                              brightness:interpolatedBrightness.floatValue
                                   alpha:interpolatedAlpha.floatValue];
        }
        else if ([self.fromValue isKindOfClass:[NSValue class]])
        {
            NSValue *fromValue = self.fromValue;
            
            if (strcmp([fromValue objCType], @encode(CGAffineTransform)) == 0)
            {
                CGAffineTransform fromTransform = [self.fromValue CGAffineTransformValue];
                CGAffineTransform toTransform = [self.toValue CGAffineTransformValue];
                
                NSNumber *transformA = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                        startValue:@(fromTransform.a)
                                                                                          endValue:@(toTransform.a)
                                                                                         startTime:self.startTimestamp
                                                                                           endTime:self.stopTimestamp
                                                                                       currentTime:currentTime];
                NSNumber *transformB = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                        startValue:@(fromTransform.b)
                                                                                          endValue:@(toTransform.b)
                                                                                         startTime:self.startTimestamp
                                                                                           endTime:self.stopTimestamp
                                                                                       currentTime:currentTime];
                NSNumber *transformC = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                        startValue:@(fromTransform.c)
                                                                                          endValue:@(toTransform.c)
                                                                                         startTime:self.startTimestamp
                                                                                           endTime:self.stopTimestamp
                                                                                       currentTime:currentTime];
                NSNumber *transformD = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                        startValue:@(fromTransform.d)
                                                                                          endValue:@(toTransform.d)
                                                                                         startTime:self.startTimestamp
                                                                                           endTime:self.stopTimestamp
                                                                                       currentTime:currentTime];
                NSNumber *transformTX = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                        startValue:@(fromTransform.tx)
                                                                                          endValue:@(toTransform.tx)
                                                                                         startTime:self.startTimestamp
                                                                                           endTime:self.stopTimestamp
                                                                                       currentTime:currentTime];
                NSNumber *transformTY = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                        startValue:@(fromTransform.ty)
                                                                                          endValue:@(toTransform.ty)
                                                                                         startTime:self.startTimestamp
                                                                                           endTime:self.stopTimestamp
                                                                                       currentTime:currentTime];
                CGAffineTransform interpolatedTransform = CGAffineTransformMake(transformA.floatValue, transformB.floatValue, transformC.floatValue, transformD.floatValue, transformTX.floatValue, transformTY.floatValue);
                
                return [NSValue valueWithCGAffineTransform:interpolatedTransform];
            }
            else if(strcmp([fromValue objCType], @encode(CGPoint)) == 0)
            {
                CGPoint fromPoint = [self.fromValue CGPointValue];
                CGPoint toPoint = [self.toValue CGPointValue];
                
                NSNumber *pointX = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                    startValue:@(fromPoint.x)
                                                                                      endValue:@(toPoint.x)
                                                                                     startTime:self.startTimestamp
                                                                                       endTime:self.stopTimestamp
                                                                                   currentTime:currentTime];
                NSNumber *pointY = [MLAnimationInterpolation interpolatedValueForAnimationType:self.animationType
                                                                                    startValue:@(fromPoint.y)
                                                                                      endValue:@(toPoint.y)
                                                                                     startTime:self.startTimestamp
                                                                                       endTime:self.stopTimestamp
                                                                                   currentTime:currentTime];
                CGPoint interpolatedPoint = CGPointMake(pointX.floatValue, pointY.floatValue);
                
                return [NSValue valueWithCGPoint:interpolatedPoint];
            }
        }
    }
    return nil;
}

@end
