//
//  MLAnimationType.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 14/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, MLAnimationType) {
    MLAnimationTypeLinear,
    MLAnimationTypeEaseIn,
    MLAnimationTypeEaseOut,
    MLAnimationTypeEaseInOut,
    MLAnimationTypeLinearOvershoot
};

@interface MLAnimationInterpolation : NSObject

+ (NSNumber *)interpolatedValueForAnimationType:(MLAnimationType)animationType startValue:(NSNumber *)startValue endValue:(NSNumber *)endValue startTime:(NSTimeInterval)startTime endTime:(NSTimeInterval)endTime currentTime:(NSTimeInterval)currentTime;

@end
