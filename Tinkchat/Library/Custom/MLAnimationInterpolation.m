//
//  MLAnimationType.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 14/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "MLAnimationInterpolation.h"

@implementation MLAnimationInterpolation

+ (NSNumber *)interpolatedValueForAnimationType:(MLAnimationType)animationType startValue:(NSNumber *)startValue endValue:(NSNumber *)endValue startTime:(NSTimeInterval)startTime endTime:(NSTimeInterval)endTime currentTime:(NSTimeInterval)currentTime
{
    NSTimeInterval relativeTime = (currentTime - startTime)/(endTime - startTime);
    float interpolatedValue = [MLAnimationInterpolation interpolatedValueForAnimationType:animationType
                                                                               startValue:startValue.floatValue
                                                                                 endValue:endValue.floatValue
                                                                             relativeTime:relativeTime];
    return @(interpolatedValue);
}


+ (float)interpolatedValueForAnimationType:(MLAnimationType)animationType startValue:(float)startValue endValue:(float)endValue relativeTime:(double)relativeTime
{
    switch (animationType)
    {
        case MLAnimationTypeLinear:
            return (endValue - startValue) * relativeTime + startValue;
            break;
        case MLAnimationTypeEaseIn:
            return (endValue - startValue)*relativeTime*relativeTime*relativeTime + startValue; //square or cube of relative time
            break;
        case MLAnimationTypeEaseOut:
        {
            return (endValue - startValue)*(-powf((relativeTime - 1), 2.0f) + 1.0f) + startValue;  // square or qubic
            break;
        }
        case MLAnimationTypeEaseInOut:
            if (relativeTime <= 0.5f)
            {
                return [MLAnimationInterpolation interpolatedValueForAnimationType:MLAnimationTypeEaseIn startValue:startValue endValue:endValue/2.0f relativeTime:2.0f * relativeTime];
            }
            else
            {
                return [MLAnimationInterpolation interpolatedValueForAnimationType:MLAnimationTypeEaseOut startValue:endValue/2.0f endValue:endValue relativeTime:2.0f * (relativeTime - 0.5f)];
            }
            break;
        case MLAnimationTypeLinearOvershoot:
        {
            float dur = 0.2f;
            float freq = 1.0f;
            float decay = 6.0f;
            if (relativeTime <= dur)
            {
                return [MLAnimationInterpolation interpolatedValueForAnimationType:MLAnimationTypeLinear startValue:startValue endValue:endValue relativeTime:relativeTime/dur];
            }
            else
            {
                float amp = (endValue - startValue)/dur;
                float omega = 2.0f * M_PI * freq;
                return endValue + amp * (sinf((relativeTime - dur) * omega)/expf(decay * (relativeTime - dur))/omega);
            }
            break;
        }
        default:
            break;
    }
    return 0;
}

@end
