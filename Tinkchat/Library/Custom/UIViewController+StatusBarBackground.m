//
//  UIViewController+StatusBarBackground.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 06/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "UIViewController+StatusBarBackground.h"
#import <objc/runtime.h>


NSString* const kStatusBarBackgrondViewKey = @"kStatusBarBackgrondViewKey";


@implementation UIViewController(StatusBarBackground)

@dynamic statusBarBackgroundView;


- (void)setStatusBarBackgroundView:(UIView *)object
{
    objc_setAssociatedObject(self, @selector(statusBarBackgroundView), object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)statusBarBackgroundView
{
    return objc_getAssociatedObject(self, @selector(statusBarBackgroundView));
}

- (void)statusBarBackgroundWithColor:(UIColor *)statusBarColor visible:(BOOL)visible
{
    if (visible)
    {
        if (!self.statusBarBackgroundView)
        {
            CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
            
            self.statusBarBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, statusBarFrame.size.width, statusBarFrame.size.height)];
            [self.view addSubview:self.statusBarBackgroundView];
        }
        
        self.statusBarBackgroundView.backgroundColor = statusBarColor;
    }
    else
    {
        [self.statusBarBackgroundView removeFromSuperview];
        self.statusBarBackgroundView = nil;
    }
}

@end
