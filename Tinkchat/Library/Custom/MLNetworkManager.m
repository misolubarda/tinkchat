//
//  MLNetworkManager.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 14.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "MLNetworkManager.h"
#import "MLMutalbeURLRequest.h"

typedef void(^successBlock)(MLNetworkManager *, NSData *);
typedef void(^failureBlock)(MLNetworkManager *, NSError *);

@interface MLNetworkManager () <NSURLSessionDataDelegate, NSURLSessionDelegate>

@property (nonatomic, strong) NSURLSessionDataTask *dataTask;

@end

@interface NSURLRequest (withHttpsCertificates)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host;
+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host;
@end


@implementation MLNetworkManager

- (void)GET:(NSString *)urlPath headerParameters:(NSDictionary *)headerParameters success:(void (^)(NSURLResponse *, NSData *))success failure:(void (^)(NSURLResponse *, NSError *))failure
{
    MLMutalbeURLRequest *aRequest = [MLMutalbeURLRequest initWithBaseURL:kServerUrl relativeURL:urlPath httpMethod:HttpMethodTypeGET headerParameters:headerParameters bodyParameters:nil];
    
    NSLog(@"request %@: %@", aRequest.HTTPMethod, aRequest);
    NSLog(@"header: %@", aRequest.allHTTPHeaderFields);
    
    if (aRequest)
    {
        self.dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:aRequest
        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^
            {
                NSLog(@"server: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                if (!error)
                {
                    if (success)
                    {
                        success(response, data);
                    }
                }
                else
                {
                    if (failure)
                    {
                        failure(response, error);
                    }
                }
            }];
        }];
        
        [self.dataTask resume];
    }

}

- (void)POST:(NSString *)urlPath headerParameters:(NSDictionary *)headerParameters bodyParameters:(NSDictionary *)bodyParameters success:(void (^)(NSURLResponse *, NSData *))success failure:(void (^)(NSURLResponse *, NSError *))failure
{
    MLMutalbeURLRequest *aRequest = [MLMutalbeURLRequest initWithBaseURL:kServerUrl relativeURL:urlPath httpMethod:HttpMethodTypePOST headerParameters:headerParameters bodyParameters:bodyParameters];
    
    NSLog(@"request %@: %@", aRequest.HTTPMethod, aRequest);
    NSLog(@"header: %@", aRequest.allHTTPHeaderFields);
    NSLog(@"body: %@", [[NSString alloc] initWithData:aRequest.HTTPBody encoding:NSUTF8StringEncoding]);

    if (aRequest)
    {
        self.dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:aRequest
        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^
            {
                NSLog(@"server: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                if (!error)
                {
                    if (success)
                    {
                        success(response, data);
                    }
                }
                else
                {
                    if (failure)
                    {
                        failure(response, error);
                    }
                }
            }];
        }];
        
        [self.dataTask resume];
    }
}

- (void)DELETE:(NSString *)urlPath headerParameters:(NSDictionary *)headerParameters success:(void (^)(NSURLResponse *, NSData *))success failure:(void (^)(NSURLResponse *, NSError *))failure
{
    MLMutalbeURLRequest *aRequest = [MLMutalbeURLRequest initWithBaseURL:kServerUrl relativeURL:urlPath httpMethod:HttpMethodTypeDELETE headerParameters:headerParameters bodyParameters:nil];
    
    NSLog(@"request %@: %@", aRequest.HTTPMethod, aRequest);
    NSLog(@"header: %@", aRequest.allHTTPHeaderFields);
    
    if (aRequest)
    {
        self.dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:aRequest
        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^
            {
                NSLog(@"server: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                if (!error)
                {
                    if (success)
                    {
                        success(response, data);
                    }
                }
                else
                {
                    if (failure)
                    {
                        failure(response, error);
                    }
                }
            }];
        }];
        
        [self.dataTask resume];
    }
    
}


@end
