//
//  NavigationBarViewController.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 19/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationBarViewController : UIViewController

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;

- (void)disableStatusBarBackgroundView;

@end
