//
//  MLURLRequest.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 17.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, HttpMethodType) {
    HttpMethodTypePOST,
    HttpMethodTypeGET,
    HttpMethodTypeDELETE
};

@interface MLMutalbeURLRequest : NSMutableURLRequest

+ (instancetype)initWithBaseURL:(NSString *)baseUrl relativeURL:(NSString *)relativeURL httpMethod:(HttpMethodType)httpMethod headerParameters:(NSDictionary *)headerParameters bodyParameters:(NSDictionary *)bodyParameters;

@end
