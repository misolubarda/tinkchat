//
//  MLNetworkManager.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 14.01.15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DOSingleton/DOSingleton.h>


@interface MLNetworkManager : NSObject

- (void)GET:(NSString *)urlPath headerParameters:(NSDictionary *)headerParameters success:(void(^)(NSURLResponse *response, NSData *data))success failure:(void(^)(NSURLResponse *response, NSError *error))failure;
- (void)POST:(NSString *)urlPath headerParameters:(NSDictionary *)headerParameters bodyParameters:(NSDictionary *)bodyParameters success:(void(^)(NSURLResponse *response, NSData *data))success failure:(void(^)(NSURLResponse *response, NSError *error))failure;
- (void)DELETE:(NSString *)urlPath headerParameters:(NSDictionary *)headerParameters success:(void(^)(NSURLResponse *response, NSData *data))success failure:(void(^)(NSURLResponse *response, NSError *error))failure;

@end
