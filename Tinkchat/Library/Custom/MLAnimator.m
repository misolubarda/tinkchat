//
//  MLAnimator.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 11/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "MLAnimator.h"

@interface MLAnimator ()

@property (nonatomic, strong) CADisplayLink *displayLink;
@property (nonatomic, strong) NSMutableArray *animationStack;

@end

@implementation MLAnimator


- (NSMutableArray *)animationStack
{
    if (!_animationStack)
    {
        _animationStack = [NSMutableArray new];
    }
    return _animationStack;
}

- (CADisplayLink *)displayLink
{
    if (!_displayLink)
    {
        _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displayLinkAction)];
        _displayLink.frameInterval = 1.0;
        [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    }
    return _displayLink;
}

- (MLAnimation *)animateWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay type:(MLAnimationType)animationType fromValue:(id)fromValue toValue:(id)toValue interpolationBlock:(void (^)(id))interpolationBlock competionBlock:(void (^)())completionBlock
{
    return [self animateWithDuration:duration offset:0.0 delay:delay type:animationType fromValue:fromValue toValue:toValue interpolationBlock:interpolationBlock competionBlock:completionBlock];
}

- (MLAnimation *)animateWithDuration:(NSTimeInterval)duration offset:(NSTimeInterval)offset delay:(NSTimeInterval)delay type:(MLAnimationType)animationType fromValue:(id)fromValue toValue:(id)toValue interpolationBlock:(void (^)(id))interpolationBlock competionBlock:(void (^)())completionBlock
{
    if (self.displayLink)
    {
        MLAnimation *anAnimation = [[MLAnimation alloc] init];
        anAnimation.fromValue = fromValue;
        anAnimation.toValue = toValue;
        anAnimation.startTimestamp = CACurrentMediaTime() + delay;
        anAnimation.stopTimestamp = anAnimation.startTimestamp + duration;
        anAnimation.offset = offset;
        anAnimation.animationType = animationType;
        anAnimation.interpolationBlock = interpolationBlock;
        anAnimation.completionBlock = completionBlock;
        
        [self.animationStack addObject:anAnimation];
        
        return anAnimation;
    }
    
    return nil;
}

- (void)deleteAnimations:(NSArray *)animationObjects
{
    for (MLAnimation *anAnimation in animationObjects)
    {
        if ([self.animationStack containsObject:anAnimation])
        {
            [self.animationStack removeObject:anAnimation];
            
//            if (anAnimation.interpolationBlock)
//            {
//                anAnimation.interpolationBlock(anAnimation.fromValue);
//            }
//            if (anAnimation.completionBlock)
//            {
//                anAnimation.completionBlock();
//            }
            
            NSLog(@"animation deleted");
        }
    }
    
    NSLog(@"animations on stack: %@", self.animationStack);
}

- (void)displayLinkAction
{
    if (self.animationStack.count == 0)
    {
        NSLog(@"animation stop");
        [self invalidate];
        return;
    }
    
    for (MLAnimation *anAnimation in [self.animationStack copy])
    {
        if (anAnimation.startTimestamp - anAnimation.offset <= CACurrentMediaTime())
        {
            if (anAnimation.stopTimestamp - anAnimation.offset <= CACurrentMediaTime())
            {
                if (anAnimation.interpolationBlock)
                {
                    anAnimation.interpolationBlock(anAnimation.toValue);
                }
                if (anAnimation.completionBlock)
                {
                    anAnimation.completionBlock();
                }
                
                [self.animationStack removeObject:anAnimation];
                NSLog(@"animation removed");

                continue;
            }
                        
            id currentValue = [anAnimation currentValueForAnimationTime:(self.displayLink.timestamp + anAnimation.offset)];
            
            anAnimation.interpolationBlock(currentValue);
        }
    }
}

- (void)invalidate
{
    [self.displayLink invalidate];
    self.displayLink = nil;
}

@end
