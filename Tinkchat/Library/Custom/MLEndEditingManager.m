//
//  MLEndEditingManager.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 06/05/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "MLEndEditingManager.h"

@interface MLEndEditingManager () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;

@end

@implementation MLEndEditingManager

- (instancetype)init
{
    if (self = [super init])
    {
        _tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mainTapGestureAction:)];
        _tapRecognizer.delegate = self;
        _tapRecognizer.cancelsTouchesInView = NO;
        [[[UIApplication sharedApplication] keyWindow] addGestureRecognizer:_tapRecognizer];
    }
    return self;
}

- (void)managerEnabled:(BOOL)enabled
{
    self.tapRecognizer.enabled = enabled;
}

- (BOOL)isEnabled
{
    return self.tapRecognizer.isEnabled;
}

- (void)mainTapGestureAction:(UITapGestureRecognizer *)recognizer
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

@end
