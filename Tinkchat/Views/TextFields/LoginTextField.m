//
//  LoginTextField.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 07/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "LoginTextField.h"

@implementation LoginTextField

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10.0f, 0.0f);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10.0f, 0.0f);
}



@end
