//
//  MainScreenTable.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 04/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "MainTableView.h"

@implementation MainTableView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

@end
