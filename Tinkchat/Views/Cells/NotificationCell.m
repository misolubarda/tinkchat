//
//  NotificationCell.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 09.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "NotificationCell.h"
#import "UsersListVC.h"
#import "DateTimeFormatter.h"
#import "TinkHelper.h"

@interface NotificationCell ()

@property (weak, nonatomic) IBOutlet UIView *notificationBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UILabel *friendNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tinkBackgroundLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tinkBackgroundWidthConstraint;
@property (weak, nonatomic) IBOutlet UIView *tinkedView;
@property (nonatomic, strong) UIColor *notificationColor;

@property (nonatomic, assign) CGPoint startPoint;
@property (nonatomic, assign) CGFloat startLeadingConstant;


@end

@implementation NotificationCell

static CGFloat const maxReactingVelocity = 1000.0;
static CGFloat const initialLeadingConstant = 20;

- (void)setTinkOnListJSON:(TinkOnListJSON *)tinkOnListJSON
{
    _tinkOnListJSON = tinkOnListJSON;
    
    self.friendNameLabel.text = tinkOnListJSON.sender_name;
    self.dateLabel.text = [[DateTimeFormatter sharedInstance] tinkDateStingFromDate:tinkOnListJSON.created_at];
    self.notificationColor = [TinkHelper colorForTinkColorID:tinkOnListJSON.color.integerValue];
}

- (void)setNotificationColor:(UIColor *)notificationColor
{
    _notificationColor = notificationColor;
    
    self.notificationBackgroundView.backgroundColor = notificationColor;
    [TinkHelper addDefaultShadowToView:self.notificationBackgroundView];
}

- (void)setSenderIsInFriendsList:(BOOL)senderIsInFriendsList
{
    _senderIsInFriendsList = senderIsInFriendsList;
    
    [self layoutIfNeeded];
    self.notificationBackgroundView.layer.mask = senderIsInFriendsList ? nil : [self arrowLayer];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)];
    recognizer.delegate = self;
    [self.notificationBackgroundView addGestureRecognizer:recognizer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
        
    self.tinkBackgroundWidthConstraint.constant = self.bounds.size.width - 2 * initialLeadingConstant;
}

- (CALayer *)arrowLayer
{
    CALayer *maskLayer = [CALayer layer];
    CAShapeLayer *largeArrowLayer = [CAShapeLayer layer];
    CAShapeLayer *smallArrowLayer = [CAShapeLayer layer];
    
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    [aPath moveToPoint:CGPointZero];
    [aPath addLineToPoint:CGPointMake(self.notificationBackgroundView.bounds.size.width - 40.f, .0f)];
    [aPath addLineToPoint:CGPointMake(self.notificationBackgroundView.bounds.size.width - 20.f, self.notificationBackgroundView.bounds.size.height/2.f)];
    [aPath addLineToPoint:CGPointMake(self.notificationBackgroundView.bounds.size.width - 40.f, self.notificationBackgroundView.bounds.size.height)];
    [aPath addLineToPoint:CGPointMake(.0f, self.notificationBackgroundView.bounds.size.height)];
    [aPath addLineToPoint:CGPointZero];
    
    largeArrowLayer.path = aPath.CGPath;
    
    [aPath removeAllPoints];
    
    [aPath moveToPoint:CGPointMake(self.notificationBackgroundView.bounds.size.width - 40.f, .0f)];
    [aPath addLineToPoint:CGPointMake(self.notificationBackgroundView.bounds.size.width - 20.f, .0f)];
    [aPath addLineToPoint:CGPointMake(self.notificationBackgroundView.bounds.size.width, self.notificationBackgroundView.bounds.size.height/2.f)];
    [aPath addLineToPoint:CGPointMake(self.notificationBackgroundView.bounds.size.width - 20.f, self.notificationBackgroundView.bounds.size.height)];
    [aPath addLineToPoint:CGPointMake(self.notificationBackgroundView.bounds.size.width - 40.f, self.notificationBackgroundView.bounds.size.height)];
    [aPath addLineToPoint:CGPointMake(self.notificationBackgroundView.bounds.size.width - 40.f, .0f)];
    
    smallArrowLayer.path = aPath.CGPath;
    
    largeArrowLayer.fillColor = [UIColor whiteColor].CGColor;
    smallArrowLayer.fillColor = [UIColor whiteColor].CGColor;
    smallArrowLayer.opacity = 0.8f;
    
    [maskLayer addSublayer:largeArrowLayer];
    [maskLayer addSublayer:smallArrowLayer];
    
    return maskLayer;
}

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]])
    {
        CGPoint velocity = [gestureRecognizer velocityInView:self];
        return fabs(velocity.x) > fabs(velocity.y);
    }
    
    return YES;
}

- (void)panGestureRecognized:(UIPanGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self];
    
    if (recognizer.state == UIGestureRecognizerStateBegan)
    {
        self.startPoint = touchPoint;
        self.startLeadingConstant = self.tinkBackgroundLeadingConstraint.constant;
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged)
    {
        self.tinkBackgroundLeadingConstraint.constant = touchPoint.x - self.startPoint.x + self.startLeadingConstant;
        CGFloat percent = (touchPoint.x - self.startPoint.x)/self.bounds.size.width;
        self.overlayView.alpha = fabs(percent);
        
        if (percent >= 0)
        {
            if (self.senderIsInFriendsList)
            {
                self.overlayView.backgroundColor = nil;
            }
            else
            {
                self.overlayView.backgroundColor = [UIColor whiteColor];
            }
        }
        else
        {
            self.overlayView.backgroundColor = [GlobalData kColorMidnightBlue];
        }
    }
    else if (recognizer.state == UIGestureRecognizerStateRecognized)
    {
        CGPoint velocity = [recognizer velocityInView:self];

        self.tinkBackgroundLeadingConstraint.constant = touchPoint.x - self.startPoint.x + self.startLeadingConstant;
        CGFloat deltaX = self.tinkBackgroundLeadingConstraint.constant - initialLeadingConstant;
        CGFloat reactingVelocity = maxReactingVelocity * (1 - fabs(deltaX)/self.bounds.size.width);
        CGFloat reactingDistance = 0.5f * self.bounds.size.width;
        
        NSTimeInterval animationTime = 0.25f;
        BOOL swipingRight;
        
        if ((deltaX > 0 && velocity.x > reactingVelocity) || (deltaX > reactingDistance && velocity.x >= 0))
        {
            self.tinkBackgroundLeadingConstraint.constant = self.bounds.size.width;
            swipingRight = YES;
        }
        else if ((deltaX < 0 && velocity.x < -reactingVelocity) || (deltaX < -reactingDistance && velocity.x <= 0))
        {
            self.tinkBackgroundLeadingConstraint.constant = -self.bounds.size.width;
            swipingRight = NO;
        }
        else
        {
            [UIView animateWithDuration:0.5f animations:^
            {
                [self resetAnimation];
                [self layoutIfNeeded];
            }];
            
            return;
        }
        
        if (swipingRight)
        {
            [UIView animateWithDuration:animationTime delay:0 options:UIViewAnimationOptionCurveLinear animations:^
             {
                 if (self.senderIsInFriendsList)
                 {
                     self.overlayView.backgroundColor = nil;
                 }
                 else
                 {
                     self.overlayView.backgroundColor = [UIColor whiteColor];
                 }
                 
                 self.overlayView.alpha = 1;
                 
                 [self layoutIfNeeded];
             }
            completion:^(BOOL finished)
             {
                 self.notificationBackgroundView.hidden = YES;
                 [self.owner removeTinkNotificationCell:self swipeRight:swipingRight];
             }];
        }
        else
        {
            [UIView animateWithDuration:animationTime delay:0 options:UIViewAnimationOptionCurveLinear animations:^
             {
                 self.overlayView.backgroundColor = [GlobalData kColorMidnightBlue];
                 self.overlayView.alpha = 1;
                 
                 [self layoutIfNeeded];
             }
                             completion:^(BOOL finished)
             {
                 self.notificationBackgroundView.hidden = YES;
                 [self.owner removeTinkNotificationCell:self swipeRight:swipingRight];
             }];
        }
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^
        {
            [self resetAnimation];
            [self layoutIfNeeded];
        }];
    }
}

- (void)continueAnimatingCompletion:(void (^)())completionBlock
{
    [UIView animateWithDuration:5.f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^
    {
        // Show Tinked label
        self.tinkedView.alpha = 1.f;
    }
    completion:^(BOOL finished)
    {
        completionBlock();
    }];
}


- (void)resetAnimation
{
    self.notificationBackgroundView.hidden = NO;
    self.tinkBackgroundLeadingConstraint.constant = initialLeadingConstant;
    self.overlayView.alpha = 0;
    self.tinkedView.alpha = 0;
}

@end
