//
//  FriendCell.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 09.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendJSON.h"

@class UsersListVC;

@interface FriendCell : UITableViewCell

@property (nonatomic, strong) FriendJSON *friendJSON;
@property (nonatomic, weak) UsersListVC *owner;

/**
 *  Trigger animation when sending tink
 */
- (void)startAnimatingNow;

- (void)continueAnimatingPastStartTime:(NSTimeInterval)animationStartTime;

- (void)resetAnimation;

@end
