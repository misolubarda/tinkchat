//
//  SettingsCell.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 09/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *middleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rightImage;

@end
