//
//  NotificationCell.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 09.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TinkOnListJSON.h"

@class UsersListVC;

@interface NotificationCell : UITableViewCell

@property (nonatomic, strong) TinkOnListJSON *tinkOnListJSON;
@property (nonatomic, weak) UsersListVC *owner;
@property (nonatomic, assign) BOOL senderIsInFriendsList;

- (void)resetAnimation;
- (void)continueAnimatingCompletion:(void(^)())completionBlock;

@end
