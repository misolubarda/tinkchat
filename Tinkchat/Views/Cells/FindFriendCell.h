//
//  FindFriendCell.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 23.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FriendJSON.h"

@interface FindFriendCell : UITableViewCell

@property (nonatomic, strong) FriendJSON *friendJSON;
@property (nonatomic, weak) id owner;
@property (nonatomic, assign) BOOL userInFriendsList;

@end
