//
//  FindFriendCell.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 23.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "FindFriendCell.h"
#import "TinkersAddedYouVC.h"
#import "FindUserVC.h"

@interface FindFriendCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (nonatomic, assign, getter=isInitiallySet) BOOL initiallySet;

@end

@implementation FindFriendCell

- (IBAction)addButtonAction:(UIButton *)sender
{
    if (self.userInFriendsList)
    {
        return;
    }
    
    if ([self.owner respondsToSelector:@selector(addTinkchatUser:completion:)])
    {
        sender.userInteractionEnabled = NO;
        
        [self.owner addTinkchatUser:self.friendJSON.user_id
        completion:^(BOOL success)
        {
            sender.userInteractionEnabled = YES;
            if (success)
            {
                self.userInFriendsList = YES;
            }
        }];
    }
}

- (void)setUserInFriendsList:(BOOL)userInFriendsList
{
    _userInFriendsList = userInFriendsList;
    
    if (userInFriendsList)
    {
        if (self.isInitiallySet)
        {
            self.nameLabel.text = [self.friendJSON.name stringByAppendingString:@" added."];
            [UIView animateWithDuration:.3f animations:^
            {
                self.addButton.alpha = 0.f;
            }];
        }
        else
        {
            self.addButton.alpha = 0.f;
        }
    }
    else
    {
        self.addButton.alpha = 1.f;
    }
    
    self.initiallySet = YES;
}

- (void)setFriendJSON:(FriendJSON *)friendJSON
{
    _friendJSON = friendJSON;
    
    self.nameLabel.text = friendJSON.name;
}

@end
