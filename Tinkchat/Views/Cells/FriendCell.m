//
//  FriendCell.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 09.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "FriendCell.h"
#import "UsersListVC.h"
#import "DateTimeFormatter.h"
#import "MLAnimator.h"
#import "KeychainManager.h"

typedef NS_ENUM(NSUInteger, FriendCellAnimationStage)
{
    FriendCellAnimationStageNone,
    FriendCellAnimationStageBeforeGradient,
    FriendCellAnimationStageOnGradient,
    FriendCellAnimationStageAfterGradient
};

@interface FriendCell ()

//static, not changing
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *friendViewWidthContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tinkViewWidthConstraint;

@property (nonatomic, strong) NSLayoutConstraint *friendLabelLeadingConstraint;
@property (nonatomic, strong) NSLayoutConstraint *friendLabelCenterYConstraint;

// normal
@property (weak, nonatomic) IBOutlet UILabel *friendNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tinkNameLabel;
@property (weak, nonatomic) IBOutlet UIView *tinkView;
@property (weak, nonatomic) IBOutlet UILabel *tinkedLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tinkCheckmarkImageView;
@property (weak, nonatomic) IBOutlet UILabel *tinkTimeElapsedLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *friendViewLeadingContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tinkViewLeadingConstraint;
@property (weak, nonatomic) IBOutlet UIView *friendView;
@property (nonatomic, strong) CAGradientLayer *gradientLayer;
@property (nonatomic, assign) CGPoint startPoint;
@property (nonatomic, assign) CGFloat startLeadingConstant;
@property (nonatomic, assign) FriendCellAnimationStage animationStage;
@property (nonatomic, assign) NSTimeInterval savedGradientMediaTime;

@property (nonatomic, strong) NSArray *currentAnimations;

@end

@implementation FriendCell

/**
 *  Cell positions
 */
static CGFloat rightButtonsWidthOfCellWidth = 0.2;

/**
 *  Animations
 */
static float const tinkGradientWidthOfCellWidth = 0.5;

static NSTimeInterval const maskSlideToTinkDuration = 0.5;
static NSTimeInterval const checkmarkFadeInDuration = 0.5;
static NSTimeInterval const checkmarkBounceDuration = 1.0;

- (void)setFriendJSON:(FriendJSON *)friendJSON
{
    _friendJSON = friendJSON;
    
    self.friendNameLabel.text = friendJSON.name;
    self.tinkNameLabel.text = friendJSON.name;
    self.tinkTimeElapsedLabel.text = [[DateTimeFormatter sharedInstance] friendTimeElapsedFromDate:friendJSON.last_tink_created_at];
}

- (NSArray *)currentAnimations
{
    if (!_currentAnimations)
    {
        _currentAnimations = @[];
    }
    return _currentAnimations;
}

- (BOOL)shouldRevealRightButton
{
    if ([self.friendJSON.user_id integerValue] == ReservedUserId_TinkchatTeam ||
        [self.friendJSON.email isEqualToString:[KeychainManager email]])
    {
        return NO;
    }
    return YES;
}

- (IBAction)removeButtonAction:(UIButton *)sender
{
    [self.owner removeFriendCell:self];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self resetAnimation];
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecognized:)];
    panRecognizer.delegate = self;
    [self.friendView addGestureRecognizer:panRecognizer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.friendViewWidthContraint.constant = self.bounds.size.width;
    self.tinkViewWidthConstraint.constant = self.friendViewWidthContraint.constant;
}

#pragma mark - Gesture recognizing

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer
{
    if (![self.owner shouldAllowGesturesForCell:self])
    {
        return NO;
    }
    
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]])
    {
        CGPoint velocity = [gestureRecognizer velocityInView:self];
        return fabs(velocity.x) > fabs(velocity.y);
    }
    return YES;
}

- (void)panRecognized:(UIPanGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self];
    
    static BOOL willTink = NO;
    static BOOL wasGoingToTink = NO;
    
    if (recognizer.state == UIGestureRecognizerStateBegan)
    {
        self.startPoint = touchPoint;
        self.startLeadingConstant = self.friendViewLeadingContraint.constant;
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged)
    {
        CGFloat deltaX = touchPoint.x - self.startPoint.x;
        
        /**
         *  Move left if allowed 
         *  OR
         *  move right allways!
         */
        if ([self shouldRevealRightButton] || deltaX >= 0)
        {
            if (deltaX < 0)
            {
                CGFloat rightButtonsPosition = - rightButtonsWidthOfCellWidth * self.friendViewWidthContraint.constant;
                if (self.friendViewLeadingContraint.constant <= rightButtonsPosition)
                {
                    return;
                }
            }
            
            CGFloat newPosition = deltaX + self.startLeadingConstant;
            if (newPosition <= 0.0f)
            {
                self.friendViewLeadingContraint.constant = newPosition;
            }
            else
            {
                self.friendViewLeadingContraint.constant = 0.0f;
                
                CGFloat reactingDistance = 0.5f * self.friendViewWidthContraint.constant;
                
                if (newPosition >= reactingDistance)
                {
                    willTink = YES;
                    self.tinkViewLeadingConstraint.constant = self.friendViewWidthContraint.constant - 5.0f;
                }
                else
                {
                    willTink = NO;
                    self.tinkViewLeadingConstraint.constant = newPosition;
                    self.tinkTimeElapsedLabel.alpha = (0.8f - newPosition/self.tinkViewWidthConstraint.constant);
                }
                
                if (willTink != wasGoingToTink)
                {
                    wasGoingToTink = willTink;
                    willTink ? (self.tinkView.backgroundColor = [GlobalData kColorPeterRiver]) : (self.tinkView.backgroundColor = [GlobalData kColorBeforeTink]);

                    [UIView animateWithDuration:0.2f animations:^{
                        [self layoutIfNeeded];
                    }];
                }
            }
        }
    }
    else if (recognizer.state == UIGestureRecognizerStateRecognized)
    {
        CGFloat rightButtonsPosition = - rightButtonsWidthOfCellWidth * self.friendViewWidthContraint.constant;
        
        CGFloat reactingDistance = 0.7f * self.bounds.size.width;

        NSTimeInterval animationTime = 0.25;
        
        if (self.tinkViewLeadingConstraint.constant >= reactingDistance)
        {
            /**
             *  Send tink
             */
            [self.owner sendTinkCell:self];
            [self proceedWithTinkAnimation];
            return;
        }
        else if (self.friendViewLeadingContraint.constant < rightButtonsPosition / 2.0f)
        {
            self.friendViewLeadingContraint.constant = rightButtonsPosition;
        }
        else
        {
            [UIView animateWithDuration:animationTime animations:^
            {
                self.tinkTimeElapsedLabel.alpha = 1.0f;
            }];
            self.tinkViewLeadingConstraint.constant = 0.0f;
            self.friendViewLeadingContraint.constant = 0.0f;
        }
        
        [UIView animateWithDuration:animationTime animations:^
        {
            [self layoutIfNeeded];
        }];
    }
}

#pragma mark - Animations

//- (void)prepareForReuse
//{
//    [super prepareForReuse];
//    
//    /**
//     *  SOLVED ISSUE WITH CELL HOLES
//     */
//    self.friendJSON = self.friendJSON;
//}

- (void)resetAnimation
{
    [[MLAnimator sharedInstance] deleteAnimations:self.currentAnimations];
    self.currentAnimations = nil;
    self.tinkView.layer.mask = nil;
    self.tinkView.backgroundColor = [GlobalData kColorBeforeTink];
    
    self.friendViewLeadingContraint.constant = 0.0f;
    self.tinkViewLeadingConstraint.constant = 0.0f;
    
    [self friendNameLabelIsOnTop:YES];
    self.tinkNameLabel.alpha = 0.0f;
    self.friendNameLabel.textColor = [UIColor blackColor];
    self.tinkTimeElapsedLabel.alpha = 1.0f;
    
    self.tinkedLabel.alpha = 0.0f;
    self.tinkCheckmarkImageView.alpha = 0.0f;
    self.tinkCheckmarkImageView.transform = CGAffineTransformMakeScale(0.5, 0.5);

    [self layoutIfNeeded];
}

- (void)startAnimatingNow
{
    [self proceedWithTinkAnimation];
}

- (void)continueAnimatingPastStartTime:(NSTimeInterval)animationStartTime
{
    NSTimeInterval offset = CACurrentMediaTime() - animationStartTime;
        
    [self completeTinkAnimationWithOffset:offset];
}

- (void)proceedWithTinkAnimation
{
    /**
     *  Prepare before animation
     */
    self.tinkTimeElapsedLabel.alpha = 0.0f;
    self.friendNameLabel.textColor = [UIColor whiteColor];

    /**
     *  Animating tink to cover cell area
     */
    MLAnimation *anAnimation = [[MLAnimator sharedInstance] animateWithDuration:maskSlideToTinkDuration delay:0.0 type:MLAnimationTypeLinear fromValue:@(self.tinkViewLeadingConstraint.constant) toValue:@(self.friendViewWidthContraint.constant) interpolationBlock:^(id currentValue)
    {
        NSNumber *aValue = (NSNumber *)currentValue;
        self.tinkViewLeadingConstraint.constant = aValue.floatValue;
    }
    competionBlock:nil];
    self.currentAnimations = [self.currentAnimations arrayByAddingObject:anAnimation];
    
    anAnimation = [[MLAnimator sharedInstance] animateWithDuration:checkmarkFadeInDuration delay:maskSlideToTinkDuration type:MLAnimationTypeLinear fromValue:@(self.tinkTimeElapsedLabel.alpha) toValue:@0.0f interpolationBlock:^(id currentValue)
    {
        NSNumber *aValue = (NSNumber *)currentValue;
        self.tinkTimeElapsedLabel.alpha = aValue.floatValue;
    }
    competionBlock:nil];
    self.currentAnimations = [self.currentAnimations arrayByAddingObject:anAnimation];

    anAnimation = [[MLAnimator sharedInstance] animateWithDuration:checkmarkFadeInDuration delay:maskSlideToTinkDuration type:MLAnimationTypeLinear fromValue:@.0f toValue:@1.0f interpolationBlock:^(id currentValue)
    {
        NSNumber *aValue = (NSNumber *)currentValue;
        self.tinkCheckmarkImageView.alpha = aValue.floatValue;
        self.tinkedLabel.alpha = 1.0f;
    }
    competionBlock:nil];
    self.currentAnimations = [self.currentAnimations arrayByAddingObject:anAnimation];

//    anAnimation = [[MLAnimator sharedInstance] animateWithDuration:checkmarkFadeInDuration delay:maskSlideToTinkDuration type:MLAnimationTypeLinear fromValue:self.friendNameLabel.textColor toValue:[UIColor whiteColor] interpolationBlock:^(id currentValue)
//    {
//        UIColor *aColor = (UIColor *)currentValue;
//        self.friendNameLabel.textColor = aColor;
//    }
//    competionBlock:nil];
//    self.currentAnimations = [self.currentAnimations arrayByAddingObject:anAnimation];
    
    anAnimation = [[MLAnimator sharedInstance] animateWithDuration:checkmarkBounceDuration delay:maskSlideToTinkDuration type:MLAnimationTypeLinearOvershoot fromValue:[NSValue valueWithCGAffineTransform:self.tinkCheckmarkImageView.transform] toValue:[NSValue valueWithCGAffineTransform:CGAffineTransformIdentity] interpolationBlock:^(id currentValue)
    {
        CGAffineTransform aTransform = [currentValue CGAffineTransformValue];
        
        self.tinkCheckmarkImageView.transform = aTransform;
    }
    competionBlock:^
    {
        [self completeTinkAnimationWithOffset:0.0];
    }];
}

- (void)completeTinkAnimationWithOffset:(NSTimeInterval)offset
{
    /**
     *  Prepare before animation
     */
    [self resetAnimation];
    
    self.tinkView.backgroundColor = [GlobalData kColorPeterRiver];
    self.tinkNameLabel.alpha = 1.0f;
    [self friendNameLabelIsOnTop:NO];
    self.tinkNameLabel.textColor = [UIColor whiteColor];
    self.friendViewLeadingContraint.constant = 0.0f;
    self.tinkViewLeadingConstraint.constant = self.friendViewWidthContraint.constant;
    self.tinkedLabel.alpha = 1.0f;
    self.tinkCheckmarkImageView.alpha = 1.0f;
    self.tinkCheckmarkImageView.transform = CGAffineTransformIdentity;
    
    self.gradientLayer = [self gradientMaskForView:self.tinkView];
    self.gradientLayer.anchorPoint = CGPointZero;
    self.gradientLayer.position = CGPointMake(0.0f, 0.0f);
    self.tinkView.layer.mask = self.gradientLayer;
    
    self.tinkTimeElapsedLabel.alpha = 0.0f;
    self.tinkTimeElapsedLabel.text = @"1m";
    
    MLAnimation *anAnimation = [[MLAnimator sharedInstance] animateWithDuration:tinkAnimationTime offset:offset delay:0.0f type:MLAnimationTypeLinear fromValue:[NSValue valueWithCGPoint:CGPointMake(0.0f, 0.0f)] toValue:[NSValue valueWithCGPoint:CGPointMake( -(1 + tinkGradientWidthOfCellWidth) * self.friendViewWidthContraint.constant, 0)] interpolationBlock:^(id currentValue)
    {
        CGPoint currentPoint = [currentValue CGPointValue];

        /**
         *  Excluding tinkView.layer from being animated while table scroll animating
         */
        [CATransaction begin];
        [CATransaction setValue:(id)kCFBooleanTrue
                         forKey:kCATransactionDisableActions];
        self.tinkView.layer.mask.position = currentPoint;
        [CATransaction commit];
    }
    competionBlock:^
    {
        /**
         *  End of animation
         */
        [self resetAnimation];
    }];
    self.currentAnimations = [self.currentAnimations arrayByAddingObject:anAnimation];
}

- (void)friendNameLabelIsOnTop:(BOOL)onTop
{
    if (onTop)
    {
        [self.contentView insertSubview:self.friendNameLabel aboveSubview:self.tinkView];
        self.friendNameLabel.textColor = [UIColor blackColor];
    }
    else
    {
        [self.contentView insertSubview:self.friendNameLabel belowSubview:self.tinkView];
        self.friendNameLabel.textColor = [GlobalData kColorPeterRiver];
    }
    
    [self.contentView removeConstraint:self.friendLabelLeadingConstraint];
    [self.contentView removeConstraint:self.friendLabelCenterYConstraint];
    
    self.friendLabelLeadingConstraint = [NSLayoutConstraint constraintWithItem:self.friendNameLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1.0f constant:20.0f];
    self.friendLabelCenterYConstraint = [NSLayoutConstraint constraintWithItem:self.friendNameLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f];
    
    [self.contentView addConstraints:@[self.friendLabelLeadingConstraint, self.friendLabelCenterYConstraint]];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
}

- (CAGradientLayer *)gradientMaskForView:(UIView *)view
{
    CGRect layerFrame = CGRectApplyAffineTransform(view.bounds, CGAffineTransformMakeScale((1 + tinkGradientWidthOfCellWidth), 1.0));
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.startPoint = CGPointMake(0, 0.5);
    gradientLayer.endPoint = CGPointMake(1, 0.5);
    gradientLayer.colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor whiteColor].CGColor, (id)[UIColor clearColor].CGColor];
    gradientLayer.locations = @[@(0), @(1.0f/(1+tinkGradientWidthOfCellWidth)), @(1)];
    gradientLayer.frame = layerFrame;
    
    return gradientLayer;
}

@end
