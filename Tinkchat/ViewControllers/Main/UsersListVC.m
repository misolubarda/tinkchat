//
//  UsersListVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 09.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

//#import <NSMutableArray+SWUtilityButtons.h>

#import "UsersListVC.h"
#import "FriendCell.h"
#import "TinkchatManager.h"
#import "NotificationTitleCell.h"
#import "DateTimeFormatter.h"
#import "MainTableView.h"
#import "TinksTableView.h"
#import "AddFriendVC.h"
#import "OnboardingManager.h"
#import "TouchTransparentTableView.h"
#import "PushRegistrationManager.h"

@interface UsersListVC () <UITableViewDataSource, UITableViewDelegate, OnboardingManagerProtocol>

@property (weak, nonatomic) IBOutlet TinksTableView *tinksTableView;
@property (weak, nonatomic) IBOutlet TouchTransparentTableView *friendsTableView;
@property (nonatomic, strong) FriendsListJSON *friendsListJSON;
@property (nonatomic, strong) TinksListJSON *tinksListJSON;
@property (nonatomic, strong) NSMutableArray *tinkCells;
@property (nonatomic) CGFloat friendsTableTopInset;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *friendsTableTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *friendsTableBackgroundTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tinksTableBottomConstraint;

@property (nonatomic) BOOL reloadFriends;

@end

@implementation UsersListVC

static CGFloat const tinksTableToScreenFactor = 0.7;

#pragma mark - Setters/Getters

- (TinksListJSON *)tinksListJSON
{
    if (!_tinksListJSON)
    {
        _tinksListJSON = [[TinksListJSON alloc] init];
    }
    return _tinksListJSON;
}

- (FriendsListJSON *)friendsListJSON
{
    if (!_friendsListJSON)
    {
        _friendsListJSON = [[FriendsListJSON alloc] init];
    }
    return _friendsListJSON;
}

- (NSMutableArray *)tinkCells
{
    if (!_tinkCells) {
        _tinkCells = [[NSMutableArray alloc] init];
    }
    return _tinkCells;
}

#pragma mark - View

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    self.friendsTableTopConstraint.constant = 0;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tinkPushNotification:) name:kNotificationCenterTink object:nil];
    
    if (![OnboardingManager isOnboardingDone])
    {
        [[OnboardingManager sharedInstance] activateOnboardingIfNeededWithDelegate:self];
        [[OnboardingManager sharedInstance] continueOnboardingIfNeeded];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshUI) name:kNotificationCenterAppActive object:nil];

    [self.view layoutIfNeeded];
    
    [self setupFriendsTable];

    if ([OnboardingManager isOnboardingDone])
    {
        [[OnboardingManager sharedInstance] removeFinalLabel];
        [[PushRegistrationManager sharedInstance] registerToPushServiceIfNeeded];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationCenterAppActive object:nil];
}

- (void)setupFriendsTable
{
    self.friendsTableTopInset = self.view.frame.size.height * tinksTableToScreenFactor;
    
    if ([OnboardingManager sharedInstance].onboardingStage == OnboardingStageSwipeRight)
    {
        /**
         *  Hide friends table
         */
        self.friendsTableTopInset = self.view.frame.size.height;
    }

    self.friendsTableView.contentInset = UIEdgeInsetsMake(self.friendsTableTopInset, 0, 0, 0);
    self.friendsTableBackgroundTopConstraint.constant = self.friendsTableTopInset;
    self.tinksTableBottomConstraint.constant = 0.f;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([OnboardingManager isOnboardingDone])
    {
        self.reloadFriends = YES;
        
        [self refreshFriendsListAndStartAnimatingFriendWithId:nil];
        [self refreshTinksList];
    }
}

- (void)refreshUI
{
    if ([OnboardingManager isOnboardingDone])
    {
        [self refreshTinksList];
        [self refreshFriendsListAndStartAnimatingFriendWithId:nil];
    }
}

- (void)refreshTinksList
{
    [[TinkchatManager sharedInstance] tinksListCompletion:^(TinksListJSON *tinksListJSON, ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            [self refreshTinksListWithNewList:tinksListJSON];
        }
        else
        {
            [errorJSON showAlertView];
        }
    }];
}

- (void)refreshFriendsListAndStartAnimatingFriendWithId:(NSString *)friendId
{
    [[TinkchatManager sharedInstance] friendsListCompletion:^(FriendsListJSON *friendsListJSON, ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            [self refreshFriendsListWithNewList:friendsListJSON];
            
            if (friendId)
            {
                [self.friendsListJSON sendTinkToFriendID:friendId notSendButAnimateOnly:YES completionBlock:^(NSTimeInterval pastStartTime) {
                    [self continueAnimatingCellWithFriendId:friendId pastStartTime:pastStartTime];
                }];
            }
        }
        else
        {
            [errorJSON showAlertView];
        }
    }];
}

- (void)refreshFriendsListWithNewList:(FriendsListJSON *)friendsListJSON
{
    [self.friendsListJSON updateWithNewFriendList:friendsListJSON dispatchAsync:YES completionBlock:^(NSArray *indexesToAdd, NSArray *indexesToRemove, NSArray *indexesToReload)
    {
        if (self.reloadFriends)
        {
            self.reloadFriends = NO;
            [self.friendsTableView reloadData];
        }
        else
        {
            NSMutableArray *indexPathsToRemove = [NSMutableArray array];
            NSMutableArray *indexPathsToAdd = [NSMutableArray array];
            NSMutableArray *indexPathsToReload = [NSMutableArray array];
            
            for (NSNumber *anIndex in indexesToAdd)
            {
                [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:anIndex.unsignedIntegerValue inSection:0]];
            }
            
            for (NSNumber *anIndex in indexesToRemove)
            {
                [indexPathsToRemove addObject:[NSIndexPath indexPathForRow:anIndex.unsignedIntegerValue inSection:0]];
            }
            
            for (NSNumber *anIndex in indexesToReload)
            {
                [indexPathsToReload addObject:[NSIndexPath indexPathForRow:anIndex.unsignedIntegerValue inSection:0]];
            }

            if (indexesToRemove.count > 0 || indexesToAdd.count > 0 || indexesToReload.count > 0)
            {
                [self.friendsTableView beginUpdates];
                if (indexesToRemove.count > 0)
                {
                    [self.friendsTableView deleteRowsAtIndexPaths:indexPathsToRemove withRowAnimation:UITableViewRowAnimationAutomatic];
                }
                if (indexesToAdd.count > 0)
                {
                    [self.friendsTableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
                }
                if (indexesToReload.count > 0)
                {
                    [self.friendsTableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationAutomatic];
                }
                [self.friendsTableView endUpdates];
            }
        }
        
        /**
         *  Update tinks with arrows if sender not in friends list
         */
        [self.tinksTableView reloadData];
    }];
}

- (void)refreshTinksListWithNewList:(TinksListJSON *)tinksListJSON
{
    [self.tinksListJSON updateWithNewTinkList:tinksListJSON completionBlock:^(NSArray *indexesToAdd, NSArray *indexesToRemove)
    {
        NSMutableArray *indexPathsToRemove = [NSMutableArray array];
        NSMutableArray *indexPathsToAdd = [NSMutableArray array];

        for (NSNumber *anIndex in indexesToAdd)
        {
            [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:anIndex.unsignedIntegerValue inSection:1]];
        }
        
        for (NSNumber *anIndex in indexesToRemove)
        {
            [indexPathsToRemove addObject:[NSIndexPath indexPathForRow:anIndex.unsignedIntegerValue inSection:1]];
        }
        
        [self.tinksTableView beginUpdates];
        [self.tinksTableView deleteRowsAtIndexPaths:indexPathsToRemove withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tinksTableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tinksTableView endUpdates];
        
        if (indexPathsToAdd.count > 0)
        {
            [[OnboardingManager sharedInstance] removeFinalLabel];
        }
    }];
}

#pragma mark - Push notification arrived

- (void)tinkPushNotification:(NSNotification *)notification
{
    [self refreshTinksList];
}

#pragma mark - Friend Cell methods

- (void)sendTinkCell:(FriendCell *)aCell
{
    if ([OnboardingManager isOnboardingDone])
    {
        /**
         *  Send tink
         */
        [self.friendsListJSON sendTinkToFriendID:aCell.friendJSON.user_id notSendButAnimateOnly:NO completionBlock:^(NSTimeInterval pastStartTime) {
            [self continueAnimatingCellWithFriendId:aCell.friendJSON.user_id pastStartTime:pastStartTime];
        }];
    }
    else
    {
        [self.friendsListJSON sendTinkToFriendID:aCell.friendJSON.user_id notSendButAnimateOnly:YES completionBlock:^(NSTimeInterval pastStartTime) {
            [self continueAnimatingCellWithFriendId:aCell.friendJSON.user_id pastStartTime:pastStartTime];
        }];
        
        [[OnboardingManager sharedInstance] continueOnboardingIfNeeded];
    }
}

- (void)removeFriendCell:(FriendCell *)aCell
{
    /**
     *  Delete tink from server
     */
    [[TinkchatManager sharedInstance] removeTinkchatUserWithId:aCell.friendJSON.user_id
    completion:^(ErrorJSON *errorJSON)
    {
        [errorJSON showAlertView];
    }];
    
    /**
     *  Delete cell from table view and from cells array
     */
    NSIndexPath *aCellIndexPath = [self.friendsTableView indexPathForCell:aCell];
    NSMutableArray *newFriends = [self.friendsListJSON.friends mutableCopy];
    [newFriends removeObjectAtIndex:aCellIndexPath.row];
    FriendsListJSON *newFriendsList = [[FriendsListJSON alloc] init];
    newFriendsList.friends = (NSArray<FriendJSON> *)newFriends;
    [self refreshFriendsListWithNewList:newFriendsList];
}

- (BOOL)shouldAllowGesturesForCell:(FriendCell *)aCell
{
    return ![self.friendsListJSON isCellAnimatingForFriendID:aCell.friendJSON.user_id];
}

#pragma mark FriendCell starting/continuing animation

- (void)continueAnimatingCell:(FriendCell *)friendCell pastStartTime:(NSTimeInterval)startTime
{
    if (startTime == 0.0)
    {
        [friendCell startAnimatingNow];
    }
    else
    {
        [friendCell continueAnimatingPastStartTime:startTime];
    }
}

- (void)continueAnimatingCellWithFriendId:(NSString *)friendId pastStartTime:(NSTimeInterval)startTime
{
    for (FriendCell *aCell in [self.friendsTableView visibleCells])
    {
        if ([aCell.friendJSON.user_id isEqualToString:friendId])
        {
            [self continueAnimatingCell:aCell pastStartTime:startTime];
            return;
        }
    }
}

#pragma mark - Tink Cell methods

- (void)removeTinkNotificationCell:(NotificationCell *)aCell swipeRight:(BOOL)right
{
    /**
     *  Onboarding
     */
    if ([OnboardingManager isOnboardingDone])
    {
        if (right)
        {
            /**
             *  User already on friends list
             */
            if ([self.friendsListJSON containsUserID:aCell.tinkOnListJSON.sender_id])
            {
                /**
                 *  Send tink to friend if swiping right. Get the BOOL if think was sent to server.
                 */
                BOOL tinkRequestSent = [self.friendsListJSON sendTinkToFriendID:aCell.tinkOnListJSON.sender_id notSendButAnimateOnly:NO completionBlock:^(NSTimeInterval pastStartTime) {
                    [self continueAnimatingCellWithFriendId:aCell.tinkOnListJSON.sender_id pastStartTime:pastStartTime];
                }];
                
                if (tinkRequestSent)
                {
                    /**
                     *  If tink request was sent, show "Tinked" and invoke block when completed
                     */
                    [aCell continueAnimatingCompletion:^
                    {
                        [self markTinkAsReadAndDeleteTinkCell:aCell];
                    }];
                }
                else
                {
                    /**
                     *  If tink request wasn't send, just delete the cell
                     */
                    [self markTinkAsReadAndDeleteTinkCell:aCell];
                }
            }
            /**
             *  User has to be added to friends list
             */
            else
            {
                /**
                 *  Add user before deleting cell if swiping right and user doesn't already exist
                 */
                [[TinkchatManager sharedInstance] addTinkchatUserWithId:aCell.tinkOnListJSON.sender_id
                completion:^(AddUserJSON *addUserJSON, ErrorJSON *errorJSON)
                {
                    if (!errorJSON)
                    {
                        [self refreshFriendsListAndStartAnimatingFriendWithId:aCell.tinkOnListJSON.sender_id];
                    }
                }];
                
                [self markTinkAsReadAndDeleteTinkCell:aCell];
            }
        }
        else
        {
            [self markTinkAsReadAndDeleteTinkCell:aCell];
        }
    }
    else
    {
        if ((right && [OnboardingManager sharedInstance].onboardingStage == OnboardingStageSwipeRight) ||
            (!right && [OnboardingManager sharedInstance].onboardingStage == OnboardingStageSwipeLeft))
        {
            /**
             *  Remove tink
             */
            [self deleteTinkCell:aCell];
            
            [[OnboardingManager sharedInstance] continueOnboardingIfNeeded];
        }
        else
        {
            [aCell resetAnimation];
        }
    }
}

- (void)markTinkAsReadAndDeleteTinkCell:(NotificationCell *)tinkCell
{
    /**
     *  Delete tink from server
     */
    [[TinkchatManager sharedInstance] markTinkAsRead:tinkCell.tinkOnListJSON.tink_id
    completion:^(ErrorJSON *errorJSON)
    {
        [errorJSON showAlertView];
    }];
    
    /**
     *  Delete cell from table view and from cells array
     */
    [self deleteTinkCell:tinkCell];
}

- (void)deleteTinkCell:(NotificationCell *)aCell
{
    /**
     *  Delete cell from table view and from cells array
     */
    NSIndexPath *aCellIndexPath = [self.tinksTableView indexPathForCell:aCell];
    NSMutableArray *newTinks = [self.tinksListJSON.tinks mutableCopy];
    [newTinks removeObjectAtIndex:aCellIndexPath.row];
    TinksListJSON *newTinksList = [[TinksListJSON alloc] init];
    newTinksList.tinks = (NSArray<TinkOnListJSON, Optional> *)newTinks;
    [self refreshTinksListWithNewList:newTinksList];
}

#pragma mark -OnboardingManager delegate
- (void)onboardingManager:(OnboardingManager *)manager refreshTinks:(TinksListJSON *)tinksList
{
    [self refreshTinksListWithNewList:tinksList];
}

- (void)onboardingManager:(OnboardingManager *)manager refreshFriends:(FriendsListJSON *)friendsList
{
    [self refreshFriendsListWithNewList:friendsList];
}

- (void)onboardingManager:(OnboardingManager *)manager didSwitchedToStage:(OnboardingStage)stage
{
    if (stage == OnboardingStageTink)
    {
        /**
         *  Show friends table
         */
        [self setupFriendsTable];
    }
}

- (void)onboardingManagerCompleted
{
    [self.tinksTableView reloadData];
    
    [[OnboardingManager sharedInstance] addFinalLabelToView:self.view belowSubview:self.tinksTableView];
    
    [self refreshTinksList];
    [self refreshFriendsListAndStartAnimatingFriendWithId:nil];
    
    [[PushRegistrationManager sharedInstance] registerToPushServiceIfNeeded];
}

#pragma mark -TableView delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tinksTableView])
    {
        if (indexPath.section == 0)
        {
            NotificationTitleCell *notificationTitleCell = [tableView dequeueReusableCellWithIdentifier:@"NotificationTitleCell" forIndexPath:indexPath];
            
            return notificationTitleCell;
        }
        else if (indexPath.section == 1)
        {
            NotificationCell *notificationCell = [tableView dequeueReusableCellWithIdentifier:@"NotificationCell" forIndexPath:indexPath];
            notificationCell.owner = self;
            notificationCell.tinkOnListJSON = self.tinksListJSON.tinks[indexPath.row];
            [notificationCell resetAnimation];
            return notificationCell;
        }
    }
    else if ([tableView isEqual:self.friendsTableView])
    {
        FriendCell *aCell = [self.friendsTableView dequeueReusableCellWithIdentifier:@"FriendCell" forIndexPath:indexPath];
        [aCell resetAnimation];
        aCell.owner = self;
        aCell.friendJSON = self.friendsListJSON.friends[indexPath.row];
        
        return aCell;
    }
    
    return nil;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.tinksTableView])
    {
        if (section == 0)
        {
            return 1;
        }
        else if (section == 1)
        {
            return self.tinksListJSON.tinks.count;
        }
    }
    else if ([tableView isEqual:self.friendsTableView])
    {
        return self.friendsListJSON.friends.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tinksTableView])
    {
        if (indexPath.section == 0)
        {
            return 44;
        }
        if (indexPath.section == 1)
        {
            return 92;
        }
    }
    else if ([tableView isEqual:self.friendsTableView])
    {
        return 50;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView isEqual:self.friendsTableView])
    {
        return 1;
    }
    else if ([tableView isEqual:self.tinksTableView])
    {
        return 2;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tinksTableView])
    {
        if (indexPath.section == 0)
        {
            cell.contentView.hidden = ![OnboardingManager isOnboardingDone];
        }
        else if (indexPath.section == 1)
        {
            NotificationCell *aCell = (NotificationCell *)cell;
            aCell.senderIsInFriendsList = [self.friendsListJSON containsUserID:aCell.tinkOnListJSON.sender_id];
            [aCell resetAnimation];

            if (![OnboardingManager isOnboardingDone])
            {
                [[OnboardingManager sharedInstance] adaptMaskToView:cell];
            }
        }
    }
    
    else if([tableView isEqual:self.friendsTableView])
    {
        FriendCell *aCell = (FriendCell *)cell;
        if ([self.friendsListJSON isCellAnimatingForFriendID:aCell.friendJSON.user_id])
        {
            [self.friendsListJSON sendTinkToFriendID:aCell.friendJSON.user_id notSendButAnimateOnly:YES completionBlock:^(NSTimeInterval pastStartTime) {
                [self continueAnimatingCell:aCell pastStartTime:pastStartTime];
            }];
        }

//        FriendCell *aCell = (FriendCell *)cell;
//        [aCell resetAnimation];
//        aCell.owner = self;
//        aCell.friendJSON = self.friendsListJSON.friends[indexPath.row];
//
//        if ([self.friendsListJSON isCellAnimatingForFriendID:aCell.friendJSON.user_id])
//        {
//            [self.friendsListJSON sendTinkToFriendID:aCell.friendJSON.user_id notSendButAnimateOnly:YES];
//        }

        if (![OnboardingManager isOnboardingDone])
        {
            [[OnboardingManager sharedInstance] adaptMaskToView:cell];
        }
    }
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        cell.preservesSuperviewLayoutMargins = NO;
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tinksTableView])
    {
        if (indexPath.section == 1)
        {
            NotificationCell *aCell = (NotificationCell *)cell;
            [aCell resetAnimation];
        }
    }
    else if ([tableView isEqual:self.friendsTableView])
    {
        FriendCell *aCell = (FriendCell *)cell;
        [aCell resetAnimation];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([self.friendsTableView isEqual:scrollView])
    {
        /**
         *  Scrolling up - limiting scrolling so background is not revealed, stays white
         */
        CGFloat minimalContentOffsetY = -scrollView.contentInset.top + self.friendsListJSON.friends.count * [self tableView:self.friendsTableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        if (scrollView.contentOffset.y > minimalContentOffsetY)
        {
            [scrollView setContentOffset:CGPointMake(0, minimalContentOffsetY) animated:NO];
        }
        
        UIEdgeInsets insets = self.tinksTableView.contentInset;

        /**
         *  Scrolling down - moving white background down with cells
         */
        if (-scrollView.contentOffset.y > scrollView.contentInset.top)
        {
            self.friendsTableBackgroundTopConstraint.constant = -scrollView.contentOffset.y;
        }
        else
        {
            self.friendsTableBackgroundTopConstraint.constant = scrollView.contentInset.top;
        }
        
        insets.bottom = self.view.bounds.size.height + scrollView.contentOffset.y;
        self.tinksTableView.contentInset = insets;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([self.friendsTableView isEqual:scrollView])
    {
        CGFloat minimalContentOffsetY = -scrollView.contentInset.top + self.friendsListJSON.friends.count * [self tableView:self.friendsTableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        if (scrollView.contentOffset.y > minimalContentOffsetY)
        {
            [scrollView setContentOffset:CGPointMake(0, -scrollView.contentInset.top) animated:YES];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AddFriendSegue"])
    {
        AddFriendVC *addFriendVC = (AddFriendVC *)segue.destinationViewController;
        addFriendVC.friendsListJSON = self.friendsListJSON;
    }
}

@end
