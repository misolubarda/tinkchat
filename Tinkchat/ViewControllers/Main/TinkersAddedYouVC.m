//
//  TinkersAddedYouVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 26/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "TinkersAddedYouVC.h"
#import "TinkchatManager.h"
#import "FindFriendCell.h"

@interface TinkersAddedYouVC () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) FriendsListJSON *usersAddedMeListJSON;

@end

@implementation TinkersAddedYouVC

- (FriendsListJSON *)usersAddedMeListJSON
{
    if (!_usersAddedMeListJSON)
    {
        _usersAddedMeListJSON = [[FriendsListJSON alloc] init];
    }
    
    return _usersAddedMeListJSON;
}

- (FriendsListJSON *)friendsListJSON
{
    if (!_friendsListJSON)
    {
        _friendsListJSON = [[FriendsListJSON alloc] init];
    }
    
    return _friendsListJSON;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)refreshList
{
    [[TinkchatManager sharedInstance] usersAddedMeListCompletion:^(FriendsListJSON *friendsListJSON, ErrorJSON *errorJSON)
    {
        [self.usersAddedMeListJSON updateWithNewFriendList:friendsListJSON dispatchAsync:YES completionBlock:^(NSArray *indexesToAdd, NSArray *indexesToRemove, NSArray *indexesToReload)
        {
            [self.tableView reloadData];
        }];
    }];
}

- (void)addTinkchatUser:(NSString *)userId completion:(void (^)(BOOL))completionBlock
{
    [[TinkchatManager sharedInstance] addTinkchatUserWithId:userId
    completion:^(AddUserJSON *addUserJSON, ErrorJSON *errorJSON)
     {
         if (!errorJSON)
         {
             completionBlock(YES);
             [self.delegate tinkersAddedYouVC_refreshFriendsList];
         }
         else
         {
             completionBlock(NO);
             [errorJSON showAlertView];
         }
     }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FindFriendCell *findFriendCell = [tableView dequeueReusableCellWithIdentifier:@"FindFriendCell" forIndexPath:indexPath];
    
    //    findFriendCell.addressBookUserJSON = self.addressBookUsersListJSON.addressBookUsersJSON[indexPath.row];
    
    findFriendCell.owner = self;
    FriendJSON *user = self.usersAddedMeListJSON.friends[indexPath.row];
    findFriendCell.friendJSON = user;
    findFriendCell.userInFriendsList = [self.friendsListJSON containsUserID:user.user_id];
    
    return findFriendCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return self.addressBookUsersListJSON.addressBookUsersJSON.count;
    return self.usersAddedMeListJSON.friends.count;
}

@end
