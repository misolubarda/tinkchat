//
//  UsersListVC.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 09.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationCell.h"
#import "FriendCell.h"

@interface UsersListVC : UIViewController

- (void)removeTinkNotificationCell:(NotificationCell *)aCell swipeRight:(BOOL)right;

- (void)sendTinkCell:(FriendCell *)aCell;
- (void)removeFriendCell:(FriendCell *)aCell;
- (BOOL)shouldAllowGesturesForCell:(FriendCell *)aCell;



@end
