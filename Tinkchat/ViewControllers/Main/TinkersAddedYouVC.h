//
//  TinkersAddedYouVC.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 26/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsListJSON.h"

@protocol TinkersAddedYouDelegate <NSObject>

- (void)tinkersAddedYouVC_refreshFriendsList;

@end

@interface TinkersAddedYouVC : UIViewController

@property (nonatomic, strong) FriendsListJSON *friendsListJSON;
@property (nonatomic, weak) id <TinkersAddedYouDelegate> delegate;

- (void)refreshList;
- (void)addTinkchatUser:(NSString *)userId completion:(void(^)(BOOL success))completionBlock;

@end
