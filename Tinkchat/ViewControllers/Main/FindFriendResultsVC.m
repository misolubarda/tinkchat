//
//  FindFriendResultsVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 24.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "FindFriendResultsVC.h"
#import "FindFriendCell.h"
//#import "Invitation+Accessors.h"
#import "TinkchatManager.h"

@interface FindFriendResultsVC () <UITableViewDataSource, UITableViewDelegate, FindFriendCellDelegate>

@end

@implementation FindFriendResultsVC

- (void)setAddressBookSearchResults:(AddressBookUsersListJSON *)addressBookSearchResults
{
    _addressBookSearchResults = addressBookSearchResults;
    [self.tableView reloadData];
}

- (void)setTinkchatUsersSearchResults:(UsersListJSON *)tinkchatUsersSearchResults
{
    _tinkchatUsersSearchResults = tinkchatUsersSearchResults;
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - FindFriendCellDelegate methods

- (void)FindFriendCell:(FindFriendCell *)aCell addUser:(UserJSON *)userJSON
{
    [[TinkchatManager sharedInstance] addTinkchatUserWithId:userJSON.user_id
    completion:^(AddUserJSON *addUserJSON, ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            
        }
        else
        {
            [errorJSON showAlertView];
        }
    }];
}

- (void)FindFriendCell:(FindFriendCell *)aCell inviteUser:(UserJSON *)userObject
{
    
}

#pragma mark - TableView delegate methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FindFriendCell *findFriendCell = [tableView dequeueReusableCellWithIdentifier:@"FindFriendCell1" forIndexPath:indexPath];
    
    switch (indexPath.section) {
        case 0:
        {
            findFriendCell.userJSON = self.tinkchatUsersSearchResults.users[indexPath.row];
            break;
        }
        case 1:
        {
            findFriendCell.addressBookUserJSON = self.addressBookSearchResults.addressBookUsersJSON[indexPath.row];
            break;
        }
        default:
            break;
    }
    
//    findFriendCell.delegate = self;
    
    return findFriendCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return self.tinkchatUsersSearchResults.users.count;
            break;
        case 1:
            return self.addressBookSearchResults.addressBookUsersJSON.count;
            break;
        default:
            break;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

@end
