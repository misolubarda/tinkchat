//
//  FindFriendVC.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 09.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsListJSON.h"

@interface AddFriendVC : UIViewController

@property (nonatomic, copy) FriendsListJSON *friendsListJSON;

- (IBAction)backButtonAction:(id)sender;

@end
