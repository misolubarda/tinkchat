//
//  SettingsVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 23.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "SettingsVC.h"
#import "TinkchatManager.h"
#import "SettingsCell.h"
#import "KeychainManager.h"
#import "MainTableView.h"
#import "SettingsPasswordVC.h"
#import "UsernameVC.h"

typedef NS_ENUM(NSUInteger, SettingsCellType) {
    SettingsCellTypeEmail,
    SettingsCellTypeFullName,
    SettingsCellTypeUsername,
    SettingsCellTypePassword,
    SettingsCellTypeTellUsAnything,
    SettingsCellTypeSignOut,
    SettingsCellTypeCount   //Do not remove
};

@interface SettingsVC () <UITableViewDelegate, UITableViewDataSource, SettingsPasswordDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet MainTableView *settingsTableView;
@property (nonatomic, assign) BOOL passwordJustChanged;
@property (weak, nonatomic) IBOutlet UILabel *tinkchatHomepageLabel;
@end

@implementation SettingsVC

- (NSString *)nameForCellType:(SettingsCellType)type
{
    switch (type)
    {
        case SettingsCellTypeUsername:
            return @"Username";
            break;
        case SettingsCellTypeFullName:
            return @"Full Name";
            break;
        case SettingsCellTypeEmail:
            return @"Email";
            break;
        case SettingsCellTypePassword:
            return @"Password";
            break;
        case SettingsCellTypeTellUsAnything:
            return @"Tell Us Anything";
            break;
        case SettingsCellTypeSignOut:
            return @"Sign out";
            break;
        default:
            break;
    }
    return nil;
}

- (SettingsCellType)cellTypeForRow:(NSInteger)row
{
    return row;
}

#pragma mark - UIView Lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.settingsTableView reloadData];
}

#pragma mark - IBActions

- (IBAction)backButtonAction:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tinkchatWebsiteButtonAction:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[GlobalData tinkchatWebsiteUrl]])
    {
        [[UIApplication sharedApplication] openURL:[GlobalData tinkchatWebsiteUrl]];
    }
}

- (IBAction)tinkchatFacebookButtonAction:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[GlobalData tinkchatFacebookApp]])
    {
        [[UIApplication sharedApplication] openURL:[GlobalData tinkchatFacebookApp]];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:[GlobalData tinkchatFacebookUrl]])
    {
        [[UIApplication sharedApplication] openURL:[GlobalData tinkchatFacebookUrl]];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SettingsCellType cellType = [self cellTypeForRow:indexPath.row];

    switch (cellType)
    {
        case SettingsCellTypeUsername:
        {
            UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
            UsernameVC *usernameVC = [loginStoryboard instantiateViewControllerWithIdentifier:@"UsernameVC"];
            usernameVC.NOT_Onboarding = YES;
            [self showViewController:usernameVC sender:self];
            break;
        }
        case SettingsCellTypeFullName:
            [self performSegueWithIdentifier:@"SettingsNameSegue" sender:self];
            break;
        case SettingsCellTypePassword:
            [self performSegueWithIdentifier:@"SettingsPasswordSegue" sender:self];
            break;
        case SettingsCellTypeTellUsAnything:
            [self performSegueWithIdentifier:@"TellUsAnythingSegue" sender:self];
            break;
        case SettingsCellTypeSignOut:
        {
            NSString *alertTitle = [KeychainManager username];
            NSString *alertMessage = @"Are you sure you want to sign out?";
            [[[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Sign out", nil] show];
            break;
        }
        default:
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsCell *aCell = [tableView dequeueReusableCellWithIdentifier:@"SettingsCell" forIndexPath:indexPath];
    
    SettingsCellType cellType = [self cellTypeForRow:indexPath.row];
    
    aCell.leftLabel.text = [self nameForCellType:cellType];
    aCell.rightImage.hidden = NO;
    aCell.middleLabel.text = @"";

    switch (cellType)
    {
        case SettingsCellTypeUsername:
            aCell.middleLabel.text = [KeychainManager username];
            break;
        case SettingsCellTypeFullName:
            aCell.middleLabel.text = [KeychainManager fullname];
            break;
        case SettingsCellTypeEmail:
            aCell.middleLabel.text = [KeychainManager email];
            aCell.rightImage.hidden = YES;
            break;
        case SettingsCellTypePassword:
            if (self.passwordJustChanged)
            {
                aCell.middleLabel.text = @"Change successful.";
                self.passwordJustChanged = NO;
            }
            else
            {
                aCell.middleLabel.text = @"";
            }
            break;
        case SettingsCellTypeSignOut:
            aCell.rightImage.hidden = YES;
            break;
        default:
            break;
    }
    
    return aCell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return SettingsCellTypeCount;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        cell.preservesSuperviewLayoutMargins = NO;
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
}

- (void)SettingsPasswordVC:(SettingsPasswordVC *)settingsPassswordVC didCompleteWithSuccess:(BOOL)success
{
    self.passwordJustChanged = success;
    [self.settingsTableView reloadData];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.firstOtherButtonIndex)
    {
        [[TinkchatManager sharedInstance] signOut];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SettingsPasswordSegue"])
    {
        SettingsPasswordVC *destVC = (SettingsPasswordVC *)segue.destinationViewController;
        destVC.delegate = self;
    }
}

@end
