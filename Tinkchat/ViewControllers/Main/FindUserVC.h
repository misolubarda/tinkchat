//
//  FindUserVC.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 26/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsListJSON.h"

@protocol FindUserDelegate <NSObject>

- (void)findUserVC_refreshFriendsList;

@end

@interface FindUserVC : UIViewController

@property (nonatomic, strong) FriendsListJSON *friendsListJSON;
@property (nonatomic, weak) id <FindUserDelegate> delegate;

- (void)findUsersWithKeyword:(NSString *)keyword;
- (void)addTinkchatUser:(NSString *)userId completion:(void(^)(BOOL success))completionBlock;

@end
