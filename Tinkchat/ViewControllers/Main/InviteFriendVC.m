//
//  InviteFriendVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 26/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "InviteFriendVC.h"
#import "TinkchatManager.h"
#import "TinkHelper.h"

typedef NS_ENUM(NSUInteger, InviteFriendResponseType)
{
    InviteFriendResponseTypeInvitationSent,
    InviteFriendResponseTypeAddedToFriends,
    InviteFriendResponseTypeAlreadyFriends
};

@interface InviteFriendVC () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *friendEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailErrorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *emailErrorImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (nonatomic, strong) NSArray *sentInvitations;
@property (weak, nonatomic) IBOutlet UILabel *invitationSentLabel;

@end

@implementation InviteFriendVC

- (NSArray *)sentInvitations
{
    if (!_sentInvitations)
    {
        _sentInvitations = @[];
    }
    return _sentInvitations;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [TinkHelper addDefaultShadowToView:self.inviteButton];
    
    [self resetUI];
}

- (IBAction)inviteButtonAction:(UIButton *)sender
{
    [self resetUI];
    
    if (!(self.friendEmailLabel.text.length > 0))
    {
        ErrorJSON *errorJSON = [[ErrorJSON alloc] init];
        errorJSON.errorCode = @(ErrorCodeInviteEmailMissing);
        [self presentErrorForErrorCode:errorJSON];
        
        return;
    }
    
    if (![TinkHelper isEmailValid:self.friendEmailLabel.text])
    {
        ErrorJSON *errorJSON = [[ErrorJSON alloc] init];
        errorJSON.errorCode = @(ErrorCodeInviteEmailNotValid);
        [self presentErrorForErrorCode:errorJSON];
        
        return;
    }
    
    
    self.view.userInteractionEnabled = NO;
    [self.activityIndicator startAnimating];

    [[TinkchatManager sharedInstance] inviteWithEmail:self.friendEmailLabel.text completion:^(InviteUserJSON *inviteUserJSON, ErrorJSON *errorJSON)
    {
        self.view.userInteractionEnabled = YES;
        [self.activityIndicator stopAnimating];
        
        if (!errorJSON)
        {
            [AnalyticsHelper trackInviteEmail:self.friendEmailLabel.text success:YES error:errorJSON];

            switch (inviteUserJSON.invitationType.integerValue)
            {
                case InviteUserJSONTypeInvited:
                    [self presentResponse:InviteFriendResponseTypeInvitationSent inviteUserJSON:inviteUserJSON];
                    break;
                case InviteUserJSONTypeFriendAdded:
                    [self presentResponse:InviteFriendResponseTypeAddedToFriends inviteUserJSON:inviteUserJSON];
                default:
                    break;
            }
        }
        else
        {
            [AnalyticsHelper trackInviteEmail:self.friendEmailLabel.text success:NO error:errorJSON];

            [self presentErrorForErrorCode:errorJSON];
        }
    }];
}

- (void)resetUI
{
    self.emailErrorLabel.hidden = YES;
    self.emailErrorImageView.hidden = YES;
    self.invitationSentLabel.hidden = YES;
}

- (void)presentResponse:(InviteFriendResponseType)responseType inviteUserJSON:(InviteUserJSON *)inviteUserJSON
{
    NSString *invitationString;
    UIColor *invitationColor;
    
    switch (responseType) {
        case InviteFriendResponseTypeInvitationSent:
        {
            invitationString = [NSString stringWithFormat:@"Invitation sent to\n%@", self.friendEmailLabel.text];
            invitationColor = [GlobalData kColorSunFlower];
            break;
        }
        case InviteFriendResponseTypeAddedToFriends:
        {
            invitationString = [NSString stringWithFormat:@"%@\n is already a user and was\nadded to your friend list as\n%@", self.friendEmailLabel.text, inviteUserJSON.fullname];
            invitationColor = [GlobalData kColorSunFlower];
            break;
        }
        case InviteFriendResponseTypeAlreadyFriends:
        {
            invitationString = [NSString stringWithFormat:@"%@\n is already on your friend list.", self.friendEmailLabel.text];
            invitationColor = [GlobalData kColorSunFlower];
            break;
        }
        default:
            break;
    }
    
    self.friendEmailLabel.text = @"";
    
    self.invitationSentLabel.text = invitationString;
    self.invitationSentLabel.textColor = invitationColor;
    self.invitationSentLabel.hidden = NO;
}

- (void)presentErrorForErrorCode:(ErrorJSON *)errorJSON
{
    switch (errorJSON.errorCode.integerValue)
    {
        case ErrorCodeInviteEmailMissing:
            self.emailErrorLabel.text = @"Email missing";
            self.emailErrorLabel.textColor = [GlobalData kColorSunFlower];
            self.emailErrorLabel.hidden = NO;
            self.emailErrorImageView.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.emailErrorImageView.hidden = NO;
            break;
        case ErrorCodeInviteEmailNotValid:
            self.emailErrorLabel.text = @"Email not valid";
            self.emailErrorLabel.textColor = [GlobalData kColorAlizarin];
            self.emailErrorLabel.hidden = NO;
            self.emailErrorImageView.image = [UIImage imageNamed:@"cross-red-icon"];
            self.emailErrorImageView.hidden = NO;
            break;
        case ErrorCodeInviteCannotInviteSelf:
            self.emailErrorLabel.text = @"This is you";
            self.emailErrorLabel.textColor = [GlobalData kColorSunFlower];
            self.emailErrorLabel.hidden = NO;
            self.emailErrorImageView.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.emailErrorImageView.hidden = NO;
            break;
        case ErrorCodeInviteAlreadyFriends:
            [self presentResponse:InviteFriendResponseTypeAlreadyFriends inviteUserJSON:nil];
            break;
        default:
            [errorJSON showAlertView];
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self inviteButtonAction:nil];
    
    return YES;
}

@end
