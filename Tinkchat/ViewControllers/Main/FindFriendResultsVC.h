//
//  FindFriendResultsVC.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 24.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UsersListJSON.h"
#import "AddressBookUsersListJSON.h"

@interface FindFriendResultsVC : UIViewController

@property (nonatomic, strong) AddressBookUsersListJSON *addressBookSearchResults;
@property (nonatomic, strong) UsersListJSON *tinkchatUsersSearchResults;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
