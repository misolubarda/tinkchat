//
//  FindUserVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 26/02/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "FindUserVC.h"
#import "TinkchatManager.h"
#import "FindFriendCell.h"
#import "MLEndEditingManager.h"

@interface FindUserVC () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) UsersListJSON *usersListJSON;
@property (nonatomic, assign) BOOL endEditingManagerWasEnabled;

@end

@implementation FindUserVC

static UIEdgeInsets initialTableInset;

- (FriendsListJSON *)friendsListJSON
{
    if (!_friendsListJSON)
    {
        _friendsListJSON = [[FriendsListJSON alloc] init];
    }
    
    return _friendsListJSON;
}

- (UsersListJSON *)usersListJSON
{
    if (!_usersListJSON)
    {
        _usersListJSON = [[UsersListJSON alloc] init];
    }
    
    return _usersListJSON;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    initialTableInset = self.tableView.contentInset;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.endEditingManagerWasEnabled = [[MLEndEditingManager sharedInstance] isEnabled];
    
    [[MLEndEditingManager sharedInstance] managerEnabled:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[MLEndEditingManager sharedInstance] managerEnabled:self.endEditingManagerWasEnabled];
}

#pragma mark - Keyboard events

- (void)keyboardDidShow:(NSNotification *)notification
{
    //TODO: Get correct keyboard height
    
//    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat newTableBottomInset = initialTableInset.bottom + 400.0f;

    UIEdgeInsets tableInsets = UIEdgeInsetsMake(initialTableInset.top, initialTableInset.left, newTableBottomInset, initialTableInset.right);
    self.tableView.contentInset = tableInsets;
    self.tableView.scrollIndicatorInsets = tableInsets;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.tableView.contentInset = initialTableInset;
    self.tableView.scrollIndicatorInsets = initialTableInset;
}

#pragma mark -Triggers
- (void)findUsersWithKeyword:(NSString *)keyword
{
    [[TinkchatManager sharedInstance] findTinkchatUserForSearchString:keyword
    completion:^(UsersListJSON *usersListJSON, ErrorJSON *errorJSON)
     {
         if (!errorJSON)
         {
             [AnalyticsHelper trackSearchWithQuery:keyword success:@(YES) error:nil];
         }
         else
         {
             [AnalyticsHelper trackSearchWithQuery:keyword success:@(NO) error:errorJSON];
         }

         [self.usersListJSON updateWithNewFriendList:usersListJSON dispatchAsync:YES completionBlock:^(NSArray *indexesToAdd, NSArray *indexesToRemove, NSArray *indexesToReload)
         {
             [self.tableView reloadData];
         }];
     }];
}

- (void)addTinkchatUser:(NSString *)userId completion:(void (^)(BOOL))completionBlock
{
    [[TinkchatManager sharedInstance] addTinkchatUserWithId:userId
    completion:^(AddUserJSON *addUserJSON, ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            completionBlock(YES);
            [self.delegate findUserVC_refreshFriendsList];
        }
        else
        {
            completionBlock(NO);
            [errorJSON showAlertView];
        }
    }];
}

#pragma mark -TableView delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FindFriendCell *findFriendCell = [tableView dequeueReusableCellWithIdentifier:@"FindFriendCell" forIndexPath:indexPath];
    
    //    findFriendCell.addressBookUserJSON = self.addressBookUsersListJSON.addressBookUsersJSON[indexPath.row];
    
    findFriendCell.owner = self;
    FriendJSON *user = self.usersListJSON.friends[indexPath.row];
    findFriendCell.friendJSON = user;
    findFriendCell.userInFriendsList = [self.friendsListJSON containsUserID:user.user_id];
    
    return findFriendCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return self.addressBookUsersListJSON.addressBookUsersJSON.count;
    return self.usersListJSON.friends.count;
}

@end
