//
//  FindFriendVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 09.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <AddressBookUI/AddressBookUI.h>

#import "AddFriendVC.h"
#import "AddressBookHelper.h"
#import "AddressBookUsersListJSON.h"

#import "FindUserVC.h"
#import "InviteFriendVC.h"
#import "TinkersAddedYouVC.h"

#import "TinkchatManager.h"

@interface AddFriendVC () <UISearchBarDelegate, TinkersAddedYouDelegate, FindUserDelegate>

/**
 *  Search bar
 */
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UIBarButtonItem *leftBarItem;
@property (nonatomic, strong) UIBarButtonItem *rightBarItem;

/**
 *  Container view
 */
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerTopConstraint;
@property (nonatomic, assign) CGFloat containerTopInitialConstant;
@property (nonatomic, strong) UIViewController *presentedVC;

/**
 *  Selection buttons
 */
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (weak, nonatomic) IBOutlet UILabel *inviteLabel;
@property (weak, nonatomic) IBOutlet UIButton *tinkersButton;
@property (weak, nonatomic) IBOutlet UILabel *tinkersLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *buttonsProportionaWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tinkersIconCenterConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inviteIconCenterConstraint;

/**
 *  Model
 */
@property (nonatomic, strong) AddressBookUsersListJSON *addressBookUsersListJSON;
@property (nonatomic, assign) Class lastButtonSelectedClass;

@end

@implementation AddFriendVC

static CGFloat const buttonProportion = 0.3;

static NSTimeInterval const buttonsAnimationDuration = 0.5;
static float const labelsFadeOutRelativeDuration = 0.3;
static float const labelsFadeInRelativeDuration = 0.3;


#pragma mark - Setters/Getters

- (UISearchBar *)searchBar
{
    if (!_searchBar)
    {
        /**
         *  Search bar
         */
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        _searchBar.keyboardType = UIKeyboardTypeEmailAddress;
        _searchBar.placeholder = @"Find Friends";
    }
    
    return _searchBar;
}

#pragma mark - VC life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
        
    /**
     *  Default table top constraint
     */
    self.containerTopInitialConstant = self.containerTopConstraint.constant;
    
    self.navigationItem.titleView = self.searchBar;
    self.navigationItem.hidesBackButton = YES;
    
    /**
     *  Keep reference to Bar button items to remove and add them again
     */
    self.rightBarItem = self.navigationItem.rightBarButtonItem;
    self.leftBarItem = self.navigationItem.leftBarButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupUI];
}

- (void)setupUI
{
    [self.view removeConstraint:self.buttonsProportionaWidthConstraint];
    self.buttonsProportionaWidthConstraint = [NSLayoutConstraint constraintWithItem:self.buttonsProportionaWidthConstraint.firstItem
                                                                          attribute:self.buttonsProportionaWidthConstraint.firstAttribute
                                                                          relatedBy:self.buttonsProportionaWidthConstraint.relation toItem:self.buttonsProportionaWidthConstraint.secondItem
                                                                          attribute:self.buttonsProportionaWidthConstraint.secondAttribute
                                                                         multiplier:buttonProportion
                                                                           constant:0];
    [self.view addConstraint:self.buttonsProportionaWidthConstraint];

    self.inviteIconCenterConstraint.constant = -self.inviteLabel.bounds.size.width/2 - 20;
    self.tinkersIconCenterConstraint.constant = 0.f;
    self.inviteLabel.alpha = 1;
    self.tinkersLabel.alpha = 0;
    self.inviteButton.backgroundColor = [GlobalData kColorEmerald];
    self.tinkersButton.backgroundColor = [GlobalData kColorNephritis];
    
    [self switchToViewControllerOfClass:[InviteFriendVC class]];
}

- (void)searchBarActive:(BOOL)active
{
    if (active)
    {
        self.containerTopConstraint.constant = 0;
        [self switchToViewControllerOfClass:[FindUserVC class]];
    }
    else
    {
        self.searchBar.text = @"";
        self.containerTopConstraint.constant = self.containerTopInitialConstant;
        [self switchToViewControllerOfClass:self.lastButtonSelectedClass];
    }
    
    self.navigationItem.leftBarButtonItem = active ? nil : self.leftBarItem;
    self.searchBar.showsCancelButton = active;
}

#pragma mark -IBAction
- (IBAction)backButtonAction:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)inviteFriendAction:(UIButton *)sender
{
    [self animateTapOnInvite:YES];
    [self switchToViewControllerOfClass:[InviteFriendVC class]];
}

- (IBAction)tinkersButtonAction:(UIButton *)sender
{
    [self animateTapOnInvite:NO];
    [self switchToViewControllerOfClass:[TinkersAddedYouVC class]];
}

- (void)animateTapOnInvite:(BOOL)invite
{
    [self.view removeConstraint:self.buttonsProportionaWidthConstraint];
    self.buttonsProportionaWidthConstraint = [NSLayoutConstraint constraintWithItem:self.buttonsProportionaWidthConstraint.firstItem
                                                                          attribute:self.buttonsProportionaWidthConstraint.firstAttribute
                                                                          relatedBy:self.buttonsProportionaWidthConstraint.relation toItem:self.buttonsProportionaWidthConstraint.secondItem
                                                                          attribute:self.buttonsProportionaWidthConstraint.secondAttribute
                                                                         multiplier:invite ? buttonProportion : 1/buttonProportion
                                                                           constant:0];
    [self.view addConstraint:self.buttonsProportionaWidthConstraint];
    
    self.inviteIconCenterConstraint.constant = invite ? -self.inviteLabel.bounds.size.width/2 - 20 : 0;
    self.tinkersIconCenterConstraint.constant = invite ? 0 : self.tinkersLabel.bounds.size.width/2 + 20;

    [UIView animateWithDuration:buttonsAnimationDuration * labelsFadeOutRelativeDuration
    animations:^
    {
        invite ? (self.tinkersLabel.alpha = 0) : (self.inviteLabel.alpha = 0);
    }];
    
    [UIView animateWithDuration:buttonsAnimationDuration * labelsFadeInRelativeDuration delay:buttonsAnimationDuration - buttonsAnimationDuration * labelsFadeInRelativeDuration options:0
    animations:^
    {
        invite ? (self.inviteLabel.alpha = 1) : (self.tinkersLabel.alpha = 1);
    }
    completion:nil];
    
    [UIView animateWithDuration:buttonsAnimationDuration
    animations:^
    {
        [self.view layoutIfNeeded];
        self.inviteButton.backgroundColor = invite ? [GlobalData kColorEmerald] : [GlobalData kColorNephritis];
        self.tinkersButton.backgroundColor = invite ? [GlobalData kColorNephritis] : [GlobalData kColorEmerald];
    }];
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [self searchBarActive:YES];
    
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self searchBarActive:NO];
    
    [self.view.window endEditing:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length > 0)
    {
        [(FindUserVC *)self.presentedVC findUsersWithKeyword:searchText];
    }
    
    [UIView animateWithDuration:0.3 animations:^
    {
        [self.view layoutIfNeeded];
    }];
}

#pragma mark -Transitionin between VCs
- (void)switchToViewControllerOfClass:(Class)vcClass;
{
    UIViewController *presentingVC;
    NSTimeInterval animationDuration = buttonsAnimationDuration;
    
    if (vcClass == [FindUserVC class])
    {
        if ([self.presentedVC isKindOfClass:[FindUserVC class]])
        {
            /**
             *  If transitioning to actual VC, do nothing.
             */
            return;
        }
        presentingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FindUserVC"];
        [((FindUserVC *)presentingVC).friendsListJSON updateWithNewFriendList:self.friendsListJSON dispatchAsync:NO completionBlock:nil];
        ((FindUserVC *)presentingVC).delegate = self;
        animationDuration = 0;
    }
    else if (vcClass == [TinkersAddedYouVC class])
    {
        if ([self.presentedVC isKindOfClass:[TinkersAddedYouVC class]])
        {
            return;
        }
        presentingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TinkersAddedYouVC"];
        [((TinkersAddedYouVC *)presentingVC).friendsListJSON updateWithNewFriendList:self.friendsListJSON dispatchAsync:NO completionBlock:nil];
        ((TinkersAddedYouVC *)presentingVC).delegate = self;
        self.lastButtonSelectedClass = [TinkersAddedYouVC class];
    }
    else if(vcClass == [InviteFriendVC class])
    {
        if ([self.presentedVC isKindOfClass:[InviteFriendVC class]])
        {
            return;
        }
        presentingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteFriendVC"];
        self.lastButtonSelectedClass = [InviteFriendVC class];
    }
    
    if ([self.presentedVC isKindOfClass:[FindUserVC class]])
    {
        animationDuration = 0;
    }
    
    [self.presentedVC willMoveToParentViewController:nil];
    [self addChildViewController:presentingVC];
    
    if (!self.presentedVC)
    {
        [self.containerView addSubview:presentingVC.view];
        presentingVC.view.frame = self.containerView.bounds;
        [presentingVC didMoveToParentViewController:self];
        self.presentedVC = presentingVC;
        
        return;
    }
    
    /**
     *  Switching presentedVC to be available immediately after calling -switchToViewControllerOfClass:
     *  without waiting for animation to complete.
     */
    UIViewController *presentedVC = self.presentedVC;
    self.presentedVC = presentingVC;
    
    self.view.userInteractionEnabled = NO;
    presentedVC.view.userInteractionEnabled = NO;
    presentingVC.view.userInteractionEnabled = NO;
    
    [self transitionFromViewController:presentedVC toViewController:presentingVC duration:animationDuration options:UIViewAnimationOptionTransitionCrossDissolve
    animations:nil
    completion:^(BOOL finished)
    {
        if (finished)
        {
            [presentedVC removeFromParentViewController];
            [presentingVC didMoveToParentViewController:self];
            
            if (vcClass == [TinkersAddedYouVC class])
            {
                [(TinkersAddedYouVC *)self.presentedVC refreshList];
            }
            
            self.view.userInteractionEnabled = YES;
            presentedVC.view.userInteractionEnabled = YES;
            presentingVC.view.userInteractionEnabled = YES;
        }
    }];
}

#pragma mark -TinkersAddedYouDelegate
- (void)tinkersAddedYouVC_refreshFriendsList
{
    [[TinkchatManager sharedInstance] friendsListCompletion:^(FriendsListJSON *friendsListJSON, ErrorJSON *errorJSON) {
        if (!errorJSON)
        {
            [self.friendsListJSON updateWithNewFriendList:friendsListJSON dispatchAsync:NO completionBlock:nil];
        }
    }];
}

#pragma mark -FindUserDelegate
- (void)findUserVC_refreshFriendsList
{
    [[TinkchatManager sharedInstance] friendsListCompletion:^(FriendsListJSON *friendsListJSON, ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            [self.friendsListJSON updateWithNewFriendList:friendsListJSON dispatchAsync:NO completionBlock:nil];
            
            if ([self.presentedVC isKindOfClass:[FindUserVC class]])
            {
                [((FindUserVC *)self.presentedVC).friendsListJSON updateWithNewFriendList:self.friendsListJSON dispatchAsync:NO completionBlock:nil];
            }
        }
    }];
}

@end
