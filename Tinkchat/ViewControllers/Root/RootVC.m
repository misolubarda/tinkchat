//
//  RootVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 10.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>
//#import <TwitterKit/TwitterKit.h>

#import "RootVC.h"
#import "TinkchatManager.h"
#import "UsersListVC.h"
#import "SignInJoinVC.h"
#import "EmailVerificationVC.h"
#import "LoginNavigationController.h"
#import "MainNavigationController.h"
#import "TinkHelper.h"
#import "MLEndEditingManager.h"
#import "UsernameVC.h"

@interface RootVC () <UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate>

@end

@implementation RootVC

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    [[TinkchatManager sharedInstance] signOut];
    
    [[MLEndEditingManager sharedInstance] managerEnabled:YES];
    
    if ([[TinkchatManager sharedInstance] isSignedIn])
    {
        if ([[TinkchatManager sharedInstance] registrationStatus] >= SignInRegistrationStatus_OnboardingRequired)
        {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            UsersListVC *usersListVC = [mainStoryboard instantiateInitialViewController];
            
            MainNavigationController *navigationVC = [[MainNavigationController alloc] initWithRootViewController:usersListVC];
            
            navigationVC.transitioningDelegate = self;
            navigationVC.modalPresentationStyle = UIModalPresentationFullScreen;
            
            [self showViewController:navigationVC sender:self];
        }
        else
        {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:[NSBundle mainBundle]];
            UsernameVC *usernameVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"UsernameVC"];
            
            LoginNavigationController *navigationVC = [[LoginNavigationController alloc] initWithRootViewController:usernameVC];
            
            navigationVC.transitioningDelegate = self;
            navigationVC.modalPresentationStyle = UIModalPresentationFullScreen;
            
            [self showViewController:navigationVC sender:self];
        }
    }
    else
    {
        UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:[NSBundle mainBundle]];
        SignInJoinVC *signInJoinVC = [loginStoryboard instantiateViewControllerWithIdentifier:@"SignInJoinVCID"];
        
        LoginNavigationController *navigationVC = [[LoginNavigationController alloc] initWithRootViewController:signInJoinVC];

        navigationVC.transitioningDelegate = self;
        navigationVC.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [self showViewController:navigationVC sender:self];
    }
     
    
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIView *containerView = [transitionContext containerView];
    
//    UIViewController *destVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *destinationView = [transitionContext viewForKey:UITransitionContextToViewKey];
    
//    UIViewController *sourceVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *sourceView = [transitionContext viewForKey:UITransitionContextFromViewKey];
  
//    CGRect finalFrame = [transitionContext finalFrameForViewController:destVC];
    
    [containerView addSubview:sourceView];
    [containerView addSubview:destinationView];
    
    destinationView.alpha = 0.0;
    
    [UIView animateWithDuration:1.0
    animations:^
    {
        sourceView.alpha = 0.0;
        destinationView.alpha = 1.0;
    }
    completion:^(BOOL finished)
    {
        [transitionContext completeTransition:YES];
    }];
    
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 1.0;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    return self;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    return self;
}

@end
