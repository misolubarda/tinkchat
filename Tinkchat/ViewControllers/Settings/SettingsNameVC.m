//
//  SettingsNameVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 17/03/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "SettingsNameVC.h"
#import "KeychainManager.h"
#import "TinkchatManager.h"
#import "LoginTextField.h"

@interface SettingsNameVC () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *changeButton;
@property (weak, nonatomic) IBOutlet LoginTextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *errorImageView;
@end

@implementation SettingsNameVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [TinkHelper addDefaultShadowToView:self.changeButton];
    
    self.nameTextField.text = [KeychainManager fullname];
    
    [self resetUI];
}

- (void)resetUI
{
    self.errorLabel.hidden = YES;
    self.errorImageView.hidden = YES;
}

- (IBAction)backButtonAction:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)changeButtonAction:(UIButton *)sender
{
    self.view.userInteractionEnabled = NO;
    [self.activityIndicator startAnimating];

    [[TinkchatManager sharedInstance] editFullname:self.nameTextField.text
    completion:^(EditUserJSON *editUserJSON, ErrorJSON *errorJSON)
    {
        self.view.userInteractionEnabled = YES;
        [self.activityIndicator stopAnimating];
        
        if (!errorJSON)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [self presentErrorForErrorCode:errorJSON];
        }
    }];
}

- (void)presentErrorForErrorCode:(ErrorJSON *)errorJSON
{
    switch (errorJSON.errorCode.integerValue)
    {
            //TODO: check this error code. It is not correct!
        case ErrorCodeEditUserData_UsernameTaken:
            self.errorLabel.hidden = NO;
            self.errorImageView.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.errorImageView.hidden = NO;
            break;
        default:
            [errorJSON showAlertView];
            break;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self resetUI];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self changeButtonAction:nil];
    
    return YES;
}

@end
