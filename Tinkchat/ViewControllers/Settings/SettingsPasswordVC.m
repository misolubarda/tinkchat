//
//  SettingsPasswordVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 18/03/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "SettingsPasswordVC.h"
#import "LoginTextField.h"
#import "TinkchatManager.h"
#import "KeychainManager.h"

@interface SettingsPasswordVC () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet LoginTextField *passwordCurrentTextField;
@property (weak, nonatomic) IBOutlet LoginTextField *passwordNewTextField;
@property (weak, nonatomic) IBOutlet UILabel *errorPasswordCurrentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *errorPasswordCurrentImageView;
@property (weak, nonatomic) IBOutlet UILabel *errorPasswordNewLabel;
@property (weak, nonatomic) IBOutlet UIImageView *errorPasswordNewImageView;
@property (weak, nonatomic) IBOutlet UIButton *changeButton;
@end

@implementation SettingsPasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [TinkHelper addDefaultShadowToView:self.changeButton];
    
    self.passwordCurrentTextField.rightView = [self buttonForTextFieldRightView];
    self.passwordCurrentTextField.rightViewMode = UITextFieldViewModeAlways;
    self.passwordNewTextField.rightView = [self buttonForTextFieldRightView];
    self.passwordNewTextField.rightViewMode = UITextFieldViewModeAlways;
    
    [self resetUI];
}

- (UIButton *)buttonForTextFieldRightView
{
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
    rightButton.titleLabel.font = [[GlobalData defaultFont] fontWithSize:12];
    [rightButton setTitle:@"HIDE" forState:UIControlStateNormal];
    rightButton.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    [rightButton addTarget:self action:@selector(showPassword:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton sizeToFit];
    
    return rightButton;
}

- (void)resetUI
{
    self.errorPasswordCurrentLabel.hidden = YES;
    self.errorPasswordCurrentImageView.hidden = YES;
    self.errorPasswordNewLabel.hidden = YES;
    self.errorPasswordNewImageView.hidden = YES;
}

- (void)showPassword:(UIButton *)sender
{
    if ([sender.superview isKindOfClass:[LoginTextField class]])
    {
        UITextField *textField = (LoginTextField *)sender.superview;
        textField.secureTextEntry = !textField.secureTextEntry;
        textField.font = [[GlobalData defaultFont] fontWithSize:22.f];
        
        /**
         *  Solving cursor position. Move cursor to the end of text.
         */
        [textField resignFirstResponder];
        [textField becomeFirstResponder];
        
        if (textField.secureTextEntry)
        {
            [sender setTitle:@"SHOW" forState:UIControlStateNormal];
        }
        else
        {
            [sender setTitle:@"HIDE" forState:UIControlStateNormal];
        }
        [sender sizeToFit];
    }
}

- (IBAction)backButtonAction:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)changeButtonAction:(UIButton *)sender
{
    [self resetUI];
    
    if (self.passwordCurrentTextField.text.length != 0)
    {
        [[TinkchatManager sharedInstance] changePassword:self.passwordCurrentTextField.text passwordNew:self.passwordNewTextField.text
        completion:^(ErrorJSON *errorJSON)
        {
            if (!errorJSON)
            {
                [self.delegate SettingsPasswordVC:self didCompleteWithSuccess:YES];
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                [self presentErrorForErrorCode:errorJSON];
                [self.delegate SettingsPasswordVC:self didCompleteWithSuccess:NO];
            }
        }];
    }
    else
    {
        ErrorJSON *incorrectPassJSON = [[ErrorJSON alloc] init];
        incorrectPassJSON.errorCode = @(ErrorCodePasswordChange_OldPasswordIncorrect);
        [self presentErrorForErrorCode:incorrectPassJSON];
    }
}

- (void)presentErrorForErrorCode:(ErrorJSON *)errorJSON
{
    switch (errorJSON.errorCode.integerValue)
    {
        case ErrorCodePasswordChange_OldPasswordIncorrect:
            self.errorPasswordCurrentLabel.hidden = NO;
            self.errorPasswordCurrentLabel.text = @"Wrong password";
            self.errorPasswordCurrentLabel.textColor = [GlobalData kColorAlizarin];
            self.errorPasswordCurrentImageView.image = [UIImage imageNamed:@"cross-red-icon"];
            self.errorPasswordCurrentImageView.hidden = NO;
            break;
        case ErrorCodePasswordChange_NewPasswordTooShort:
            self.errorPasswordNewLabel.hidden = NO;
            self.errorPasswordNewLabel.text = @"Password too short";
            self.errorPasswordNewLabel.textColor = [GlobalData kColorAlizarin];
            self.errorPasswordNewImageView.image = [UIImage imageNamed:@"cross-red-icon"];
            self.errorPasswordNewImageView.hidden = NO;
            break;
        default:
            [errorJSON showAlertView];
            break;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.font = [[GlobalData defaultFont] fontWithSize:22.f];
}

//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    if (textField.secureTextEntry)
//    {
//        textField.font = [[GlobalData defaultFont] fontWithSize:22.f];
//    }
//}
@end
