//
//  SettingsPasswordVC.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 18/03/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SettingsPasswordVC;

@protocol SettingsPasswordDelegate <NSObject>

- (void)SettingsPasswordVC:(SettingsPasswordVC *)settingsPassswordVC didCompleteWithSuccess:(BOOL)success;

@end

@interface SettingsPasswordVC : UIViewController

@property (nonatomic, weak) id <SettingsPasswordDelegate> delegate;

@end
