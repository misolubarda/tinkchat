//
//  TellUsAnythingVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 18/03/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "TellUsAnythingVC.h"
#import "TinkchatManager.h"

@interface TellUsAnythingVC () <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@end

@implementation TellUsAnythingVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [TinkHelper addDefaultShadowToView:self.sendButton];
}

- (IBAction)backButtonAction:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sendButtonAction:(UIButton *)sender
{
    [[TinkchatManager sharedInstance] sendFeedback:self.textView.text
    completion:^(ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [errorJSON showAlertView];
        }
    }];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [self.view endEditing:YES];
        return NO;
    }
    
    return YES;
}

@end
