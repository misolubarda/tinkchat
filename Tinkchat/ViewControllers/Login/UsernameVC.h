//
//  UsernameVC.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 03/06/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarViewController.h"

@interface UsernameVC : NavigationBarViewController

@property (nonatomic) BOOL NOT_Onboarding;

@end
