//
//  SignInVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 10.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "SignInVC.h"
#import "TinkchatManager.h"
#import "KeychainManager.h"
#import "TinkHelper.h"

@interface SignInVC () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *emailErrorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *emailErrorImageView;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *passwordErrorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *passwordErrorImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *errorRevealingConstraint;

@end

@implementation SignInVC

#pragma mark - ViewController life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [TinkHelper addDefaultShadowToView:self.signInButton];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
    rightButton.titleLabel.font = [[GlobalData defaultFont] fontWithSize:12];
    [rightButton setTitle:@"SHOW" forState:UIControlStateNormal];
    rightButton.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    [rightButton addTarget:self action:@selector(showPassword:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton sizeToFit];
    
    self.passwordTextField.rightView = rightButton;
    self.passwordTextField.rightViewMode = UITextFieldViewModeAlways;
    
    [self resetUIanimated:NO];
    
    [self.emailTextField becomeFirstResponder];
}

- (void)resetUIanimated:(BOOL)animated
{
    self.emailErrorLabel.hidden = YES;
    self.passwordErrorLabel.hidden = YES;
    self.emailErrorImageView.hidden = YES;
    self.passwordErrorImageView.hidden = YES;
    self.errorRevealingConstraint.constant = 40;
    
    if (animated)
    {
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    else
    {
        [self.view layoutIfNeeded];
    }
}

- (IBAction)backButtonAction:(UIBarButtonItem *)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Core
- (IBAction)signInButtonAction:(UIButton *)sender
{
    [self enableInteraction:NO];
    [self resetUIanimated:YES];
    
    [[TinkchatManager sharedInstance] signInUserWithEmail:self.emailTextField.text password:self.passwordTextField.text
    completion:^(SignInJSON *signInJSON, ErrorJSON *errorJSON)
    {
        [self enableInteraction:YES];

        if (!errorJSON)
        {
            [AnalyticsHelper trackLogin:YES error:errorJSON];
            
            if (signInJSON.registrationStatus <= SignInRegistrationStatus_UsernameRequired)
            {
                [self performSegueWithIdentifier:@"UsernameVCSegue" sender:self];
            }
            else
            {
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
        }
        else
        {
            [AnalyticsHelper trackLogin:NO error:errorJSON];

            [self presentErrorForErrorCode:errorJSON];
        }
    }];
}

- (void)presentErrorForErrorCode:(ErrorJSON *)errorJSON
{
    switch (errorJSON.errorCode.integerValue)
    {
        case ErrorCodeSignIn_EmailRequired:
            self.emailErrorLabel.text = @"Forgot this one";
            self.emailErrorLabel.hidden = NO;
            self.emailErrorImageView.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.emailErrorImageView.hidden = NO;
            break;
        case ErrorCodeSignIn_PasswordRequired:
            self.passwordErrorLabel.text = @"Forgot this one";
            self.passwordErrorLabel.hidden = NO;
            self.passwordErrorImageView.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.passwordErrorImageView.hidden = NO;
            break;
        case ErrorCodeSignIn_EmailOrPassWrong:
        {
            self.errorRevealingConstraint.constant = 100;
            
            [UIView animateWithDuration:0.3f animations:^{
                [self.view layoutIfNeeded];
            }];
            break;
        }
        case ErrorCodeSignIn_AccountNotActivated:
        {
            NSString *activationString = [NSString stringWithFormat:@"Account not yet activated. We sent a new activation link to %@. Click it to activate your account.", self.emailTextField.text];
            [[[UIAlertView alloc] initWithTitle:@"Check your email" message:activationString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            break;
        }
        default:
            [errorJSON showAlertView];
            break;
    }
}

- (void)enableInteraction:(BOOL)enable
{
    self.signInButton.alpha = enable ? 1.0f : 0.5f;
    self.view.userInteractionEnabled = enable;
    
    if (enable)
    {
        [self.activityIndicator stopAnimating];
    }
    else
    {
        [self.activityIndicator startAnimating];
        [self.view endEditing:YES];
    }
}

- (void)showPassword:(UIButton *)sender
{
    self.passwordTextField.secureTextEntry = !self.passwordTextField.secureTextEntry;
    
    /**
     *  Solving cursor position. Move cursor to the end of the text.
     */
    [self.passwordTextField resignFirstResponder];
    [self.passwordTextField becomeFirstResponder];
    
    if (self.passwordTextField.secureTextEntry)
    {
        [sender setTitle:@"SHOW" forState:UIControlStateNormal];
    }
    else
    {
        [sender setTitle:@"HIDE" forState:UIControlStateNormal];
    }
    
    [sender sizeToFit];
}

#pragma mark - TextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self resetUIanimated:YES];
    
    // Disable emoji input
    return [textField textInputMode] != nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.emailTextField])
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.passwordTextField])
    {
        [self signInButtonAction:nil];
    }
    
    return YES;
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.view endEditing:YES];
}

@end
