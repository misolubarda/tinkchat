//
//  EmailVerificationVC.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 22.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarViewController.h"

@interface EmailVerificationVC : NavigationBarViewController

@property (nonatomic, strong) NSString *email;

@end
