//
//  SignInVC.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 10.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarViewController.h"

@interface SignInVC : NavigationBarViewController

@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end
