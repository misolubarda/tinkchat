//
//  SignInJoinVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 10.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "SignInJoinVC.h"
#import "TinkchatManager.h"
#import "TinkHelper.h"


@interface SignInJoinVC ()
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;

@end

@implementation SignInJoinVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [TinkHelper addDefaultShadowToView:self.joinButton];
    [TinkHelper addDefaultShadowToView:self.signInButton];
    [TinkHelper addDefaultShadowToView:self.facebookButton];
}

- (IBAction)facebookButtonAction:(UIButton *)sender
{
    [[TinkchatManager sharedInstance] signInUserWithFacebookCompletion:^(ErrorJSON *errorJSON)
    {
        if (!errorJSON)
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            [errorJSON showAlertView];
        }
    }];
}

@end
