//
//  ForgotPasswordVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 02/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "TinkchatManager.h"
#import "KeychainManager.h"
#import "ForgotPasswordNotificationVC.h"
#import "TinkHelper.h"

@interface ForgotPasswordVC ()
@property (weak, nonatomic) IBOutlet UIButton *resetPasswordButton;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *emailErrorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *emailErrorImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation ForgotPasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [TinkHelper addDefaultShadowToView:self.resetPasswordButton];
    
    [self resetUI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.emailTextField becomeFirstResponder];
}

- (void)resetUI
{
    self.emailErrorLabel.hidden = YES;
    self.emailErrorImageView.hidden = YES;
}

#pragma mark - Core
- (IBAction)resetPasswordButtonAction:(UIButton *)sender
{
    [self enableInteraction:NO];
    [self resetUI];
    
    [[TinkchatManager sharedInstance] resetPasswordForEmail:self.emailTextField.text
    completion:^(ErrorJSON *errorJSON)
    {
        [self enableInteraction:YES];
        
        if (!errorJSON)
        {
            [self performSegueWithIdentifier:@"ForgotPasswordNotificationSegue" sender:self];
        }
        else
        {
            [self presentErrorForErrorCode:errorJSON];
        }
    }];
}

- (IBAction)backButtonAction:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)presentErrorForErrorCode:(ErrorJSON *)errorJSON
{
    switch (errorJSON.errorCode.integerValue)
    {
        case ErrorCodePasswordReset_EmailMissing:
            self.emailErrorLabel.text = @"Forgot this one";
            self.emailErrorLabel.textColor = [GlobalData kColorSunFlower];
            self.emailErrorLabel.hidden = NO;
            self.emailErrorImageView.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.emailErrorImageView.hidden = NO;
            break;
        case ErrorCodePasswordReset_EmailNotExist:
            self.emailErrorLabel.text = @"Email does not exist";
            self.emailErrorLabel.hidden = NO;
            self.emailErrorLabel.textColor = [GlobalData kColorAlizarin];
            self.emailErrorImageView.image = [UIImage imageNamed:@"cross-red-icon"];
            self.emailErrorImageView.hidden = NO;
            break;
        default:
            [errorJSON showAlertView];
            break;
    }
}

- (void)enableInteraction:(BOOL)enable
{
    self.resetPasswordButton.alpha = enable ? 1.0f : 0.5f;
    self.view.userInteractionEnabled = enable;
    
    if (enable)
    {
        [self.activityIndicator stopAnimating];
    }
    else
    {
        [self.activityIndicator startAnimating];
        [self.view endEditing:YES];
    }
}

#pragma mark - TextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self resetUI];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self resetPasswordButtonAction:nil];
    
    return YES;
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ForgotPasswordNotificationSegue"])
    {
        ForgotPasswordNotificationVC *notificationVC = (ForgotPasswordNotificationVC *)segue.destinationViewController;
        notificationVC.email = self.emailTextField.text;
    }
    [self.view endEditing:YES];
}
@end
