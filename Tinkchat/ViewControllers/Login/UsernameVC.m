//
//  UsernameVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 03/06/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "UsernameVC.h"
#import "TinkchatManager.h"
#import "TinkHelper.h"
#import "KeychainManager.h"

@interface UsernameVC () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *usernameErrorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *usernameErrorImage;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *manualsLabel;
@end


@implementation UsernameVC

#pragma mark - View

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.NOT_Onboarding)
    {
        self.title = @"Username";
        [self disableStatusBarBackgroundView];
        [self.navigationBar removeFromSuperview];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow-back-icon"] style:UIBarButtonItemStyleDone target:self action:@selector(backButtonAction:)];
        self.nextButton.backgroundColor = [GlobalData kColorAlizarin];
        self.usernameTextField.text = [KeychainManager username];
        [self.nextButton setTitle:@"Save" forState:UIControlStateNormal];
    }
    else
    {
        self.nextButton.backgroundColor = [GlobalData kColorEmerald];
        [self.nextButton setTitle:@"Next" forState:UIControlStateNormal];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [TinkHelper addDefaultShadowToView:self.nextButton];
    
    [self resetUI];
}

- (void)backButtonAction:(UIBarButtonItem *)buttonItem
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)resetUI
{
    self.usernameErrorLabel.hidden = YES;
    self.usernameErrorImage.hidden = YES;
    self.manualsLabel.textColor = [GlobalData kColorConcrete];
    self.manualsLabel.font = [self.manualsLabel.font fontWithSize:16.0f];
}

- (void)presentErrorForErrorCode:(ErrorJSON *)errorJSON
{
    switch (errorJSON.errorCode.integerValue)
    {
        case ErrorCodeEditUserData_UsernameTaken:
            self.usernameErrorLabel.text = @"Usename taken";
            self.usernameErrorLabel.hidden = NO;
            self.usernameErrorImage.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.usernameErrorImage.hidden = NO;
            break;
        case ErrorCodeEditUserData_UsernameInvalid:
            self.usernameErrorImage.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.usernameErrorImage.hidden = NO;
            self.manualsLabel.textColor = [GlobalData kColorSunFlower];
            self.manualsLabel.font = [self.manualsLabel.font fontWithSize:18.0f];
            break;
        case ErrorCodeEditUserData_UsernameTooShort:
            self.usernameErrorLabel.text = @"Min. 4 characters";
            self.usernameErrorLabel.hidden = NO;
            self.usernameErrorImage.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.usernameErrorImage.hidden = NO;
            break;
        case ErrorCodeEditUserData_UsernameTooLong:
            self.usernameErrorLabel.text = @"Max. 24 characters";
            self.usernameErrorLabel.hidden = NO;
            self.usernameErrorImage.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.usernameErrorImage.hidden = NO;
            break;
        default:
            [errorJSON showAlertView];
            break;
    }
}

#pragma mark - IBAction
- (IBAction)nextButtonAction:(UIButton *)sender
{
    BOOL usernameValid = [self isUsernameValid:self.usernameTextField.text];
    
    if (usernameValid)
    {
        self.view.userInteractionEnabled = NO;
        [self.activityIndicator startAnimating];
        
        [[TinkchatManager sharedInstance] editUsername:self.usernameTextField.text onboarding:!self.NOT_Onboarding
        completion:^(EditUserJSON *editUserJSON, ErrorJSON *errorUsernameJSON)
        {
            self.view.userInteractionEnabled = YES;
            [self.activityIndicator stopAnimating];
            
            if (!errorUsernameJSON)
            {
                if (self.NOT_Onboarding)
                {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else
                {
                    [self proceedOnboarding];        
                }
            }
            else
            {
                [self presentErrorForErrorCode:errorUsernameJSON];
            }
        }];
    }
}

- (void)proceedOnboarding
{
    self.view.userInteractionEnabled = NO;
    [self.activityIndicator startAnimating];
    
    /**
     *  After username set, onboarding required
     */
    [[TinkchatManager sharedInstance] editRegistrationStatusTo:SignInRegistrationStatus_OnboardingRequired
                                                    completion:^(ErrorJSON *errorStatusJSON)
     {
         self.view.userInteractionEnabled = YES;
         [self.activityIndicator stopAnimating];
         
         if (!errorStatusJSON)
         {
             [self.navigationController dismissViewControllerAnimated:YES completion:nil];
         }
         else
         {
             [self presentErrorForErrorCode:errorStatusJSON];
         }
     }];
}


- (BOOL)isUsernameValid:(NSString *)username
{
    if (username.length < 4)
    {
        ErrorJSON *errorJSON = [[ErrorJSON alloc] init];
        errorJSON.errorCode = @(ErrorCodeEditUserData_UsernameTooShort);
        
        [self presentErrorForErrorCode:errorJSON];
        
        return NO;
    }
    else if (username.length > 24)
    {
        ErrorJSON *errorJSON = [[ErrorJSON alloc] init];
        errorJSON.errorCode = @(ErrorCodeEditUserData_UsernameTooLong);
        
        [self presentErrorForErrorCode:errorJSON];
        
        return NO;
    }
    else if (![username.lowercaseString isEqualToString:username])
    {
        ErrorJSON *errorJSON = [[ErrorJSON alloc] init];
        errorJSON.errorCode = @(ErrorCodeEditUserData_UsernameInvalid);
        
        [self presentErrorForErrorCode:errorJSON];
        
        return NO;
    }
    
    NSString *usernameRegexp = @"(?=[a-z0-9]{4,24})[a-z]{1,24}[0-9]{0,23}";
    
    NSPredicate *usernamePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", usernameRegexp];
    
    if (![usernamePredicate evaluateWithObject:username])
    {
        ErrorJSON *errorJSON = [[ErrorJSON alloc] init];
        errorJSON.errorCode = @(ErrorCodeEditUserData_UsernameInvalid);
        
        [self presentErrorForErrorCode:errorJSON];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self resetUI];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self nextButtonAction:nil];
    
    return YES;
}

@end
