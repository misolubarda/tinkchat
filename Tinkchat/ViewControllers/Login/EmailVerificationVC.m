//
//  EmailVerificationVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 22.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "EmailVerificationVC.h"

@interface EmailVerificationVC ()
@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@end

@implementation EmailVerificationVC


- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString *message = [NSString stringWithFormat:@"We sent an activation link to\n%@\n\nClick it to complete\nyour profile.", self.email];
    NSDictionary *mainAttributes = @{NSFontAttributeName : [[GlobalData defaultFont] fontWithSize:20.f],
                                     NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:message attributes:mainAttributes];
    self.messageLabel.attributedText = attributedMessage;
}

- (IBAction)proceedButtonAction:(UIButton *)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
