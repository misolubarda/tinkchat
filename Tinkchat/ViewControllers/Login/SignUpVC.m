//
//  JoinVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 10.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "SignUpVC.h"
#import "TinkchatManager.h"
#import "EmailVerificationVC.h"
#import "TinkHelper.h"
#import "LoginTextField.h"

@interface SignUpVC () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet LoginTextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *emailErrorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *emailErrorImageView;
@property (weak, nonatomic) IBOutlet LoginTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *passwordErrorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *passwordErrorImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation SignUpVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [TinkHelper addDefaultShadowToView:self.joinButton];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
    rightButton.titleLabel.font = [[GlobalData defaultFont] fontWithSize:12];
    [rightButton setTitle:@"HIDE" forState:UIControlStateNormal];
    rightButton.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    [rightButton addTarget:self action:@selector(showPassword:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton sizeToFit];
    
    self.passwordTextField.rightView = rightButton;
    self.passwordTextField.rightViewMode = UITextFieldViewModeAlways;
    
    
    /**
     * Privacy policy and terms     
     */
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *mainAttributes = @{NSFontAttributeName: [[GlobalData defaultFont] fontWithSize:14.f],
                                     NSForegroundColorAttributeName: [GlobalData kColorConcrete],
                                     NSParagraphStyleAttributeName: paragraphStyle};
    NSMutableDictionary *hyperlinkAttributes = [mainAttributes mutableCopy];
    [hyperlinkAttributes addEntriesFromDictionary:@{NSLinkAttributeName: @"http://tinkchatapp.com/privacy",
                                                    NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                                    NSForegroundColorAttributeName: [GlobalData kColorConcrete]}];
    
    NSMutableAttributedString *disclamerString = [[NSMutableAttributedString alloc] initWithString:@"By using this application you agree\n to our "
                                                                                        attributes:mainAttributes];
    NSAttributedString *privacyHyperlink = [[NSAttributedString alloc] initWithString:@"privacy policy" attributes:hyperlinkAttributes];
    NSAttributedString *disclamerStringMiddle = [[NSAttributedString alloc] initWithString:@" and " attributes:mainAttributes];
    [hyperlinkAttributes addEntriesFromDictionary:@{NSLinkAttributeName: @"http://tinkchatapp.com/terms"}];
    NSAttributedString *termsHyperlink = [[NSAttributedString alloc] initWithString:@"terms" attributes:hyperlinkAttributes];
    NSAttributedString *disclamerStringRight = [[NSAttributedString alloc] initWithString:@"." attributes:mainAttributes];
    
    [disclamerString appendAttributedString:privacyHyperlink];
    [disclamerString appendAttributedString:disclamerStringMiddle];
    [disclamerString appendAttributedString:termsHyperlink];
    [disclamerString appendAttributedString:disclamerStringRight];
    
    self.disclamerTextView.attributedText = disclamerString;
    
    [self resetUI];
}

- (void)resetUI
{
    self.emailErrorLabel.hidden = YES;
    self.emailErrorImageView.hidden = YES;
    self.passwordErrorLabel.hidden = YES;
    self.passwordErrorImageView.hidden = YES;
}

- (IBAction)backButtonAction:(UIBarButtonItem *)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)joinButtonAction:(UIButton *)sender
{
    [self enableInteraction:NO];
    [self resetUI];

    [[TinkchatManager sharedInstance] signUpUserWithEmail:self.emailTextField.text password:self.passwordTextField.text
    completion:^(ErrorJSON *errorJSON)
    {
        [self enableInteraction:YES];

        if (!errorJSON)
        {
            [AnalyticsHelper trackSignUp:YES error:errorJSON];
            
            [self performSegueWithIdentifier:@"EmailVerifiedSegue" sender:self];
        }
        else
        {
            [AnalyticsHelper trackSignUp:NO error:errorJSON];

            [self presentErrorForErrorCode:errorJSON];
        }
    }];
}

- (void)presentErrorForErrorCode:(ErrorJSON *)errorJSON
{
    switch (errorJSON.errorCode.integerValue)
    {
        case ErrorCodeSignUp_EmailMissing:
            self.emailErrorLabel.text = @"Forgot this one";
            self.emailErrorLabel.textColor = [GlobalData kColorSunFlower];
            self.emailErrorLabel.hidden = NO;
            self.emailErrorImageView.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.emailErrorImageView.hidden = NO;
            break;
        case ErrorCodeSignUp_PasswordMissing:
            self.passwordErrorLabel.text = @"Forgot this one";
            self.passwordErrorLabel.textColor = [GlobalData kColorSunFlower];
            self.passwordErrorLabel.hidden = NO;
            self.passwordErrorImageView.image = [UIImage imageNamed:@"exclamation-yellow-icon"];
            self.passwordErrorImageView.hidden = NO;
            break;
        case ErrorCodeSignUp_EmailTaken:
            self.emailErrorLabel.text = @"Already in use";
            self.emailErrorLabel.textColor = [GlobalData kColorAlizarin];
            self.emailErrorLabel.hidden = NO;
            self.emailErrorImageView.image = [UIImage imageNamed:@"cross-red-icon"];
            self.emailErrorImageView.hidden = NO;
            break;
        case ErrorCodeSignUp_PasswordTooShort:
            self.passwordErrorLabel.text = @"8 characters or longer";
            self.passwordErrorLabel.textColor = [GlobalData kColorAlizarin];
            self.passwordErrorLabel.hidden = NO;
            self.passwordErrorImageView.image = [UIImage imageNamed:@"cross-red-icon"];
            self.passwordErrorImageView.hidden = NO;
            break;
        default:
            [errorJSON showAlertView];
            break;
    }
}

- (void)enableInteraction:(BOOL)enable
{
    self.joinButton.alpha = enable ? 1.0f : 0.5f;
    self.view.userInteractionEnabled = enable;
    
    if (enable)
    {
        [self.activityIndicator stopAnimating];
    }
    else
    {
        [self.activityIndicator startAnimating];
        [self.view endEditing:YES];
    }
}

- (void)showPassword:(UIButton *)sender
{
    self.passwordTextField.secureTextEntry = !self.passwordTextField.secureTextEntry;
    
    /**
     *  Solving cursor position. Move cursor to the end of text.
     */
    [self.passwordTextField resignFirstResponder];
    [self.passwordTextField becomeFirstResponder];
    
    if (self.passwordTextField.secureTextEntry)
    {
        [sender setTitle:@"SHOW" forState:UIControlStateNormal];
    }
    else
    {
        [sender setTitle:@"HIDE" forState:UIControlStateNormal];
    }
    
    [sender sizeToFit];
}

#pragma mark - TextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self resetUI];
    
    // Disable emoji input
    return [textField textInputMode] != nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.emailTextField])
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.passwordTextField])
    {
        [self joinButtonAction:nil];
    }
    
    return YES;
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.view endEditing:YES];
    
    if ([segue.identifier isEqualToString:@"EmailVerifiedSegue"])
    {
        EmailVerificationVC *verificationVC = (EmailVerificationVC *)segue.destinationViewController;
        verificationVC.email = self.emailTextField.text;
    }
}

@end
