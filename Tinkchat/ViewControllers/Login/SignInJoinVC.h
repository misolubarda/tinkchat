//
//  SignInJoinVC.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 10.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarViewController.h"

@interface SignInJoinVC : NavigationBarViewController

@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;

@end
