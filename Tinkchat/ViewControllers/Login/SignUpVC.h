//
//  JoinVC.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 10.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarViewController.h"

@interface SignUpVC : NavigationBarViewController

@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITextView *disclamerTextView;

@end
