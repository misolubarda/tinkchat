//
//  ForgotPasswordNotificationVC.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 02/04/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "ForgotPasswordNotificationVC.h"
#import "TinkHelper.h"

@interface ForgotPasswordNotificationVC ()
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@end

@implementation ForgotPasswordNotificationVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *message = [NSString stringWithFormat:@"We sent a new password to\n%@", self.email];
    NSDictionary *mainAttributes = @{NSFontAttributeName : [[GlobalData defaultFont] fontWithSize:20.0f],
                                     NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:message attributes:mainAttributes];
    self.messageLabel.attributedText = attributedMessage;
}

- (IBAction)proceedButtonAction:(UIButton *)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
