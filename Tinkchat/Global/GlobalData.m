//
//  GlobalData.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 22.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import "GlobalData.h"

#if defined(DEV)
NSString *kServerUrl = @"https://tinkchatapp.herokuapp.com";
#else
NSString *kServerUrl = @"http://tinkchatapp.com/";
#endif
NSString *kUserDefaultsKeyPushToken = @"pushToken";
NSString *kUserDefaultsKeyAuthToken = @"authToken";
NSString *kUserEmail = @"kUserEmail";
NSString *kUserPassword = @"kUserPassword";
NSString *kUserName = @"kUserName";

NSString* const kNotificationCenterTink = @"kNotificationCenterTink";
NSString* const kNotificationCenterAppActive = @"kNotificationCenterApplicationDidBecomeActive";

#if defined(DEBUG)
NSTimeInterval const tinkAnimationTime = 60.0;
#else
NSTimeInterval const tinkAnimationTime = 60.0;
#endif

@implementation GlobalData

+ (NSURL *)tinkchatWebsiteUrl
{
    return [NSURL URLWithString:@"http://tinkchatapp.com"];
}

+ (NSURL *)tinkchatFacebookApp
{
    return [NSURL URLWithString:@"fb://profile/1539711526270680"];
}

+ (NSURL *)tinkchatFacebookUrl
{
    return [NSURL URLWithString:@"https://www.facebook.com/tinkchat?fref=ts"];
}


+ (UIColor *)kColorConcrete
{
    return [UIColor colorWithRed:149.f/255.f green:165.f/255.f blue:166.f/255.f alpha:1.f];
}

+ (UIColor *)kColorPeterRiver
{
    return [UIColor colorWithRed:52.f/255.f green:152.f/255.f blue:219.f/255.f alpha:1.f];
}

+ (UIColor *)kColorSunFlower
{
    return [UIColor colorWithRed:241.f/255.f green:196.f/255.f blue:15.f/255.f alpha:1.f];
}

+ (UIColor *)kColorPomegranate
{
    return [UIColor colorWithRed:192.f/255.f green:57.f/255.f blue:43.f/255.f alpha:1.f];
}

+ (UIColor *)kColorEmerald
{
    return [UIColor colorWithRed:46.f/255.f green:204.f/255.f blue:113.f/255.f alpha:1.f];
}

+ (UIColor *)kColorAlizarin
{
    return [UIColor colorWithRed:231.f/255.f green:76.f/255.f blue:60.f/255.f alpha:1.f];
}

+ (UIColor *)kColorWetAsphalt
{
    return [UIColor colorWithRed:52.f/255.f green:73.f/255.f blue:94.f/255.f alpha:1.f];
}

+ (UIColor *)kColorBelizeHole
{
    return [UIColor colorWithRed:41.f/255.f green:128.f/255.f blue:185.f/255.f alpha:1.f];
}

+ (UIColor *)kColorMidnightBlue
{
    return [UIColor colorWithRed:44.f/255.f green:62.f/255.f blue:80.f/255.f alpha:1.f];
}

+ (UIColor *)kColorTorquise
{
    return [UIColor colorWithRed:26.f/255.f green:188.f/255.f blue:156.f/255.f alpha:1.f];
}

+ (UIColor *)kColorAmethyst
{
    return [UIColor colorWithRed:155.f/255.f green:89.f/255.f blue:182.f/255.f alpha:1.f];
}

+ (UIColor *)kColorCarrot
{
    return [UIColor colorWithRed:230.f/255.f green:126.f/255.f blue:34.f/255.f alpha:1.f];
}

+ (UIColor *)kColorNephritis
{
    return [UIColor colorWithRed:39.f/255.f green:174.f/255.f blue:96.f/255.f alpha:1.f];
}


+ (UIColor *)kColorBeforeTink
{
    return [UIColor colorWithRed:73.0f/255.0f green:163.0f/255.0f blue:223.0f/255.0f alpha:1.0f];
}


+ (UIFont *)defaultFont
{
    return [UIFont fontWithName:@"VarelaRound-Regular" size:20.f];
}



@end

