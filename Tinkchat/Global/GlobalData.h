//
//  GlobalData.h
//  Tinkchat
//
//  Created by Mišo Lubarda on 22.11.14.
//  Copyright (c) 2014 Tinkchat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString *kServerUrl;
extern NSString *kUserDefaultsKeyPushToken;
extern NSString *kUserDefaultsKeyAuthToken;
extern NSString *kUserEmail;
extern NSString *kUserPassword;
extern NSString *kUserName;

extern NSString* const kNotificationCenterTink;
extern NSString* const kNotificationCenterAppActive;

extern NSTimeInterval const tinkAnimationTime;

typedef NS_ENUM(NSUInteger, ReservedUserId) {
    ReservedUserId_TinkchatTeam = 0
};

@interface GlobalData : NSObject

+ (NSURL *)tinkchatWebsiteUrl;
+ (NSURL *)tinkchatFacebookApp;
+ (NSURL *)tinkchatFacebookUrl;

+ (UIColor *)kColorConcrete;
+ (UIColor *)kColorPeterRiver;
+ (UIColor *)kColorPomegranate;
+ (UIColor *)kColorSunFlower;
+ (UIColor *)kColorEmerald;
+ (UIColor *)kColorAlizarin;
+ (UIColor *)kColorWetAsphalt;
+ (UIColor *)kColorBelizeHole;
+ (UIColor *)kColorMidnightBlue;
+ (UIColor *)kColorTorquise;
+ (UIColor *)kColorAmethyst;
+ (UIColor *)kColorCarrot;
+ (UIColor *)kColorNephritis;

+ (UIColor *)kColorBeforeTink;

+ (UIFont *)defaultFont;

@end
