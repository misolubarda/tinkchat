//
//  MaskView.m
//  Tinkchat
//
//  Created by Mišo Lubarda on 23/03/15.
//  Copyright (c) 2015 Tinkchat. All rights reserved.
//

#import "MaskView.h"

@interface MaskView ()

@property (nonatomic, strong) UIView *backgroundMaskView;
@property (nonatomic, strong) UIView *translucentMaskView;
@property (nonatomic, strong) UILabel *maskLabel;
@property (nonatomic, strong) NSLayoutConstraint *maskLabelTopConstraint;
@property (nonatomic, strong) CALayer *maskLayer;

@end

@implementation MaskView

#pragma mark -Setters/Getters
- (UIView *)backgroundMaskView
{
    if (!_backgroundMaskView)
    {
        _backgroundMaskView = [[UIView alloc] initWithFrame:self.bounds];
        _backgroundMaskView.backgroundColor = [UIColor whiteColor];
        _backgroundMaskView.alpha = 1.0f;
    }
    return _backgroundMaskView;
}

- (UIView *)translucentMaskView
{
    if (!_translucentMaskView)
    {
        _translucentMaskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _translucentMaskView.backgroundColor = [UIColor whiteColor];
//        _translucentMaskView.layer.cornerRadius = 25.f;
//        _translucentMaskView.layer.masksToBounds = YES;
        _translucentMaskView.alpha = 1.f;
    }
    return _translucentMaskView;
}

- (UILabel *)maskLabel
{
    if (!_maskLabel)
    {
        _maskLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _maskLabel.font = [GlobalData defaultFont];
        _maskLabel.textColor = [UIColor whiteColor];
        _maskLabel.numberOfLines = 0;
        _maskLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.superview addSubview:_maskLabel];
        
        [self.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_maskLabel]-20-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_maskLabel)]];
        self.maskLabelTopConstraint = [NSLayoutConstraint constraintWithItem:_maskLabel
                                                                   attribute:NSLayoutAttributeTop
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.superview
                                                                   attribute:NSLayoutAttributeTop
                                                                  multiplier:1.f
                                                                    constant:20.f];
        [self.superview addConstraint:self.maskLabelTopConstraint];
    }
    return _maskLabel;
}

- (void)maskSuperview:(UIView *)superview
{
    if (superview)
    {
        /**
         *  Setup
         */

        self.frame = superview.bounds;

        /**
         *  Adding self as subview to catch taps.
         */
        [superview addSubview:self];
        
        /**
         *  Adding vies for mask layers
         */
        [superview addSubview:self.backgroundMaskView];
        [superview addSubview:self.translucentMaskView];
        
        /**
         *  Creating mask with sublayers
         */
        self.maskLayer = [CALayer layer];
        self.maskLayer.frame = self.bounds;
        [self.maskLayer addSublayer:self.backgroundMaskView.layer];
        [self.maskLayer addSublayer:self.translucentMaskView.layer];
        
        /**
         *  Adding mask
         */
        superview.layer.mask = self.maskLayer;
    }
    else
    {
        /**
         *  Clean up
         */
        [self.maskLayer removeFromSuperlayer];
        self.maskLayer = nil;
        [self.maskLabel removeFromSuperview];
        [self.backgroundMaskView removeFromSuperview];
        [self.translucentMaskView removeFromSuperview];
        [self removeFromSuperview];
    }
}

- (void)setMaskText:(NSString *)text topSpace:(CGFloat)originY animated:(BOOL)animated
{
    [self setMaskText:text topSpace:originY color:[UIColor whiteColor] animated:animated];
}

- (void)setMaskText:(NSString *)text topSpace:(CGFloat)originY color:(UIColor *)color animated:(BOOL)animated
{
    NSTimeInterval animationTime = animated ? 0.5f : .0f;

    [UIView animateWithDuration:animationTime
    animations:^
    {
        self.maskLabel.alpha = 0.f;
    }
    completion:^(BOOL finished)
    {
        self.maskLabel.text = text;
        self.maskLabel.textColor = color;
        self.maskLabelTopConstraint.constant = originY;
        [self layoutIfNeeded];
        
        [UIView animateWithDuration:animationTime
        animations:^
        {
            self.maskLabel.alpha = 1.f;
        }];
    }];
}

- (void)setMaskFrame:(CGRect)frame animated:(BOOL)animated
{
    NSTimeInterval animationTime = animated ? 0.5f : .0f;
    
    [UIView animateWithDuration:animationTime
    animations:^
    {
        self.translucentMaskView.alpha = .0f;
    }
    completion:^(BOOL finished)
    {
        /**
         *  Enlarging mask to cover more space than just frame
         */
        CGRect largerFrame = CGRectApplyAffineTransform(frame, CGAffineTransformMakeScale(1.f, 1.2f));
        largerFrame = CGRectMake(frame.origin.x, frame.origin.y - (largerFrame.size.height - frame.size.height)/2.f, largerFrame.size.width, largerFrame.size.height);
        self.translucentMaskView.frame = largerFrame;

        [UIView animateWithDuration:animationTime
        animations:^
        {
            self.translucentMaskView.alpha = 1.f;
        }];
    }];
}

- (CGRect)maskFrame
{
    return self.translucentMaskView.frame;
}

- (void)hideMaskViewCompletion:(void (^)())completionBlock
{
    [UIView animateWithDuration:1.0f
    animations:^
    {
        self.backgroundMaskView.alpha = 1.f;
        self.maskLabel.alpha = 0.f;
    }
    completion:^(BOOL finished)
    {
        completionBlock();
    }];
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if (CGRectContainsPoint(self.translucentMaskView.frame, point))
    {
        return NO;
    }
    
    return YES;
}

@end
